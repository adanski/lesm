--- Contains functions load and save tables for the application
-- @module Core
-- @type class
Core             = { }
Core.NAME        = 'LuaESMod'
Core.LONGNAME    = 'Lua Enhanced Server Module'
Core.VERSION     = 'v2.7.6'
Core.SUB_VERSION = ''
Core.DESCRIPTION = 'Server side lua mod. Enhances gameplay and server management.'
Core.URL         = 'https://bitbucket.org/zelly/lua-enhanced-server-mod'
Core.AUTHOR      = 'Zelly'
Core.CONTACT     = 'Xfire: anewxfireaccount'
Core.LICENSE     = [[
The MIT License (MIT)

Copyright (c) 2015 Zelly

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]

-----------------
-- Core VARIABLES
-----------------

--- Basepath is where the executable and lua files are
Core.basepath = string.gsub( et.trap_Cvar_Get("fs_basepath"),"\\","/") .. "/" .. et.trap_Cvar_Get("fs_game") .. "/"
--- Homepath is where logs and json goes
Core.homepath = string.gsub( et.trap_Cvar_Get("fs_homepath"),"\\","/") .. "/" .. et.trap_Cvar_Get("fs_game") .. "/"
Core.LOADED  = false -- If core has been successfully initialized
Core.running = false -- If run routine is running or not
Core.Bits    = { }   -- Stores bit numbers

------------------
-- LOCAL FUNCTIONS
------------------
local tonumber,os_time,next = tonumber,os.time,next

--- sort function
-- Sorts by key "id"
local _sortById   = function(a,b) return a.id < b.id end

--- Initializes each level associated with a xp amount
local _setLevels = function()
    if Config.AutoLevel.Enabled then Console:Info("Setting Automatic Leveling XP for Levels") end
    local current_xp = Config.AutoLevel.XPBase
    for level = Config.AutoLevel.StartLevel, Config.AutoLevel.EndLevel do
        if level > 0 then
            if level == Config.AutoLevel.StartLevel then
                Levels[level] = current_xp
            else
                current_xp = Misc:round( current_xp + (current_xp * Config.AutoLevel.XPModifier) , 0 )
                Levels[level] = current_xp
            end
        end
    end
end

--- This loads all the extra tables, and checks whether or not the are empty to create new ones if not exist
local _loadTables = function()
    Console:Info("Loading saved files...")
    local mod = Game:Mod()
    if mod == "nq" then mod = "noquarter" end
    local tempdefaults = dofile(Core.basepath .. "LESM/util/defaults.lua")
    Game.Maps        = FileHandler:Load{ filePath = "LESM/save/Maps.json"}
    FieldNames       = FileHandler:Load{ filePath = "LESM/extra/FieldNames.json"}
    ClientFieldNames = FileHandler:Load{ filePath = "LESM/extra/ClientFieldNames.json"}
    Classes          = FileHandler:Load{ filePath = "LESM/extra/Classes.json"               , default = tempdefaults.classes }
    Countries        = FileHandler:Load{ filePath = "LESM/extra/Countries.json"             , default = tempdefaults.countries}
    Grammar          = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Grammar     , default = tempdefaults.grammar}
    SwearReplace     = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.SwearFilter , default = tempdefaults.swearfilter}
    Rules            = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Rules       , default = tempdefaults.rules}
    Banners          = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Banners     , default = tempdefaults.banners}
    Color            = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Colors      , default = tempdefaults.colors}
    Cvars            = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Cvars       , default = tempdefaults.cvars}
    Entities         = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Entities    , default = tempdefaults.entities}
    -- TODO defaults.lua entities
    --[[{ {
            map        = "mapnamehere",
            scriptname = "scriptnamehere",
            timeleft   = false,
            players    = false,
            find       = false,
            loop       = false,
            free       = false,
        }, }]]--
    WeaponHandler.Weapons = FileHandler:Load{ filePath = "LESM/extra/Weapons.json"               , default = tempdefaults.weapons[mod]}
    WeaponHandler:Init()
end



--- Reads map cvars set by map rotation
local _readCvar = function(cvar)
    local cvarvalue = et.trap_Cvar_Get(cvar)
    local set_var  = tonumber( Misc:match(cvar,Config.MapListVarName .. "(%d+)") )
    local mapname  = Misc:match(cvarvalue,"map%s+(%S+)")
    local vstr_var = tonumber( Misc:match(cvarvalue,"set%s+nextmap%s+vstr%s+" .. Config.MapListVarName .. "(%d+)") )
    if mapname and vstr_var and set_var then return { id = set_var , mapname = mapname, vstr=vstr_var } end
    return nil
end



--- Loads custom constants for code readability primarily
local _loadCustomConstants = function()
    Console:Info("Loading constants")
    et.TEAM_NONE       = 0
    et.TEAM_AXIS       = 1
    et.TEAM_ALLIES     = 2
    et.TEAM_SPEC       = 3
    et.CLASS_SOLDIER   = 0
    et.CLASS_MEDIC     = 1
    et.CLASS_ENGINEER  = 2
    et.CLASS_FIELDOPS  = 3
    et.CLASS_COVERTOPS = 4
    et.SKILL_SENSE     = 0
    et.SKILL_ENGINEER  = 1
    et.SKILL_MEDIC     = 2
    et.SKILL_SIGNAL    = 3
    et.SKILL_LIGHT     = 4
    et.SKILL_HEAVY     = 5
    et.SKILL_COVERT    = 6
    et.MEASUREMENT_UNITS    = 0
    et.MEASUREMENT_IMPERIAL = 1
    et.MEASUREMENT_METRIC   = 2
    if Game:Mod() == 'etpro' then
        et.MAXCHARS_CHAT   = 58
        et.MAXCHARS_ECHO   = 58
        et.MAXCHARS_CENTERPRINT = 58
        et.MAXCHARS_BANNERPRINT = 58
        et.MAXCHARS_PRINT  = 85 -- r_mode 4
    else
        et.MAXCHARS_CHAT   = 64
        et.MAXCHARS_PRINT  = 93 -- r_mode 4
    end
    
    et.TEAM = { }
    et.TEAM[et.TEAM_NONE]   = "None"
    et.TEAM[et.TEAM_AXIS]   = "Axis"
    et.TEAM[et.TEAM_ALLIES] = "Allies"
    et.TEAM[et.TEAM_SPEC]   = "Spectator"
    
    et.CLASS = { }
    et.CLASS[et.CLASS_SOLDIER]   = "Soldier"
    et.CLASS[et.CLASS_MEDIC]     = "Medic"
    et.CLASS[et.CLASS_ENGINEER]  = "Engineer"
    et.CLASS[et.CLASS_FIELDOPS]  = "Field Ops"
    et.CLASS[et.CLASS_COVERTOPS] = "Covert Ops"
    
    et.SKILL = { }
    et.SKILL[et.SKILL_SENSE]    = "Battle Sense"
    et.SKILL[et.SKILL_ENGINEER] = "Engineer"
    et.SKILL[et.SKILL_MEDIC]    = "Medic"
    et.SKILL[et.SKILL_SIGNAL]   = "Signals"
    et.SKILL[et.SKILL_LIGHT]    = "Light Weapons"
    et.SKILL[et.SKILL_HEAVY]    = "Heavy Weapons"
    et.SKILL[et.SKILL_COVERT]   = "Covert Ops"
end

--- Starts the application
-- Registers all the routines
function Core:Init()
    local _START_TIME = et.trap_Milliseconds()
    require("LESM/UserObject")
    require("LESM/UserHandler")
    require("LESM/RoutineHandler")
    require("LESM/MessageHandler")
    require("LESM/ClientHandler")
    require("LESM/WeaponHandler")
    require("LESM/ClientObject")
    require("LESM/EntityHandler")
    require("LESM/Command")
    require("LESM/CommandHandler")
    require("LESM/MailHandler")
    require("LESM/FileHandler")
    require("LESM/ConfigHandler")
    require("LESM/ConsoleObject")
    require("LESM/Logger")
    require("LESM/Game")
    require("LESM/Misc")
    require("LESM/Sound")
    require("LESM/RegularUser")
    Console:Info("Initializing...")
    
    
    
    --- JSON File Handler
    -- @type json
    -- http://regex.info/blog/lua/json
    --JSON = assert(dofile(self.basepath .. "LESM/util/JSON.lua"),"Error loading json")
    if string.find(_VERSION,'5.0') then
        dofile(Core.basepath .. "LESM/util/jsonsimple.lua")
    else
        JSON = (loadfile(self.basepath .. "LESM/util/JSON.lua"))()
    end
    -- TODO Lua 5.3 has bit support
    require("LESM/util/bit")-- http://luaforge.net/projects/bit/
    
    ConfigHandler:Init()
    Logger:Init()
    if Game:Gamestate() == 2 then
        Logger:Log("warmup",Game:Map())
    elseif Game:Gamestate() == 0 then
        Logger:Log("game",Game:Map())
    end
    _loadTables()
    UserHandler:LoadUsers()
    RegularUser:Init()
    Mail:Init()
    CommandHandler:Init()
    _loadCustomConstants()
    EntityHandler:Init()
    _setLevels()
    self:setBits()
    Console:Info("Adding runframe routines")
    RoutineHandler:RegisterCoreRoutines()
    
    RoutineHandler:refreshBanners()
    if Config.Encrypt then
        if string.find(_VERSION,'5.0') then
            Console:Info("Using basic string encryption")
            sha = nil
        elseif string.find(_VERSION,'5.1') then
            local _SHA1_START_TIME = Game:getLevelTime()
            Console:Info("Using sha1...")
            sha = require("LESM/util/sha1") -- https://github.com/kikito/sha1.lua
            Console:Info("Loaded sha1 in " .. Game:getLevelTime() - _SHA1_START_TIME .. "ms")
        elseif string.find(_VERSION,'5.3') then
            local _SHA2_START_TIME = Game:getLevelTime()
            Console:Info("Using sha2...")
            sha = require("LESM/util/sha2") -- http://lua-users.org/wiki/SecureHashAlgorithmBw
            Console:Info("Loaded sha2 in " .. Game:getLevelTime() - _SHA2_START_TIME .. "ms")
        else
            Console:Warn("No encryption was loaded, requirements not met")
            sha = nil
        end
    end
    Sound:Load()
    self:BlacklistLoad()
    if Config.DamagePrint then
        if Game:Mod() == "silent" then
            et.trap_Cvar_Set("g_debugDamage",1)
            Console:Info("DamagePrint has enabled g_debugDamage for its use")
        elseif Game:Mod() == "legacy" then
            Console:Info("DamagePrint has enabled")
        else
            Console:Warning("DamagePrint is only supported by silent and legacy")
            Config.DamagePrint = false
        end
    end
    Game:ChangeState()
    
    MessageHandler.MOTD = FileHandler:Load{ filePath = "LESM/save/motd.txt" , fileType = 'text' , makeIfNotExists = true }
    if Misc:trim(MessageHandler.MOTD) == '' then
        Console:Warning("Not LESM/save/motd.txt found, Message of the Day is disabled.")
    end
    Console:Info("Registering modname...")
    et.RegisterModname( self.NAME)
    et.trap_SendConsoleCommand( et.EXEC_APPEND , "sets " .. self.NAME .. " " .. self.VERSION .. self.SUB_VERSION .. "\n" ) -- Set ModInfo to serverinfo
    Console:Info(self.LONGNAME .. " " .. self.VERSION  .. self.SUB_VERSION )
    Console:PrintClean(self.LONGNAME .." " .. Color.Secondary .. self.VERSION .. Color.Tertiary .. self.SUB_VERSION .. " " .. Color.Primary .. self.DESCRIPTION .. " Created by " .. Color.Secondary .. self.AUTHOR)
    Console:Info("Done initializing in " .. Game:getLevelTime() - _START_TIME .. "ms")
    self.LOADED = true
end

--- Shut down and save all necessary files
function Core:DeInit()
    Console:Info("Deinitializing...")
    if Game:Gamestate() == 0 then
        Logger:Log("warmup_end",Game:Map())
    elseif Game:Gamestate() == -1 then
        Logger:Log("game_end",Game:Map())
    end
    self:BlacklistSave()
    UserHandler:SaveUsers()
    RegularUser:Deinit()
    Mail:DeInit()
    Logger:DeInit()
    Console:Info("Done deinitializing")
end

--- Get r_customwidth cvar from client
function Core.rcustomwidth_function(Client)
    if not Client then return end
    
    local width = tonumber(Client.cvars['r_customwidth']) or 800
    
    if width == 800 then
        Client.maxChars = 95
    elseif width == 960 then
        Client.maxChars = 106 -- untested probably higher
    elseif width == 1024 then
        Client.maxChars = 113 -- untested probably higher
    elseif width == 1152 then
        Client.maxChars = 128 -- untested probably higher
    elseif width == 1280 then
        Client.maxChars = 155
    elseif width == 1600 then
        Client.maxChars = 177 -- untested probably higher
    elseif width == 2048 then
        Client.maxChars = 227 -- untested probably higher
    else
        Client.maxChars = math.floor(width / 9 ) -- Approximate guess
    end
    
    Console:Info( "Setting " .. Client:getName() .. " maxChars to : " .. Client.maxChars)
end

--- Gets r_mode cvar from client
function Core.rmode_function(Client)
    if not Client then return end
    
    local r_mode = tonumber(Client.cvars['r_mode']) or 4
    
    if r_mode == 4 then
        Client.maxChars = 95
    elseif r_mode == 5 then
        Client.maxChars = 106 -- untested probably higher
    elseif r_mode == 6 then
        Client.maxChars = 113 -- untested probably higher
    elseif r_mode == 7 then
        Client.maxChars = 128 -- untested probably higher
    elseif r_mode == 8 then
        Client.maxChars = 155
    elseif r_mode == 9 then
        Client.maxChars = 177 -- untested probably higher
    elseif r_mode == 10 then
        Client.maxChars = 227 -- untested probably higher
    elseif r_mode == -1 then
        Client:checkCvar("r_customwidth" , Core.rcustomwidth_function)
        return
    else
        Client.maxChars = 95
    end
    
    Console:Info( "Setting " .. Client:getName() .. " maxChars to : " .. Client.maxChars)
end

function Core:MapList()
    local cvar     = Config.MapListVarName .. tostring(Config.MapListStart)
    local lastcvar = ""
    local maps     = { }
    local count    = 0 -- failsafe in case things go bad
    while count < 100 do
        count = count + 1
        if lastcvar == cvar then break end
        lastcvar = cvar
        local map = _readCvar(cvar)
        if map then
            maps[tlen(maps)+1] = map
            if map.vstr == Config.MapListStart then break end
            cvar = Config.MapListVarName .. tostring(map.vstr)
        else
            break
        end
    end
    local current = et.trap_Cvar_Get("nextmap")
    current = string.gsub(current,"vstr ","")
    local current_map = _readCvar(current)
    
    return maps,current_map
end

--- Blacklist System

-- TODO Figure out loading data...
-- TODO Logger/Debug messages
-- TODO Mail messages
-- TODO Basically all of rudclient needs to be rewritten as well...

local _BlacklistId = function (blacklist)
    if not blacklist then return end
    local blacklistid;
    if type(blacklist) == "table" then
        blacklistid = tonumber(blacklist.id)
    elseif tonumber(blacklist) then
        blacklistid = tonumber(blacklist)
    end
    return blacklistid
end

function Core:BlacklistLoad()
    Blacklist        = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Blacklist }
    self:BlacklistVerify()
end

function Core:BlacklistSave()
    self:BlacklistVerify()
    FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Blacklist, fileData = Blacklist}
end

function Core:BlacklistAdd(blacklist)
    if not blacklist then return end
    if not type(blacklist) == "table" then return end
    local old_blacklist,blacklist_index = self:BlacklistGet(blacklist.id)
    if not old_blacklist then old_blacklist = { } end -- set this so we dont get errors when checking if for keys
    blacklist.id        = tonumber(blacklist.id)      or old_blacklist.id or self:getNextId(Blacklist)
    blacklist.guid      = blacklist.guid              or old_blacklist.guid or "" ; blacklist.guid = Misc:trim(blacklist.guid)
    blacklist.guid2     = blacklist.guid2             or old_blacklist.guid2 or "" ; blacklist.guid2 = Misc:trim(blacklist.guid2)
    blacklist.ip        = blacklist.ip                or old_blacklist.ip or "" ; blacklist.ip = Misc:triml(blacklist.ip)
    blacklist.title     = blacklist.title             or old_blacklist.title or "" ; blacklist.title = Misc:trim(blacklist.title)
    blacklist.reason    = blacklist.reason            or old_blacklist.reason or "" ; blacklist.reason = Misc:trim(blacklist.reason)
    blacklist.desc      = blacklist.desc              or old_blacklist.desc or "" ; blacklist.desc = Misc:trim(blacklist.desc)
    blacklist.banner    = blacklist.banner            or old_blacklist.banner or "Console" ; blacklist.banner = Misc:trim(blacklist.banner)
    blacklist.expires   = tonumber(blacklist.expires) or old_blacklist.expires or -1
    if blacklist.expires > 0 and os.time() > blacklist.expires then blacklist.expires = blacklist.expires  + os.time() end
    blacklist.temp      = blacklist.temp or false
    if old_blacklist.id then
        Blacklist[blacklist_index] = blacklist
    else
        Blacklist[tlen(Blacklist)+1] = blacklist
    end
end

function Core:BlacklistRemove(blacklist)
    if not blacklist then return false end
    local _,index = self:BlacklistGet(blacklist)
    local found = false
    while index do
        found = true
        table.remove(Blacklist,index)
        _,index = self:BlacklistGet(blacklist)
    end
    return found
end

function Core:BlacklistGet(blacklist)
    local id = _BlacklistId(blacklist)
    if not id then return nil end
    for blacklist_index=1,tlen(Blacklist) do
        local blacklist = Blacklist[blacklist_index]
        if blacklist.id == id then return blacklist,blacklist_index end
    end
    return nil
end

function Core:BlacklistIsExpired(blacklistid)
    local blacklist = self:BlacklistGet(blacklistid)
    if not blacklist then return true end -- Returns true, as most checks to this if false will kick the client
    if blacklist.expires <= 0 then
        return false
    elseif ( os_time() - blacklist.expires ) > 0 then
        return false
    else
        return true
    end
end

function Core:BlacklistVerify()
    local _Blacklist = { }
    for k=1,tlen(Blacklist) do
        local b    = Blacklist[k]
        local keep = true
        if type(b) == "table" then
            b.id          = tonumber(b.id)
            b.title       = Misc:trim(b.title)
            b.desc = b.desc or { }
            if not type(b.desc) == "table" then b.desc = { } end
            b.guid        = Misc:trim(b.guid)
            b.guid2       = Misc:trim(b.guid2)
            b.ip          = Misc:triml(b.ip)
            b.banner      = Misc:trimlname(b.banner)
            b.reason      = Misc:trim(b.reason)
            b.expires     = tonumber(b.expires) or -1
            if b.temp ~= true then b.temp = false end
            if b.expires <= 0 then
                b.expires = -1
            elseif ( os_time() - b.expires ) < 0 then
                -- Expired
                keep = false
            end
            if b.banner == "" then b.banner = "N/A" end
            if not b.id or b.temp == true or ( b.ip == "" and b.guid == "" ) then keep = false end
        else
            keep = false
        end
        if keep then _Blacklist[tlen(Blacklist)+1] = b end
    end
    Blacklist = _Blacklist
    table.sort(Blacklist,_sortById)
end


function Core:BlacklistCheck(Client)
    if not next(Blacklist) or Client:isBot() then return nil end
    local ip                    = Client:getIp()
    local eGuid,sGuid,etproGuid = Client:getGuids()
    local blacklist;
    for k=1,tlen(Blacklist) do
        local b = Blacklist[k]
        if not self:BlacklistIsExpired(b.id) then
            if ip    ~= "" and b.ip ~= ""     and ip    == b.ip                 then blacklist = b ; break end
            if eGuid ~= "" and b.guid ~= ""   and eGuid == b.guid   then blacklist = b ; break end
            if sGuid ~= "" and b.guid2 ~= "" and sGuid == b.guid2 then blacklist = b ; break end
            if etproGuid ~= "" and b.guid2 ~= "" and etproGuid == b.guid2 then blacklist = b ; break end
            if ip    ~= "" and b.ip ~= ""     and string.find(ip,b.ip,1,true)          then blacklist = b ; break end
        end
    end
    if blacklist then
        if Config.Blacklist.AllowIfAuth or Config.Blacklist.AllowIfLeader then
            local leader,auth = false,false
            for k=1,tlen(Clients) do
                local m = false
                if Clients[k]:getLevel() >= Config.Level.Leader then
                    leader = true
                    if Config.Blacklist.AllowIfLeader and not m then
                        Clients[k]:Chat("Blacklisted client " .. Color.Secondary .. Client:getName() .. Color.Primary .. " has joined.")
                        Clients[k]:Play(Config.Sound.Blacklist)
                        m = true
                    end
                end
                if Clients[k]:isAuth() then
                    auth = true
                    if Config.Blacklist.AllowIfAuth and not m then
                        Clients[k]:Chat("Blacklisted client " .. Color.Secondary .. Client:getName() .. Color.Primary .. " has joined.")
                        Clients[k]:Play(Config.Sound.Blacklist)
                        m = true
                    end
                end
            end
            if ( Config.Blacklist.AllowIfAuth and auth ) or ( Config.Blacklist.AllowIfLeader and leader ) then return nil end -- Leader/Auth Found
        end
        return blacklist -- Blacklisted
    end
    return nil
end

function Core:BlacklistReason(blacklist)
    if tonumber(blacklist) then blacklist = self:BlacklistGet(blacklist) end
    if not blacklist then return "" end
    local reason = { }
    reason[tlen(reason)+1] = Color.Primary .. Misc:GetStaticString("Ban",12) .. Color.Tertiary .. "#" .. Color.Secondary .. blacklist.id 
    reason[tlen(reason)+1] = Color.Primary .. Misc:GetStaticString("Banner by",12) .. Color.Secondary .. blacklist.banner
    if blacklist.temp then
        reason[tlen(reason)+1] = Color.Primary .. Misc:GetStaticString("Banned for",12) .. Color.Secondary .. "this map only"
    else
        if blacklist.expires <= 0 then
            reason[tlen(reason)+1] = Color.Primary .. Misc:GetStaticString("Banned for",12) .. Color.Secondary .. "permanently"
        else
            reason[tlen(reason)+1] = Color.Primary .. Misc:GetStaticString("Banned for",12) .. Color.Secondary .. Misc:SecondsToClock(blacklist.expires-os_time())
        end
    end
    reason[tlen(reason)+1] = Color.Primary .. Misc:GetStaticString("Reason",12) .. Color.Secondary .. blacklist.reason
    return table.concat(reason,"\n")
end
--- End blacklist

function Core:DecayXp()
    if not Config.XpSave.Enabled then return end
    local DoDecay  = true
    local DoExpire = true
    if Config.XpSave.Decay <= 0 or Config.XpSave.DecayRate <= 0 then DoDecay = false end
    if Config.XpSave.Expire <= 0 then DoExpire = false end
    if not DoDecay and not DoExpire then return end
    
    local currentTime = os_time()
    if not Config.XpSave.RequireLogin then
        for k=1,tlen(RegularUserData) do
            local rudclient = RegularUserData[k]
            if not rudclient.lastseen then rudclient.lastseen = currentTime end -- Verify lastseen is in check
            if DoExpire and Misc:table_total(rudclient.skills) > 0 and ( currentTime - rudclient.lastseen ) > Config.XpSave.Expire then
                rudclient.skills = { 0,0,0,0,0,0,0,}
                Console:Info(rudclient.lastname .. "'s skill has expired")
                Console:Debug("[xpsave] " .. rudclient.lastname .. "'s skill has expired","xpsave")
            elseif DoDecay then
                for j=1,tlen(rudclient.skills) do
                    if ( currentTime - rudclient.lastseen ) > Config.XpSave.Decay and rudclient.skills[j] > Config.XpSave.DecayMin then
                        rudclient.skills[j] = rudclient.skills[j] - Config.XpSave.DecayRate
                        if rudclient.skills[j] < Config.XpSave.DecayMin then rudclient.skills[j] = Config.XpSave.DecayMin end
                    end
                end
            end
        end
    end

    for k=1,tlen(Users) do
        if DoExpire and Misc:table_total(Users[k].skills) > 0 and ( currentTime - Users[k].lastseen ) > Config.XpSave.Expire then
            Users[k].skills = { 0,0,0,0,0,0,0,}
            Console:Info(Users[k].username .. "'s skill has expired")
            Console:Debug(Users[k].username .. "'s skill has expired","xpsave")
        elseif DoDecay then
            for j=1,tlen(Users).skills do
                if ( currentTime - Users[k].lastseen ) > Config.XpSave.Decay and Users[k].skills[j] > Config.XpSave.DecayMin then
                    Users[k].skills[j] = Users[k].skills[j] - Config.XpSave.DecayRate
                    if Users[k].skills[j] < Config.XpSave.DecayMin then Users[k].skills[j] = Config.XpSave.DecayMin end
                end
            end
        end
    end
end

--- Bit cvar Stuff
--- Initializes each bit with a number
-- bits.tobits k value needs added to by 1 in order to match with this
function Core:setBits()
    self.Bits = { [1]=1, }
    for k=2, Game:Maxclients()+1 do self.Bits[k] = self.Bits[k-1] * 2 end
end

--- Get slot of bitnumber
function Core:getBitNum(bitnumber) -- This is all I need here I think.
    for k=0 , tlen(self.Bits) do
        if self.Bits[k] == bitnumber then return k end
    end
end

function Core:getETProIACGuid( text )
    if Game:Mod() ~= 'etpro' then return end
    -- etpro IAC: 0 GUID [GUIDISHEREGUIDISHEREGUIDISHEREGUIDISHERE] [name]
    --      Only called once per connection
    -- etpro IAC: 0 [NAMEISHERECANCONTAINALOTOFCHARACHTERS] [GUIDISHEREGUIDISHEREGUIDISHEREGUIDISHERE] [win32]
    --      Called every game init
    -- Kind of relys on globalcombinedfixes.lua to verify this isn't malformed.
    -- Put that first
    
    local clientNum , guid = Misc:match( text , "^etpro IAC: (%d+) %[.+%] %[(%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x)%]" )
    if not clientNum or not guid then return end
    --Console:Info(va("getETProIACGuid: %d %s",clientNum,guid))
    if string.len(guid) ~= 40 then
        Console:Warning(va("%d malformed guid?",clientNum))
        return
    end -- Possibily misformed
    local Client = ClientHandler:getClient(clientNum)
    if not Client then
        Console:Warning(va("%d could not return client",clientNum))
        return
    end
    Console:Info(va("Client %s got guid %s",Client:getName() , guid ))
    Client.etpro_guid = guid
    if Client:isOnline() then Client.User:updateNotFound( 'etpro' , guid ) end
    Client:validateGuid()
end

--- Get next id from table
-- table[key][index] index defaults to "id"
function Core:getNextId(t,index)
    if not t or type(t) ~= "table"then return end
    if not index then index = "id" end
    
    local id = 0
    while true do
        id = id + 1
        local f = false
        for k=1,tlen(t) do
            if t[k][index] == id then f=true;break end
        end
        if not f then return id end
    end
    return id
end

function Core:encrypt(s)
    if sha then
        if sha.hash256 then
            return sha.hash256(s)
        else
            return sha(s)
        end
    else
        return s
        -- Well since this function does not work anymore in 5.3 I am not gonna waste my time fixing it.
        -- It was a false sense of security anyways, literally anyone that knows any lua could have bypassed encrypytion.
        -- Oh well.
        --return Misc:encrypt(s)
    end
end

function Core:getTeamNum(team)
    if not team then return nil end
    
    if ( team == "b" or team == "allies" or team == "allied" or tonumber(team) == et.TEAM_ALLIES ) then
        team = et.TEAM_ALLIES
    elseif ( team == "r" or team == "axis" or tonumber(team) == et.TEAM_AXIS ) then
        team = et.TEAM_AXIS
    elseif ( team == "s" or team == "spectator" or team == "spec" or tonumber(team) == et.TEAM_SPEC ) then
        team = et.TEAM_SPEC
    end
    if ( team ~= et.TEAM_AXIS and team ~= et.TEAM_ALLIES and team ~= et.TEAM_SPEC ) then return end
    
    return team
end