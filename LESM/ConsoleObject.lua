Console = { }

function Console:getName() return "(Lua)" end
-------------------------------
-- # Printing/Chat Methods # --
-------------------------------
local _printOneOrMultiLine = function( message , length )
    length = tonumber(length)
    if not length or not message then return nil end
    
    -- Not sure whats going on here one of these is having too many args
    -- but seperating them made it not error
    local y = MessageHandler:sformat('',Client)
    local x = et.Q_CleanStr( y )
    local paddingLength = string.len( x )
    
    if ( string.len( et.Q_CleanStr( message ) ) + paddingLength ) <= length then
        return message
    else
        return Misc:FormatLongString( message , length - paddingLength )
    end
end

function Console:Chat( message  )
    message = _printOneOrMultiLine( message , et.MAXCHARS_CHAT )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( -1 , "chat \"" .. MessageHandler:sformat(message) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( -1 , "chat \"" .. MessageHandler:sformat(message[k]) .. Color.WHITE .. "\"")
        end
    end
end

function Console:Print( message  )
    message = _printOneOrMultiLine( message , et.MAXCHARS_CHAT )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( -1 , "print \"" .. MessageHandler:sformat(message) .. Color.WHITE .. "\n\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( -1 , "print \"" .. MessageHandler:sformat(message[k]) .. Color.WHITE .. "\n\"")
        end
    end
end

function Console:Echo( message  )
    message = _printOneOrMultiLine( message , et.MAXCHARS_CHAT )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( -1 , "echo \"" .. MessageHandler:sformat(message) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( -1 , "echo \"" .. MessageHandler:sformat(message[k]) .. Color.WHITE .. "\"")
        end
    end
end

function Console:Banner( message  )
    message = _printOneOrMultiLine( message , et.MAXCHARS_CHAT )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( -1 , "bp \"" .. MessageHandler:sformat(message) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( -1 , "bp \"" .. MessageHandler:sformat(message[k]) .. Color.WHITE .. "\"")
        end
    end
end

function Console:CenterPrint( message  )
    message = _printOneOrMultiLine( message , et.MAXCHARS_CHAT )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( -1 , "cp \"" .. MessageHandler:sformat(message) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( -1 , "cp \"" .. MessageHandler:sformat(message[k]) .. Color.WHITE .. "\"")
        end
    end
end

function Console:ChatClean(message)
    et.trap_SendServerCommand( -1, "chat \"" .. message .. Color.WHITE .. "\"")
end

function Console:PrintClean(message)
    et.trap_SendServerCommand( -1, "print \"" .. message .. Color.WHITE .. "\n\"")
end

function Console:EchoClean(message)
    et.trap_SendServerCommand( -1, "cpm \"" .. message .. Color.WHITE .. "\"")
end

function Console:BannerClean(message)
    et.trap_SendServerCommand( -1, "bp \"" .. message .. Color.WHITE .. "\"")
end

function Console:CenterPrintClean(message)
    et.trap_SendServerCommand( -1, "cp \"" .. message .. Color.WHITE .. "\"")
end


function Console:InfoClean(message)
    et.G_LogPrint(et.Q_CleanStr( message ) .."\n")
end

function Console:Warning(message)
    et.G_LogPrint("[Warning] "..et.Q_CleanStr( message ) .."\n")
end

function Console:Error(message,verbose)
    if verbose and not Config.Debug.Enabled then return end
    et.G_LogPrint("[Error] "..et.Q_CleanStr( message ) .."\n")
end

function Console:Info(message)
    et.G_LogPrint("[LESM] "..et.Q_CleanStr( message ) .."\n")
end

function Console:Debug(message,type)
    if not Config.Debug.Enabled then return end
    message = et.Q_CleanStr(message)
    Logger:Debug(message,type)
end
    
function Console:isConsole() return true end

function Console:getColumn() return 5 end

function Console:getMaxChars() return Config.ConMaxChars or 38 end
function Console:getMaxRows() return 25 end

function Console:PrintLocation(str,location)
    if not str then return end
    if not location then location = "info" end
    location = Misc:triml(location)
    local maxChars = 70 -- Client would be longer ... so idk :3
    if location == "chat" then
        self:Chat(str)
    elseif location == "chatclean" then
        self:ChatClean(str)
    elseif location == "print" then
        self:Print(str)
    elseif location == "printclean" then
        self:PrintClean(str)
    elseif location == "echo" then
        self:Echo(str)
    elseif location == "echoclean" then
        self:EchoClean(str)
    elseif location == "banner" then
        self:Banner(str)
    elseif location == "bannerclean" then
        self:BannerClean(str)
    elseif location == "centerprint" then
        self:CenterPrint(str)
    elseif location == "centerprintclean" then
        self:CenterPrintClean(str)
    elseif location == "info" then
        self:Info(str)
    elseif location == "infoclean" then
        self:InfoClean(str)
    elseif location == "debug" then
        self:Debug(str,"other")
    else
        self:Info(str)
    end
end

function Console:IteratePrint(message,location,key,From,useReplaceText)
    if not message then return end
    if not location then return end
    location = Misc:triml(location)
    for k=1,tlen(Clients) do
        local Client = Clients[k]
        local client_location = location
        if not Client:isIgnored(From) then
            if key and Client:isOnline() then client_location = Misc:triml(Client.User:getKey(key)) end
            if client_location == "" or client_location == "server" then client_location = location end
            if useReplaceText then message = MessageHandler:replace( message , true , Client ) end
            if client_location ~= "none" then Client:PrintLocation(message,client_location) end
        end
    end
end