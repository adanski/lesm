Command = class("Command")

function Command:initialize(name)
    if not name then return nil end
    --self.CLAN           = false
    self.TITLE          = name -- Do not know if I will use or not, just a pretty form of the name
    self.NAME           = string.lower(name) -- The name of the command
    self.CORE           = false -- Can be used with no prefix Ex. /testcommand
    self.ADMIN          = false -- Can be used with admin prefix in console Ex. /!testcommand
    self.CHAT           = false -- Can be used in chat with chat prefix Ex. /say !testcommand
    self.LUA            = false -- Can be used with lua prefix Ex. /lua testcommand
    self.AUTH           = false -- Can be used with auth prefix Ex. /auth testcommand
    self.MAIL           = false -- Can be used with mail prefix Ex. /mail testcommand (Should be saved only for mail related commands)
    self.CONSOLE        = false -- Can be used in the rcon/server console Note: you may have to check if Client:isConsole() if you are doing certain client calls
    self.OVERWRITE      = false -- If this command is normally a shrubbot command, Will return 0 instead of 1
    self.NOSHRUBBOT     = false -- Should not be loaded if shrubbot is loaded.
    self.REQUIRE_LEVEL  = 0     -- Required level to use the command
    self.REQUIRE_LOGIN  = false -- Required to be logged in to use the command
    self.REQUIRE_AUTH   = false -- Required to have authorized key to use the command
    self.GLOBALOUTPUT   = false -- If the command will have global output (Whether or not spam protection is applied to it)
    --self.REQUIRE_CLAN   = false
    self.REQUIRE_MOD    = { }   -- If command only works on certain mods then it should be in this list
    self.REQUIRE_FLAG   = ""
    self.PATH           = ""    -- Just for wiki command
    self.DESCRIPTION    = { }   -- Description of the command
    self.ALIASES        = { }   -- List of aliases this command can have. (No need to add the name of the command)
    self.SYNTAX         = { { "","" }, -- Syntax explaination
        --[[
            -- help command will take of this:
            -- [1] is arguments and [2] is description of that version of the command
            { "<args>" , "Explained" } /cmdname <args> (Explained)
            { "" , "No arguments needed", }
            { "" , "" }
        ]]--
    }
    self.EXAMPLES       = { -- Examples of commands
        --[[
        { "0 This is my message", } /mail cmdname 0 This is my message
        ]]--
    }
    self.INVALID = 3
    self.PASS    = 2
    self.SUCCESS = 1
    self.ERROR   = 0
end

function Command:canUseCommand(Client) -- Move to Command?
    if not Client then
        Console:Debug("Command:canUseCommand Failed to get command", "command")
        return false
    end
    if Client:isConsole() then return self.CONSOLE end
    if Client:isOnline() and ( self.NAME == "login" or self.NAME == "register" ) then return false end
    if not Client:isOnline() and ( self.NAME == "logout" or self.REQUIRE_LOGIN ) then return false end
    if not Client:isAuth() and Client:getLevel() < self.REQUIRE_LEVEL then return false end
    if not Client:isAuth() and self.REQUIRE_AUTH then return false end
    if not Client:isConsole() and not Client:isAuth() and self.REQUIRE_FLAG ~= "" and et.G_shrubbot_permission and et.G_shrubbot_permission( Client.id, self.REQUIRE_FLAG ) == 0 then
        return false
    end
    return true
end

function Command:getCommandType()
    if self.NAME == "help" then return "default" end
    if self.MAIL then return "mail" end
    if self.REQUIRE_AUTH then return "auth" end
    if self.REQUIRE_LOGIN then return "lua" end
    if self.OVERWRITE then return "overwrite" end
    if self.REQUIRE_LEVEL >= Config.Level.Admin or self.REQUIRE_FLAG ~= '' then return "admin" end
    return "default"
end

function Command:getCommandArg0()
    if self.NAME == "help" then return "/" .. Config.Prefix.Lua .. " " end
    if self.MAIL then
        return "/" .. Config.Prefix.Mail .. " "
    elseif self.REQUIRE_AUTH then
        return "/" .. Config.Prefix.Auth .. " "
    elseif self.REQUIRE_LOGIN then
        return "/" .. Config.Prefix.Lua .. " "
    elseif self.REQUIRE_LEVEL > 0 then -- Don't want self.ADMIN check here, because that doesn't really matter
        return "/" .. Config.Prefix.Admin
    elseif self.CONSOLE and not self.CHAT and not self.ADMIN then
        return Config.Prefix.Lua .. " "
    elseif self.LUA and not self.CHAT and not self.ADMIN then
        return "/" .. Config.Prefix.Lua .. " "
    elseif self.CHAT then
        return Config.Prefix.Chat
    elseif self.ADMIN then
        return "/" .. Config.Prefix.Admin
    end
end

function Command:getColor()
    return Color[self:getCommandType()]
end

function Command:InfoMessage( Client , message )
    if not Client or not message then return end
    message = self:getColor() .. string.lower(self.NAME) .. Color.Secondary .. ": ".. Color.Primary .. message
    if Client:isConsole() then
        Client:InfoClean( message )
    else
        Client:ChatClean( message )
    end
end

function Command:InfoMessageAll( message )
    if not message then return end
    message = self:getColor() .. string.lower(self.NAME) .. Color.Secondary .. ": ".. Color.Primary .. message
    Console:ChatClean(message)
    Console:InfoClean(message)
end

function Command:InfoMessageClean( Client , message )
    if not Client or not message then return end
    if Client:isConsole() then
        Client:InfoClean(message)
    else
        Client:PrintClean(message)
    end
end

function Command:ErrorMessage( Client , message )
    if not Client or not message then return end
    message = self:getColor() .. string.lower(self.NAME) .. Color.Secondary .. ": ".. Color.Error .. message
    if ( Client:isConsole() ) then
        Client:Error(message)
    else
        Client:ChatClean(message)
    end
end

function Command:Call(Client,Args,CommandType)
    self.Info = function (self,message)
        self:InfoMessage(Client,message)
        return self.SUCCESS
    end
    self.InfoClean = function (self,message)
        self:InfoMessageClean(Client,message)
        return self.SUCCESS
    end
    self.InfoAll = function (self,message)
        self:InfoMessageAll(message)
        return self.SUCCESS
    end
    self.Error = function (self,message)
        self:ErrorMessage(Client,message)
        return tonumber(errcode) or self.ERROR -- Able to return self:Error, saves me one line in a lot of palces
    end
    self.getHelp = function (self)
        return CommandHandler:getHelp(Client,self.NAME)
    end
    if not self.OVERWRITE and not Client:isConsole() then
        if ( Client:getLevel() < self.REQUIRE_LEVEL and not Client:isAuth() ) then
            self:Error("You do not have the required level to use this command")
            Client:Play(Config.Sound.Error)
            return self.SUCCESS
        end
        if self.REQUIRE_LOGIN then
            if not Client:isOnline() then
                self:Error("You need to be logged in to use this command")
                Client:Play(Config.Sound.Error)
                return self.SUCCESS
            end
            if self.REQUIRE_AUTH and not Client:isAuth() then
                self:Error("You are not authorized to use this command")
                Client:Play(Config.Sound.Error)
                return self.SUCCESS
            end
        end
    end
    if Misc:triml(Args[1]) == "help" and self.NAME ~= "help" then return self:getHelp() end    -- Signal help command when help is first argument in command
    if self.NAME == "help" and not Args[1] and CommandType == "mail" then Args[1] = "mail" end -- Signal list mail commands from help
    
    local status , returncode = pcall(self.Run,self,Client,Args)
    if not returncode then returncode = 1 end
    if not status then
        self:Error("Error running command")
        if not Client:isConsole() and Client:isAuth() then self:Error(returncode) end
        LOG_ERROR("LESM:Command(" .. self.NAME .. ")",returncode)
        return self.ERROR
    else
        
        if ( ( not self.OVERWRITE and Game:Shrubbot() ) or not Game:Shrubbot() ) and not Client:isConsole() then
            if ( returncode == self.ERROR or returncode == self.INVALID ) then
                Client:Play(Config.Sound.Error)
            elseif ( returncode == self.SUCCESS ) then
                Client:Play(Config.Sound.Command)
            end
        end
        
        if not self.OVERWRITE then returncode  = self.SUCCESS end
        if not Game:Shrubbot() then returncode = self.SUCCESS end
        if self.OVERWRITE and ( CommandType ~= "chat" and CommandType ~= "admin" ) then returncode = self.SUCCESS end
        if returncode ~= self.ERROR and returncode ~= self.SUCCESS then returncode = self.ERROR end
        
        Console:Debug("Command:Call " .. self.TITLE .. " has returncode:[" .. tostring(returncode) .. "] cmdtype:[" .. tostring(CommandType) .. "]","command")
        return returncode
    end
end

function Command:Run()
    -- Raise some sort of error
    self:Error(Console,"Undefined Run method")
    --print("Undefined Run method in Command " .. tostring(self.NAME) )
    return 0
end