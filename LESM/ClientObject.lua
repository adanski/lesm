ClientObject = class("ClientObject")

local _xpSetLevel = function ( self, newLevel )
    if newLevel <= self:getLevel() then return end
    if not self:setLevel(newLevel) then return end
    local xp = Levels[newLevel]
    Sound:playAll( Config.Sound.Autolevel , self )
    local autolevel_message = Config.AutoLevel.Message
    autolevel_message = string.gsub(autolevel_message,"<xp>",xp)
    autolevel_message = string.gsub(autolevel_message,"<newlevel>",newLevel)
    
    Console:IteratePrint(autolevel_message,Config.AutoLevel.Location,"autolevellocation",self,true)
end

local _getTimeout = function(autoLogin)
    local currentTime = os.time() 
    if autoLogin then
        return currentTime + ( 24 * 3 * 60 * 60 ) -- 3 days
    else
        return currentTime + ( 1 * 60 * 60 ) -- 1 hour
    end
end

function ClientObject:initialize(clientId)
    self.id               = tonumber(clientId)
    self.User             = nil
    self.firstTime        = true
    self.begin            = false
    self.firstConnect     = false
    self.spawnWalkthrough = false
    self.isReady          = false
    self.hasVoted         = false
    self.skipNext         = false
    self.isAfk            = false
    self.invalid          = true
    self.freeze           = false
    self.freezeLocation   = {}
    self.rudclient        = nil
    self.afkReason        = ""
    self.etpro_guid       = ""
    self.reports          = 0
    self.following        = -1
    self.authFollowing    = -1
    self.team             = 0
    self.teamSwitches     = 0
    self.spawnTimer       = -1
    self.spam             = 0.0
    self.spamMultiplier   = 1.0
    self.teamBlock        = 0.0
    self.lastTeamBlock    = 0.0
    self.entityMessage    = 0.0
    self.pingMin          = -1
    self.pingMax          = -1
    self.maxChars         = 85 -- etpro needs 87 for r_mode 4 - 2 for newline
    self.savePoint        = { }
    self.pings            = { }
    self.cvars            = { }
    self.disableTeam      = false
    self.name             = ""
    self.tagRenames       = 0
    self.alteredPing      = false
    self.lastPulse        = 0
    self.aliveState       = 0
    self.lastkill         = 0
    self.killchain        = 0
    self.killspree        = 0
    self.printTable       = { }
    self.confirm          = {
        teamSwitch = false,
        mailDelete = false,
        mailEmpty  = false,
    }
end


------------------
-- # Features # --
------------------

--- Displays a message when client connects
-- FEATURE ConnectMessage
-- Possibility of having empty stuff. like Username: <userkey(username)> would be Username: for non loggedinplayers
-- Should I add 'greeting' key to use in this situation? if exists use that instead of connect print
function ClientObject:connectMessage()
    local message;
    
    if self:isBot() then
        message = Misc:trim(Config.ConnectPrint.MessageBots)
    else
        message = Misc:trim(Config.ConnectPrint.Message)
    end
    
    if message == '' then return end
    
    message = MessageHandler:replace( message , true , self )
    Console:IteratePrint(message,Config.ConnectPrint.Location,"connectlocation",self,true)
end

--- Plays a sound when client connects
-- FEATURE Greeting/GreetingSound
function ClientObject:connectSound()
    if self:isBot() or not self.firstConnect then return end
    if not self:isOnline() then Sound:playAll(Config.Sound.Connect,self); return end -- No custom greeting go back to default
    
    local greeting      = Misc:trim( self.User:getKey("greeting") )
    local greetingsound = Misc:trim( self.User:getKey("greetingsound") , true , true )
    
    if Misc:trim( self.User:getKey("greeting") ,false , true ) ~= "" and Config.Profile.Greetings then -- Make sure greeting isn't empty
        greeting =  MessageHandler:replace( greeting , {
            map=true,mod=true,teams=true,colors=true,
            clientnum=true,clientname=true,clientcountry=true,clientclass=true,
            clientteam=true,clientlevel=true,clientetversion=true,clientprotocol=true,clientping=true,
        } , self )
        Console:IteratePrint( MessageHandler:greeting( greeting , self ) ,Config.Profile.Location,"greetinglocation",self)
    end
    
    if greetingsound ~= "" and Config.Profile.SoundGreetings then
        local soundList = Sound:canUse( self, Sound:find(greetingsound) )
        if next(soundList) then
            Sound:playAll(soundList[1],self)
        end
    end
    Sound:playAll(Config.Sound.Connect,self)
end

--- Plays a sound and message when client disconnects
-- FEATURE Farewell/sounds
function ClientObject:disconnectSound()
    if self:isBot() or not self.begin then return end -- If they haven't at least fully connected then don't play sound.
    if not self:isOnline() then Sound:playAll(Config.Sound.Disconnect,self); return end
    
    local farewell      = Misc:trim( self.User:getKey("farewell") )
    local farewellsound = Misc:trim( self.User:getKey("farewellsound") , true , true )
    
    if Misc:trim( self.User:getKey("farewell") , false , true ) ~= "" and Config.Profile.Farewells then
        farewell =  MessageHandler:replace( farewell , {
            map=true,mod=true,teams=true,colors=true,
            clientnum=true,clientname=true,clientcountry=true,clientclass=true,
            clientteam=true,clientlevel=true,clientetversion=true,clientprotocol=true,clientping=true,
        } , self )
        Console:IteratePrint( MessageHandler:farewell( farewell , self ) ,Config.Profile.Location,"farewelllocation",self)
    end
    
    if farewellsound ~= "" and Config.Profile.SoundFarewells then
        local soundList = Sound:canUse( self, Sound:find(farewellsound) )
        if next(soundList) then
            Sound:playAll(soundList[1],self)
        end
    end

    Sound:playAll(Config.Sound.Disconnect,self)
end

--- Adds current Clients ping to Client.pings table if active
-- FEATURE PingMonitor
function ClientObject:pingChange()
    if self:isActive() and not self.alteredPing then
        self.pings[tlen(self.pings)+1] = self:entGet("ps.ping")
    end
end

--- Levels up player based on their xp
-- ( Requires Config.AutoLevel )
-- FEATURE AutoLevel
function ClientObject:xpCheckLevel()
    if not Config.AutoLevel.Enabled then return end
    if     self:isBot()     then return end
    if not self:isActive()  then return end
    if Config.AutoLevel.RequireLogin and not self:isOnline() then return end
    if not self.rudclient or not self.rudclient.autolevel then return end
    --if not self:setLevel(self:getLevel()) then return end -- failed setlevel test
    -- TODO I had this hear for some reason, looks rather silly to me :p
    local xp = self:getTotalXp()
    for k=1, tlen(Levels) do
        if xp >= Levels[k] then
            if k == tlen(Levels) then
                _xpSetLevel(self,k)
            else
                if xp < Levels[k+1] then _xpSetLevel(self,k) end
            end
        end
    end
end

--- Follows the target after death
-- See: commands/follow
-- FEATURE FollowAfterDeath
function ClientObject:checkFollow()
    --Console:Chat(self:getName() .. " is alive = " .. self:isAlive())
    if self:isAlive() ~= 1 then return end
    for k=1, tlen(Clients) do
        local Client = Clients[k]
        if ( Client:getTeam() == et.TEAM_SPEC and Client.following == self.id ) then
            Client:entSet( "sess.spectatorState", 2)
            Client:entSet( "sess.spectatorClient", self.id)
        elseif Client:isAuth() and self.id == Client.authFollowing and Client:isAlive() == 3 then
            Client:entSet( "sess.spectatorState", 2)
            Client:entSet( "sess.spectatorClient", self.id)
        end
    end
end

--- Checks for clients near you with specified low health
-- FEATURE PulseHealth
-- TODO Update for ammo/health/ other things
function ClientObject:pulseLowHealth()
    if not self:isOnline() then return end
    if not self:getClass() == et.CLASS_MEDIC then return end
    if not self:isAlive() == 1 then return end
    local pulsehealth   = self.User:getKey("pulsehealth")
    local pulsedistance = self.User:getKey("pulsedistance")
    if pulsedistance == 0 then return end
    if pulsehealth == 0 then return end
    local currentTime = os.time()
    if ( self.lastPulse + self.User:getKey("pulsetime") ) > currentTime then return end
    for k=1, tlen(Clients) do
        if ( self.id ~= Clients[k].id and self:getTeam() == Clients[k]:getTeam() ) then
            if Clients[k]:isAlive() == 1 or Clients[k]:isAlive() == 2 then
                local distance    = EntityHandler:getDistance(self.id,Clients[k].id)
                local health      = Clients[k]:getHealth()
                local distancemsg = self:getDistanceMessage( distance )
                
                if ( pulsehealth >= health and pulsedistance >= distance ) then
                    health = tostring(health) .. "hp"
                    if Clients[k]:isAlive() == 2 then health = "dead" end
                    self:PrintLocation(Color.Tertiary .. "(" .. Color.Secondary .."HealthPulse" .. Color.Tertiary .. ") " .. Color.Primary .. Clients[k]:getName() ..  Color.Primary .. " has only " .. Color.Secondary .. health .. Color.Primary .. " " .. Color.Secondary .. distancemsg .. " away" ,"echoclean","pulselocation")
                    self.lastPulse = currentTime
                end
            end
        end
    end
end

--- Adds [length] time to Clients User Profile
-- FEATURE UserProfile
function ClientObject:addTime(length)
    if not self:isOnline() or not self:isActive() then return end
    local currentTimePlayed = tonumber( self.User:getKey("timeplayed") ) or 0
    if currentTimePlayed < 0 then currentTimePlayed = 0 end
    self.User:update("timeplayed",currentTimePlayed + length)
end

--- Add time to client vars
function ClientObject:updateTimeVars( time )
    if not self:isConnected() then return end
    time = tonumber(time) or 0.0
    time = time + 0.0
    if self.teamBlock > 0.0 then self.teamBlock = self.teamBlock - time end
    if self.spam > 0.0 then self.spam = self.spam - time end
    if self.entityMessage > 0.0 then self.entityMessage = self.entityMessage - time end
    if self.teamBlock < 0.0 then self.teamBlock = 0.0 end
    if self.spam < 0.0 then self.spam = 0.0 end
    if self.entityMessage < 0.0 then self.entityMessage = 0.0 end
end

--- Play sound to client
function ClientObject:Play(sound) Sound:play(self,sound) end

--- Display message of the day
-- FEATURE Message of the day
function ClientObject:motd()
    if self:isOnline() and not self.User:getKey('motd') then return end
    
    local motd = MessageHandler:motd(self)
    if not motd then return end
    self:Chat("See console for the message of the day")
    self:Print(motd)
end

--- Automatically login a client to their user
-- See: commands/autologin
-- FEATURE UserProfile , AutoLogin
function ClientObject:autoLogin()
    if self:isBot() or not self:isConnected() or self:isOnline() then return end
    
    if not Config.Profile.AutoCreate and Misc:trim( self:getPassword() , true , true ) == "no_login" then -- If auto create is on prevent then from not logging in.
        self:Print("Opted out of autologin check")
        Console:Info(va("%s opted out of autologin check" , self:getname() ))
        return
    end
    
    local eGuid,sGuid,etproGuid = self:getGuids()
    local ip = self:getIp()
    local User;
    
    if Config.Profile.RequireBoth then -- Require both IP & GUID
        local UserIP        = UserHandler:getWithKey("autologinip",ip)
        local UserGUID      = UserHandler:getWithKey("autologinguid",eGuid)
        local UserSID       = UserHandler:getWithKey("autologinsid",sGuid)
        local UserETProGuid = UserHandler:getWithKey("autologinetpro",etproGuid)
        if UserIP then
            if UserGUID and UserIP.id == UserGUID.id then
                User = UserIP
            elseif UserSID and UserIP.id == UserSID.id then
                User = UserIP
            elseif UserETProGuid and UserIP.id == UserETProGuid.id then
                User = UserIP
            end
        end
    else
        User = UserHandler:getWithKey("autologinip",ip) or UserHandler:getWithKey("autologinguid",eGuid) or UserHandler:getWithKey("autologinsid",sGuid) or UserHandler:getWithKey("autologinsid",etproGuid)
    end
    
    if not User then
        if Config.Profile.AutoCreate then
            UserHandler:autoregister(self)
        else
            self:DisplayMessage("register")
        end
        return
    end
    
    if not Config.Profile.AutoCreate and not User:getKey("autologin") then
        if not User:checkTimeout() then
            self:DisplayMessage("register")
            return
        end
    end
    
    self.User  = User
    self.isAfk = self.User:getKey('afk')
    MessageHandler:adminwatch(Color.Secondary .. "AUTOLOGIN" .. Color.Tertiary .. "#" .. Color.Primary .. self:getName() .. Color.Tertiary .. "->" .. Color.Secondary .. self.User:getKey('username'),0,self.id,false)
    self:Print("You were automatically logged in as " .. Color.Secondary .. self.User:getKey('username'))
end

--- Checks current ammo status and notifys if they are low
-- Needs a little more support for other mods
-- FEATURE AmmoCheck
-- FIXME FINISH AMMOCHECK
function ClientObject:ammoCheck()
    if not self:isActive() or not self:isOnline() or self:isAlive() ~= 1 then return end
    if not self.User:getKey("ammocheck") then return end
    
    local   weapon = self:entGet( "s.weapon" )
    local     ammo = self:entGet( "ps.ammo" , weapon )
    local ammoclip = self:entGet( "ps.ammoclip" , weapon )
    local Weapon = WeaponHandler:getWithId( weapon )
    if not Weapon then return end
    if Weapon.ac_ammo < 0 or Weapon.ac_ammoclip < 0 then return end
    local ammopercent     = ammo     / Weapon.ac_ammo
    local ammoclippercent = ammoclip / Weapon.ac_ammoclip
    if ( ammoclippercent == 0.0 and ammopercent == 0.0 ) then
        self:PrintLocation("^8*** ^4-^1 No Ammo ^4- ^8***","centerprintclean","ammochecklocation")
    elseif ( ammoclippercent <= 20.0  and ammopercent >= 20.0 ) then
        self:PrintLocation("^7*** ^4-^3 Reload ^4- ^7***","centerprintclean","ammochecklocation")
    else
        self:PrintLocation("^7*** ^4-^1 Low Ammo ^4- ^7***","centerprintclean","ammochecklocation")
    end
end

--- A more advanced version of etpro's timerset
-- Counts down enemy spawntime when command is input
-- This is usually off by 1-1000 msecs, Have tried fixing a bunch but cannot find a good way to be accurate
-- See: commands/spawn
-- Disabled
-- FEATURE SpawnTimer
--[[
function ClientObject:spawnTimerCheck()
    if ( self.spawnTimer == -1 ) then return end
    if not ( self:isActive() ) then
        self.spawnTimer = -1
        return
    end
    if ( self.spawnTimer == 5 ) then -- 5 seconds
        self:Play("sound/lua/spawn1sec.wav") -- 5 seconds
        self:CenterPrintClean("^7*** ^4-^2 5 ^4- ^7***")
    elseif ( self.spawnTimer  == 3 ) then -- 3 seconds
        self:Play("sound/lua/spawn1sec.wav") -- 3 seconds
        self:CenterPrintClean("^7*** ^4-^3 3 ^4- ^7***")
    elseif ( self.spawnTimer == 2 ) then -- 2 seconds
        self:Play("sound/lua/spawn1sec.wav") -- 2 seconds
        self:CenterPrintClean("^7*** ^4-^8 2 ^4- ^7***")
    elseif ( self.spawnTimer == 1 ) then -- 1 seconds
        self:Play("sound/lua/spawn1sec.wav") -- 1 seconds
        self:CenterPrintClean("^7*** ^4-^1 1 ^4- ^7***")
    elseif ( self.spawnTimer <= 0 ) then
        if ( self:getTeam() == 1 ) then
            self.spawnTimer = ( Game:AlliedRespawn() )
        elseif ( self:getTeam() == 2 ) then
            self.spawnTimer = ( Game:AxisRespawn() )
        else
            self.spawnTimer = -1
            self:Print("Spawntimer disabled")
            return
        end
        -- Other Team Spawned
        self:Play("sound/lua/spawn0sec.wav")
        self:ChatClean("^7*** ^4-^1 SPAWNED ^4- ^7***")
        return
    end
    self.spawnTimer = ( self.spawnTimer - 1 )
end]]--

--- Sets Clients Secondary to a single pistol or SMG if available
-- TODO Use et weapon constants
-- Not tested yet
-- FEATURE SinglePistol
function ClientObject:checkSecondary()
    if not self:isOnline() or not self.User:getKey("singlepistol") or self:getTeam() == et.TEAM_SPEC then return end
    
    if self:getTeam() == et.TEAM_ALLIES then
        if self:getClass() == et.CLASS_SOLIDER and self:getSkillRank(et.CLASS_SOLIDER) == 4 then
            self:latchSecondaryWeapon(8)  -- Thompson
        elseif self:getClass() == et.CLASS_COVERT then
            self:latchSecondaryWeapon(43) -- Silenced Colt
        else
            self:latchSecondaryWeapon(7)  -- Colt
        end
    elseif self:getTeam() == et.TEAM_AXIS then
        if self:getClass() == et.CLASS_SOLIDER and self:getSkillRank(et.CLASS_SOLIDER) == 4 then
            self:latchSecondaryWeapon(3)  -- Mp40
        elseif self:getClass() == et.CLASS_COVERT then
            self:latchSecondaryWeapon(14) -- Silenced Luger
        else
            self:latchSecondaryWeapon(2)  -- Luger
        end
    end
end

---------------
-- XP FUNCTIONS
---------------

--- Gets Client's total XP
function ClientObject:getTotalXp()
    local xp = 0.0
    local fname = "sess.skillpoints"
    if Game:Mod() == 'etpro' then fname = 'sess.skill' end
    for k=0,6 do
        xp = xp + self:entGet( fname , k )
    end
    xp = xp + 0.0
    return Misc:int(xp)
end

--- Gets Client's skill rank (level in skill)
function ClientObject:getSkillRank(skill)
    skill = tonumber(skill)
    if not skill or skill < 0 or skill > 6 then return nil end
    
    return self:entGet( 'sess.medals' , skill )
end

--- Gets Client's XP of a specific skill
function ClientObject:getXpSkill(skill)
    if not skill or skill > 6 or skill < 0 then return 0.0 end
    local fname = "sess.skillpoints"
    if Game:Mod() == 'etpro' then fname = 'sess.skill' end
    return self:entGet( fname , skill ) or 0.0
end

--- Sets Client's XP of a specific skill
function ClientObject:setXpSkill(xp,skill)
    xp    = xp or 0.0
    xp    = xp + 0.0
    skill = tonumber(skill)
    if not skill then return end
    if self:getXpSkill(skill) > xp then return end -- Don't want to set their skill lower than they already have
    et.G_XP_Set( self.id, Misc:int( xp ) , skill , 0 )
end

--- Get a lua table of xp skills
-- NOTE: this will start at 1 and end at 7
function ClientObject:getSkillTable()
    return {
        self:getXpSkill(et.SKILL_SENSE),
        self:getXpSkill(et.SKILL_ENGINEER),
        self:getXpSkill(et.SKILL_MEDIC),
        self:getXpSkill(et.SKILL_SIGNAL),
        self:getXpSkill(et.SKILL_LIGHT),
        self:getXpSkill(et.SKILL_HEAVY),
        self:getXpSkill(et.SKILL_COVERT),
    }
end

--- Loads Client's XP That was saved in their UserProfile
-- ( Requires Config.XpSave )
-- FEATURE XpSave , UserProfile , NoShrubbot
function ClientObject:loadXp()
    if not Config.XpSave.Enabled then return end
    if     Config.XpSave.RequireLogin and not self:isOnline() then return end
    if not Config.XpSave.Bots and self:isBot() then return end
    Console:Debug("[xpsave] Attempting load xpsave for " .. self:getName(),"xpsave")
    
    local skills = { }
    if self:isOnline() then
        skills = self.User.skills
    else
        if not self.rudclient then
            Console:Warning( va("%s could not load xp because rudclient was not created yet" , self:getName() ))
            return
        end
        skills = self.rudclient.skills
    end
    if not next(skills) then
        Console:Debug("[xpsave] failed to load xpsave data for " .. self:getName(),"xpsave")
        return
    end
    self:setXpSkill( skills[et.SKILL_SENSE    + 1 ] , et.SKILL_SENSE    )
    self:setXpSkill( skills[et.SKILL_ENGINEER + 1 ] , et.SKILL_ENGINEER )
    self:setXpSkill( skills[et.SKILL_MEDIC    + 1 ] , et.SKILL_MEDIC    )
    self:setXpSkill( skills[et.SKILL_SIGNAL   + 1 ] , et.SKILL_SIGNAL   )
    self:setXpSkill( skills[et.SKILL_LIGHT    + 1 ] , et.SKILL_LIGHT    )
    self:setXpSkill( skills[et.SKILL_HEAVY    + 1 ] , et.SKILL_HEAVY    )
    self:setXpSkill( skills[et.SKILL_COVERT   + 1 ] , et.SKILL_COVERT   )
    et.ClientUserinfoChanged( self.id )
    self:Print("Your xp has been loaded!")
    Console:Debug("[xpsave] load " .. tostring(self.id)  .. " " .. tostring(Misc:table_total(skills)) .. "xp","xpsave")
end

--- Saves Client's XP That was saved in their UserProfile
-- This function is called every 10 seconds in runframe so dont print anything here
-- Don't know if I want RUD_Create (Update) here or not.
-- ( Requires Config.XpSave )
-- FEATURE XpSave , UserProfile , NoShrubbot
function ClientObject:saveXp()
    if not Config.XpSave.Enabled then return end
    if Config.XpSave.RequireLogin and not self:isOnline() then return end
    if not Config.XpSave.Bots and self:isBot() then return end
    if Game:Gamestate() ~= 0 then return end
    Console:Debug("[xpsave] Attempting save xpsave for " .. self:getName(),"xpsave")
    
    local skills = self:getSkillTable()
    if not next(skills) then
        Console:Debug("[xpsave] failed to save xpsave data for " .. self:getName(),"xpsave")
        return
    end
    local current_skills = { }
    if self:isOnline() then
        current_skills = self.User.skills
    else
        if not self.rudclient then
            Console:Warning( va("%s could not save xp because rudclient wasn't created" , self:getName() ))
            return
        end
        current_skills = self.rudclient.skills
    end
    
    if Misc:table_total(current_skills) == Misc:table_total(skills) then
        Console:Debug("[xpsave] not saving data due to current skills being equal for " .. self:getName(),"xpsave")
        return
    elseif Misc:table_total(current_skills) >= Misc:table_total(skills) then
        Console:Debug("[xpsave] not saving data due to current skills being greater for " .. self:getName(),"xpsave")
        return
    end
    
    if self:isOnline() then
        self.User:update("skills",skills)
    else
        if not self.rudclient then
            Console:Warning( va("%s could not save xp because rudclient wasn't created" , self:getName() ))
            return
        end
        self.rudclient.skills = skills
    end
    
    Console:Debug("[xpsave] save " .. tostring(self.id)  .. " " .. tostring(Misc:table_total(skills)) .. "xp","xpsave")
end

--- Gets a converted message based off of distance
-- will use what distance method you have in your user key
function ClientObject:getDistanceMessage( distance )
    local units;
    if not self:isOnline() then
        units = Config.Distance.Measurement
    else
        units = self.User:getKey("distancemeasurement")
    end
    return MessageHandler:distance( units , distance )
end


function ClientObject:checkSpam(time)
    
    if self.spam > 0.0 then
        local message;
        if self.spam < 1.0 then
            local ms = Misc:int( self.spam * 1000.0 )
            message = tostring(ms) .. Color.Primary .. " milliseconds"
        else
            message = tostring( Misc:round( self.spam , 2 ) ) .. Color.Primary .. " seconds"
        end
        self:PrintClean(Color.Secondary .. "spam" .. Color.Tertiary .. ": " .. Color.Primary .. "blocked for " .. Color.Tertiary .. message)
        return true
    else
        time = tonumber(time) or 1.5
        time = time + 0.0
        self:setSpamProtect(time)
        return false
    end
end

--- Querys a cvar
function ClientObject:checkCvar(cvar,func)
    if self:isBot() then return end
    if not et.G_QueryClientCvar then return end
    if type(func) ~= "function" then return end
    cvar = Misc:trim(cvar,true)
    if cvar == "" then return end
    et.G_QueryClientCvar( self.id , cvar)
    RoutineHandler:add( function ()
            if self.cvars[cvar] then -- Only do function if cvar successfully received
                func(self)
            end
        end,0,2, self.id .. " query " .. cvar )
end

function ClientObject:kick(time,reason)
    time   = tonumber(time) or 0
    reason = Misc:trim(reason)
    et.trap_DropClient( self.id, reason, time )
end

function ClientObject:blacklist(blacklist)
    if not self:isConnected() or self:isBot() then return end
    local eGuid,sGuid,etproGuid = self:getGuids()

    blacklist.guid    = eGuid
    if sGuid ~= '' then
        blacklist.guid2 = sGuid
    elseif etproGuid ~= '' then
        blacklist.guid2 = etproGuid
    end
    blacklist.ip      = self:getIp()
    Core:BlacklistAdd(blacklist)

    self:kick(120,blacklist.reason)
end


--- Kicks client if his ping average is above the configured amount
-- ( Requires Config.PingMonitorKick )
-- FEATURE PingMonitor
function ClientObject:pingCheck()
    if not self:isConnected() then return end -- TODO Check zombied players temporarily
    if tlen(self.pings) >= Config.PingMonitor.MinChecks then
        local average = Misc:table_average(self.pings)
        if average >= Config.PingMonitor.Ping then
            local cfgBLTime = Misc:getTimeFormat( Config.PingMonitor.BlacklistTime )
            if Config.PingMonitor.Spec then
                self:setTeam("spectator")
                self:Chat(Config.PingMonitor.Message)
                if self.lastTeamBlock == 0.0 then
                    self.lastTeamBlock = Config.TeamBlock
                    self.teamBlock     = Config.TeamBlock
                else
                    self.teamBlock     = self.lastTeamBlock * Config.TeamBlockMultiplier
                    self.lastTeamBlock = self.teamBlock
                end
            elseif Config.PingMonitor.Warn then
                if not self.rudclient then
                    Console:Error( va( "pingCheck %s no rudclient" , self:getName() ))
                    return
                end -- Saftey check
                RegularUser:warn(self.rudclient, { warner = "Lua Automatic" , reason = Config.PingMonitor.Message } )
            elseif cfgBLTime ~= 0 then
                if cfgBLTime < 0 then cfgBLTime = -1 end
                if cfgBLTime > 1 then cfgBLTime = os.time() + cfgBLTime end
                self:blacklist{ title   = "(Auto) PingMonitor" ,
                                desc    = { "Automatic ping monitor blacklist", "Average Ping: " .. average .. " With " .. tlen(self.pings) .. " checks", },
                                expires = cfgBLTime,
                                banner  = "LUA Automatic",
                                reason  = Config.PingMonitor.Message, }
            end
        end
    end
end

function ClientObject:checkNameMinLength()
    if string.len(Misc:trim(self:getName(), true, true)) < Config.MinNameLength then
        Client:setName(Misc:randomWord())
        MessageHandler:adminwatch("RENAME" .. Color.Tertiary .. "#" .. Color.Primary .. "Name length too short", 0,self.id)
        self:Chat("Your name is too short and you have been renamed to " .. Client:getName())
    end
end
function ClientObject:checkNameRename()
    if not next(Config.Rename) then return end
    local name = Misc:trim(self:getName(), true, true)
    for k=1,tlen(Config.Rename) do
        local name_match = Misc:trim(Config.Rename[k], true, true)
        if name == name_match then
            Client:setName(Misc:randomWord())
            MessageHandler:adminwatch("RENAME" .. Color.Tertiary .. "#" .. Color.Primary .. "Name not allowed in config", 0,self.id)
            self:Chat("Your name is not allowed, you have been renamed to " .. Client:getName())
            return
        end
    end
end
function ClientObject:checkNameClanTag()
    if not Config.ClanTag.Enabled then return end
    if self:getLevel() >= Config.ClanTag.Level then return end
    if not next(Config.ClanTag.Tags) then return end
    local name = self:getName()
    if not Config.ClanTag.Exact then name = Misc:trim(name, true, true) end
    local tagmatch;
    for k=1,tlen(Config.ClanTag.Tags) do
        if Config.ClanTag.Exact then
            if string.find(name, Config.ClanTag.Tags[k],1,true) then
                tagmatch = Config.ClanTag.Tags[k]
                break;
            end
        else
            local tagstr = Misc:trim(Config.ClanTag.Tags[k],true,true)
            if string.find(name, tagstr, 1, true) then
                tagmatch = tagstr
                break;
            end
        end
    end
    if tagmatch then
        self.tagRenames = self.tagRenames + 1
        self:setName(string.gsub(name, Misc:escape(tagmatch), ""))
        MessageHandler:adminwatch("RENAME" .. Color.Tertiary .. "#" .. Color.Primary .. "Clantag " .. tagmatch .. Color.Primary .. " in name " .. Color.Secondary .. self.tagRenames .. Color.Tertiary .. "/" .. Color.Secondary .. Config.ClanTag.MaxRenames .. Color.Primary .. " renames", 0,self.id)
        self:Chat(Config.ClanTag.Message) -- TODO MessageFilter?
        if self.tagRenames > Config.ClanTag.MaxRenames then
            local blacklist_time = Misc:getTimeFormat(Config.ClanTag.BlacklistTime)
            if blacklist_time ~= 0 then
                if cfgBLTime < 0 then cfgBLTime = -1 end
                if cfgBLTime > 1 then cfgBLTime = os.time() + cfgBLTime end
                self:blacklist{ title   = "(Auto) ClanTag" ,
                                desc    = { "Automatic clantag blacklist",
                                            "Name: " .. nameMatch .. " matched with " .. match, },
                                expires = cfgBLTime,
                                banner  = "LUA Automatic",
                                reason  = Config.ClanTag.Message, }
            elseif Config.ClanTag.Warn then
                if not self.rudclient then
                    Console:Error( va( "validateName %s no rudclient" , self:getName() ))
                    return
                end -- Saftey check
                RegularUser:warn(self.rudclient,{ warner = "Lua Automatic" , reason = Config.ClanTag.Message })
            end
        else
            self:Chat(Color.Secondary .. self.tagRenames .. Color.Tertiary .. "/" .. Color.Secondary .. Config.ClanTag.MaxRenames .. Color.Primary .. " renames")
        end
    end
end
--- If name is etplayer then rename the player to a random name
-- Also renames if the cleanstring(Without color codes) is empty, or was just spaces
-- ( Requires Config.RenameETPlayer )
-- FEATURE ETPlayerRename
function ClientObject:validateName()
    if self:isBot() or not self:isConnected() then return end
    self:checkNameMinLength()
    self:checkNameRename()
    self:checkNameClanTag()
end

function ClientObject:validateGuid()
    local eGuid,sGuid,etproGuid = self:getGuids()
    
    if Game:Mod() == "silent" and sGuid == '' then
        Console:Warning(va("%s silent id not yet registered" , self:getName() ))
        self.invalid = true
        RoutineHandler:add( function () self:validateGuid() end,0,5, "Getting new silentid for " .. self:getName() )
        return
    elseif Game:Mod() == "etpro" and etproGuid == '' then
        Console:Warning(va("%s etpro guid not yet registered" , self:getName() ))
        self.invalid = true
        RoutineHandler:add( function () self:validateGuid() end,0,5, "Getting new etpro guid for " .. self:getName() )
        return
    elseif Game:Mod() == "legacy" and eGuid == '' then
        -- TODO its possible a player connets without PB enabled.
        -- in this case the client will never be validated.
        -- Perhaps make an option to fallback to IP Identity after so many attempts.
        -- Or they can register to have it automatically identify as ip?
        Console:Warning(va("%s legacy guid not yet registered" , self:getName() ))
        self.invalid = true
        RoutineHandler:add( function () self:validateGuid() end,0,5, "Getting new legacy guid for " .. self:getName() )
        return
    end
    
    if self.invalid then
        Console:Info( va("%s is now valid",self:getName() ))
        self.invalid = false
        self:clientBegin()
    end
end

--- Client has been killed
-- A sort of callback
function ClientObject:died()
    --[[if self.killspree >= 5 then
        Console:Chat(va("%s" .. Color.Primary .. "'s " .. Color.Tertiary .. "%d" .. Color.Primary .." killing spree was ended",self:getName(),self.killspree) )
    end--]]
    self.lastkill         = 0
    self.killchain        = 0
    self.killspree        = 0
    self:checkSecondary()
    if self:isAuth() and self.authFollowing ~= -1 then -- Likely don't even need this because we arent gibbed.
        local Target = ClientHandler:getClient(self.authFollowing)
        if Target and Target:isAlive() == 1 then -- Just to verify we are following right person
            self:entSet( "sess.spectatorState"  , 2 )
            self:entSet( "sess.spectatorClient" , Target.id )
        else
            self.authFollowing = -1
        end
    end
end

--- Logs the client out
-- If called from logout command [cmd] will be true
-- See: commands/logout
-- Needs to be moved and possibily redesigned
function ClientObject:Logout( fromCommand )
    if not self:isOnline() then
        self:Error("You are currently not logged in")
        return
    end
    if tlen(self.pings) >= 10 then
        local average = 0
        for k=1,tlen(self.pings) do
            average = ( average + self.pings[k] )
        end
        average = ( average / tlen(self.pings) )
        while ( tlen(self.User.pings) >= 100 ) do
            table.remove(self.User.pings,1)
        end
        self.User.pings[tlen(self.User.pings)+1] = average
    end
    self.User:updateFields(self)
    self:saveXp()
    if fromCommand then
        if self.User:getKey("autologin") then
            self:Print("You still have autologin enabled, you will be automatically logged in next connect")
        else
            self.User:update("timeout",0) -- No autologin
        end
        
        self.User:update("afk",false)
        self.User:update("afkreason","")
        self.User:update("afktime",0)
        self:Play(Config.Sound.Logout)
    end
    self.User = nil
end

function ClientObject:clientBegin()
    self:validateName()
    self:autoLogin()
    Game:setLastConnect(self,true)

    local rudclient = RegularUser:create(self)
    self:loadXp()

    self:logBegin()

    local blacklist = Core:BlacklistCheck(self)
    if blacklist then self:kick(120,blacklist.reason);return; end
    if RegularUser:checkWarn(rudclient) then return end

    if self.firstConnect then
        for k=1,tlen(Clients) do
            local Client = Clients[k]
            if not Client.invalid then -- Seems like I should not have to check this...
                if Client:isIgnored(self) then
                    Client:ignore(self) -- Just makes sure that the mod side code is set to ignore
                    Client:Echo("Ignored player has connected") --  a little notification in case they forget they ignored.
                elseif Client:isSoundIgnored(self) then
                    Client:Echo("Sound Ignored player has connected") --  a little notification in case they forget they ignored.
                end
            end
        end
        self:connectMessage()
        self:connectSound()
    end

    self:checkSecondary()

    if self:getTeam() ~= et.TEAM_SPEC then
        if Config.Profile.ForceRegister and not self:isOnline() and not self:isBot() then
            self:Chat("You must first register a profile and log in before you can play.")
            self:setTeam("spectator")
            CommandHandler:getHelp(self,"register")
        end
        if self.isAfk then CommandHandler:getCommand("back"):Call(self,{ }) end
    end
    
    if Game:Mod() == "silent" and Config.Profile.AutoWidth > 0 then
        if self:isOnline() and self.User:getKey("maxchars") <= 10 then
            self:checkCvar("r_mode" , Core.rmode_function)
        end
    end
    if self:isOnline() then
        Mail:CheckTotal(self)
    end
    self:motd()
end



--- Displays various longer messages
-- Put into one function in case I want to repeat saying them
function ClientObject:DisplayMessage(messageType)
    messageType = string.lower(messageType)
    if messageType == "register" and Config.DisplayRegister then
        self:Chat("You need to register to gain access to special features.^z(See console for more information)")
        self:Print("Register by typing: " .. Color.Command .. "/register <username> <password>")
        self:Print("Do not use a password you use for other websites , accounts etc..")
        self:Print("Why register?")
        self:Print("Ingame mail - Send and receive message to other users. Also the server admin may post news updates about the server or clan")
        self:Print("Ingame profile - Set your xfire,age,realname,forums name.")
        self:Print("Ingame clan profile - Track your clan members their rank among other things")
        self:Print("Track stats - Such as kills,deaths,teamkills,teamdeaths,botkills,botdeaths")
        self:Print("See when a player was last logged in")
        self:Print("See how long a player has played")
        self:Print("Have access to multitude of extra commands - Regular players get about 20 extra commands. Server Members get around 80")
    end
end

----------------------------
-- # Client Information # --
----------------------------

--- Adds a multiplier to the Client's spam protection
-- See: commands/spamprotect
-- FEATURE SpamProtect
function ClientObject:setSpamProtect(multiplier)
    multiplier = tonumber(multiplier) or 1.0
    multiplier = multiplier + 0.0
    local userExtra = ( Config.SpamProtect * self.spamMultiplier ) - Config.SpamProtect
    local commandMultiplier = ( Config.SpamProtect * multiplier ) - Config.SpamProtect
    self.spam = ( Config.SpamProtect + userExtra + commandMultiplier )
end

--- Gets Client's shrubbot level
-- If no shrubbot then get level from their UserProfile
-- TODO rudclient level
function ClientObject:getLevel()
    if not Game:Shrubbot() then
        if self:isOnline() then
            return self.User:getKey("level")
        else
            return 0
        end
    end
    return tonumber(et.G_shrubbot_level(self.id))
end

--- Sets Client's shrubbot level to [level]
-- If no shrubbot then update their UserProfile
-- TODO rudclient level
function ClientObject:setLevel(level)
    level = tonumber(level)
    if not level then
        Console:Warning("Could not setlevel (no level)" .. self:getName())
        return false
    end
    if level == self:getLevel() then
        Console:Warning("Could not setlevel (same level)" .. self:getName())
        return false
    end
    if not Game:Shrubbot() then
        if self:isOnline() then
            self.User:update("level",level)
            return true
        else
            Console:Warning("Could not setlevel (no user)" .. self:getName())
            return false
        end
    end
    et.trap_SendConsoleCommand( et.EXEC_APPEND, "setlevel ".. self.id.." "..tostring(level).."\n" )
    et.trap_SendConsoleCommand( et.EXEC_APPEND, "readconfig\n" )
    return true
end
--- Returns cl_guid and silentid guid
-- Returns "" if empty
function ClientObject:getGuids()
    local eGuid = self:getGuid()
    if not eGuid or string.len(eGuid) ~= 32 then eGuid = "" end
    local sGuid = self:getSilentId()
    if not sGuid or string.len(sGuid) ~= 32 then sGuid = "" end
    return string.upper(eGuid),string.upper(sGuid),string.upper(self.etpro_guid or "")
end

function ClientObject:getETProGuid() return Misc:trim(self.etpro_guid) end

--- Gets client ping
-- Does not get ping from server browser
function ClientObject:getPing() return tonumber( self:entGet("ps.ping") ) end

--- Sets Client Ping
-- Does not set ping on server browser
-- Does not actually affect networking, just for display on scoreboard
function ClientObject:setPing(ping)
    ping = tonumber(ping)
    if ping == nil then
        return
    elseif ping >= 1000 then
        ping = 999
    elseif ping < 0 then
        ping = 0
    end
    self:entSet("ps.ping",ping)
    self.alteredPing = true
end

--- Checks whether the client is connected
-- Not sure the effectiveness of this on other mods besides silent
function ClientObject:isConnected()
    if tonumber(self:entGet("pers.connected")) == 2 then
        return true
    else
        return false
    end
end

--- Checks if player is currently playing
-- False for bots and spectators
-- False for people still connecting
function ClientObject:isActive()
    if self:isBot() then return false end -- Do I want this here?
    if self:isConnected() then
        if self:getTeam() == 1 or self:getTeam() == 2 then
            return true
        else
            return false
        end
    else
        return false
    end
end

--- Get Client's team ( number )
-- 0 - not on a team
-- 1 - Allies
-- 2 - Axis
-- 3 - Spectator
function ClientObject:getTeam()
    return tonumber(self:entGet("sess.sessionTeam"))
end

--- Sets Client's team to [team]
function ClientObject:setTeam( team )
    team = Core:getTeamNum(team)
    if not team or self:getTeam() == team then return end
    
    --self.skipNext = true
    --self.team     = team
    
    local METHOD = {
        "ref putaxis "   .. self.id .. "\n",
        "ref putallies " .. self.id .. "\n",
        "ref remove "    .. self.id .. "\n",
    }
    if Game:Mod() == "silent" then
        METHOD = {
            "!putteam " .. self.id .. " r\n",
            "!putteam " .. self.id .. " b\n",
            "!putteam " .. self.id .. " s\n",
        }
    end
    et.trap_SendConsoleCommand(et.EXEC_APPEND, METHOD[team])
end

--- Translates team number to a full team name
function ClientObject:getTeamName(team)
    team = tonumber(team)
    if not team then team = self:getTeam() end
    if team == 1 then
        return "Axis"
    elseif team == 2 then
        return "Allies"
    elseif team == 3 then
        return "Spectator"
    else
        return "None"
    end
end

--- Gets full class table
-- Defined in LESM/extra/classes.json
function ClientObject:getClassTable()
    local class = self:getClass()
    for k=1,tlen(Classes) do
        if Classes[k].id == class then
            return Classes[k]
        end
    end
    return nil
end

--- Gets class number of Client
function ClientObject:getClass()
    local class = tonumber(self:entGet("sess.playerType"))
    return class
end

--- Sets clients max health
function ClientObject:setMaxHealth(maxHp)
    self:setHealth(maxHp)
    if Game:Mod() ~= 'silent' then return end
    self:entSet("ps.stats",4,maxHp)
end

--- Switches class to [class] next respawn
function ClientObject:latchClass(class)
    class = tonumber(class)
    if not class then return end
    self:entSet("sess.latchPlayerType",class)
end

--- Switches primary weapon to [weapon] next respawn
function ClientObject:latchPrimaryWeapon(weapon)
    weapon = tonumber(weapon)
    if not weapon then return end
    self:entSet("sess.latchPlayerWeapon",weapon)
end

--- Switches secondary weapon to [weapon] next respawn
function ClientObject:latchSecondaryWeapon(weapon)
    weapon = tonumber(weapon)
    if not weapon then return end
    self:entSet("sess.latchPlayerWeapon2",weapon)
end

--- If Client is currently alive
-- I am not sure about awaiting respawn or if waiting for revive. I think it still is below 0 but I can't remember.
function ClientObject:isAlive()
    if not self:isConnected() or self:getTeam() == 3 then return 0 end -- Not On Team
    local gib_health = 50
    if Game:Mod() == "silent" then
        local limbo_health = tonumber( et.trap_Cvar_Get( "g_forceLimboHealth" ) )
        if limbo_health ~= 0 then gib_health = 100 end
    end
    if self:getHealth() > 0 then
        return 1 -- Alive
    elseif ( self:getHealth() <= 0 and self:getHealth() > gib_health ) then
        return 2 -- In gib state
    elseif self:getHealth() <= gib_health then
        return 3 -- Is dead and gibbed
    end
    return 0
end

--- Checks if is console
-- Sometimes Console is passed instead of Client in places
-- If that is expected I use this
function ClientObject:isConsole() return false end

function ClientObject:ignore(Target)
    if not Target or not Target.rudclient then return end
    Misc:table_addNotExist(self.rudclient.ignored,Target.rudclient.ident)
    if Game:Mod() ~= "etpro" then
        local sessIgnored     = self:entGet("sess.ignoreClients")
        local sessIgnored_bit = bit.tobits( sessIgnored )
        if sessIgnored_bit[Target.id+1] ~= 1 then self:entSet("sess.ignoreClients",sessIgnored + Core.Bits[Target.id+1]) end
    end
end

function ClientObject:unignore(Target)
    if not Target or not Target.rudclient then return end
    Misc:table_remove(self.rudclient.ignored,Target.rudclient.ident)
    if Game:Mod() ~= "etpro" then
        local sessIgnored     = self:entGet("sess.ignoreClients")
        local sessIgnored_bit = bit.tobits( sessIgnored )
        if sessIgnored_bit[Target.id+1] == 1 then self:entSet("sess.ignoreClients",sessIgnored - Core.Bits[Target.id+1]) end
    end
end

function ClientObject:soundIgnore(Target)
    if not Target or not Target.rudclient then return end
    Misc:table_addNotExist(self.rudclient.soundignored,Target.rudclient.ident)
end

function ClientObject:soundUnignore(Target)
    if not Target or not Target.rudclient then return end
    Misc:table_remove(self.rudclient.soundignored,Target.rudclient.ident)
end

--- Gets table of Client's ignored clientNum
function ClientObject:getIgnored()
    local sessIgnored = bit.tobits( self:entGet("sess.ignoreClients") )
    local ignored     = { }
    for k=1,tlen(sessIgnored) do
        if sessIgnored[k] == 1 then ignored[tlen(ignored)+1] = k - 1 end -- Compensate lua to c
    end
    return ignored
end

--- Checks if a particular client is ignored
function ClientObject:isSoundIgnored(Target)
    if not Target or not Target.rudclient then return false end
    if self:isIgnored(Target) then return true end
    if Misc:table_find(self.rudclient.soundignored,Target.rudclient.ident) then return true end
    return false
end

--- Checks if a particular client is ignored
function ClientObject:isIgnored(Target)
    if not Target or not Target.rudclient then return false end
    if Misc:table_find(self.rudclient.ignored,Target.rudclient.ident) then return true end
    
    if Game:Mod() ~= "etpro" then
        local sessIgnored = bit.tobits( self:entGet("sess.ignoreClients") )
        if sessIgnored[ Target.id + 1 ] == 1 then return true end
    end
    
    return false
end


function ClientObject:isStrictMuted()
    if not self.rudclient then
        return false
    else
        return self.rudclient.mutestrict
    end
end

--- Checks whether a player is currently muted
function ClientObject:isMuted()
    -- Not Muted:  Silent(0)
    -- PermaMuted: Silent(-1)
    -- Muted:      Silent(Above 0,seconds from year 2000 + time muted I think...)
    if et.ClientIsFlooding and et.ClientIsFlooding( self.id ) == 1 then return true end -- if they are currently flooding
    if self:entGet("sess.muted") > 0 or self:entGet("sess.muted") == -1 then return true end
    if not self.rudclient then
        return false
    else
        if self.rudclient.mutestrict or self.rudclient.mutetime == -1 or self.rudclient.mutetime > 0 then return true end
        return false
    end
end


--- Sets Client's disguise
-- Client does not need to be covert
-- 8  - disguised, 1 if disguised, 0 otherwise PW_OPS_DISGUISED
-- 9  - information of the covert op PW_OPS_CLASS_1
-- 10 - information of the covert op PW_OPS_CLASS_2
-- 11 - information of the covert op PW_OPS_CLASS_3
-- PArtially works in silent, crashes client if disguise as other client
-- Does not work in legacy
function ClientObject:setDisguise(class,rank,name)
    if Game:Mod() == 'legacy' then return end
    class = tonumber(class)
    rank  = tonumber(rank)
    name  = Misc:trim(name)
    if not class then class = 0 end
    if ( class < 0 ) or ( class >= 4 ) then class = 0 end
    if not rank then rank  = 0 end
    if ( name  == "" ) then name  = "UnnamedPlayer" end
    self:entSet("ps.powerups", 8 , 1)
    self:entSet("ps.powerups", ( 9 + class ) , 1) -- Just a random guess idk...
    
    self:setCSKey("dn",name) -- Disguise Name
    self:setCSKey("dr",rank) -- Disguise Rank
end


function ClientObject:entSet(fieldname,index,value)
    if not fieldname or not index then return end
    if value then
        EntityHandler:safeEntSet(self.id,fieldname,index,value)
    else
        EntityHandler:safeEntSet(self.id,fieldname,index)
    end
end

function ClientObject:entGet(fieldname,index)
    if not fieldname then return end
    if index then
        return EntityHandler:safeEntGet(self.id,fieldname,index)
    else
        return EntityHandler:safeEntGet(self.id,fieldname)
    end
end

function ClientObject:hasObjective()
    local objective = self:entGet("ps.powerups",6)
    if objective == 1 then return true end
    objective = self:entGet("ps.powerups",7)
    if objective == 1 then return true end
    return false
end

--- Gets Client's current health
function ClientObject:getHealth()
    return tonumber(self:entGet("health"))
end

--- Sets Client's current health
function ClientObject:setHealth(health)
    if ( tonumber(health) == nil ) then return end
    self:entSet("health",health)
end

--- Gets Client's current origin (table)
function ClientObject:getOrigin()
    return self:entGet("ps.origin")
end

--- Sets Client's current origin
function ClientObject:setOrigin(origin)
    if (origin == nil ) or ( type(origin) ~= "table" ) then return end
    self:entSet("ps.origin",origin)
end

--- Gets Client's current weapon id
function ClientObject:getWeapon()
    return tonumber(self:entGet("s.weapon"))
end

--- Gets Client's current ammo for [weapon]
-- Ammo that is available to reload
function ClientObject:getAmmo(weapon)
    if ( tonumber(weapon) == nil ) then weapon = self:getWeapon() end
    return tonumber(self:entGet("ps.ammo",weapon))
end

--- Sets Client's current ammo for [weapon]
-- Ammo that is available to reload
function ClientObject:setAmmo(weapon,ammo)
    if ( tonumber(weapon) == nil ) then weapon = self:getWeapon() end
    if ( tonumber(ammo) == nil ) then return end
    self:entSet("ps.ammo",weapon,ammo)
end

function ClientObject:setCharge()
    self:entSet("ps.classWeaponTime",-999999)
end

--- Gets Client's current ammo in clip for [weapon]
-- Ammo that is IN the current clip
function ClientObject:getAmmoClip(weapon)
    if ( tonumber(weapon) == nil ) then weapon = self:getWeapon() end
    return tonumber(self:entGet("ps.ammoclip",weapon))
end

--- Sets Client's current ammo in clip for [weapon]
-- Ammo that is IN the current clip
function ClientObject:setAmmoClip(weapon,ammo)
    if ( tonumber(weapon) == nil ) then weapon = self:getWeapon() end
    if ( tonumber(ammo) == nil ) then return end
    self:entSet("ps.ammoclip",weapon,ammo)
end

--- Gets Client's current adrenaline time in msecs
function ClientObject:getAdren()
    return tonumber(self:entGet("ps.powerups",12))
end

--- sets Client's current adrenaline time in msecs to [adrentime]
function ClientObject:setAdren(adrentime)-- AdrenTime In Ms
    if tonumber(adrentime) == nil then return end
    adrentime = Game:getLevelTime() + tonumber(adrentime)
    self:entSet("ps.powerups",12,adrentime)
end

--- Get Client's userinfo string
function ClientObject:getUserInfo()
    if not self.id then
        Console:Error("Error getting userinfo")
        return ""
    end
    return et.trap_GetUserinfo(self.id)
end

--- Gets Client's protocol and translates it to known versions
function ClientObject:getProtocol()
    local protocol = et.Info_ValueForKey( self:getUserInfo(), "protocol" )
    if (protocol == nil) then
        return "missing protocol"
    elseif (protocol == "") then
        return "empty protocol"
    elseif (protocol == "82") then
        return "2.55"
    elseif (protocol == "83") then
        return "2.56"
    elseif (protocol == "84") then
        return "2.60b"
    else
        return "unknown protocol ( "..protocol..")"
    end
end

--- Gets Client's et version
function ClientObject:getVersion()
    -- cg_etVersion
    local version = et.Info_ValueForKey( self:getUserInfo(), "cg_etVersion" )
    if ( version == nil ) then
        return "missing version"
    elseif ( version == "" ) then
        return "empty version"
    else
        return version
    end
end

--- Gets Client's ConfigString
function ClientObject:getCS()
    if ( et.CS_PLAYERS ~= nil ) then return et.trap_GetConfigstring( et.CS_PLAYERS + self.id ) end
    return et.trap_GetConfigstring( 689 + self.id )
end

--- Sets Client's ConfigString
function ClientObject:setCS(configstring)
    if ( et.CS_PLAYERS ~= nil ) then return et.trap_SetConfigstring( et.CS_PLAYERS + self.id , configstring ) end
    return et.trap_SetConfigstring( 689 + self.id , configstring )
end

--- Gets Client's ConfigString [key]
function ClientObject:getCSKey(key)
    local value = et.Info_ValueForKey( self:getCS(), key )
    return value
end

--- Sets Client's ConfigString [key] to [value]
function ClientObject:setCSKey(key,value)
    local new_cs = et.Info_SetValueForKey( self:getCS(), key , value )
    self:setCS(new_cs)
end

--- Gets Client's mac address
-- Only works in etpub
function ClientObject:getMac()
    if ( Game:Mod() ~= "etpub" ) then return nil end
    return et.Info_ValueForKey( self:getUserInfo(), "mac" )
end

--- Gets Client's ip (And removes the trailing port number)
function ClientObject:getIp()
    return string.gsub(et.Info_ValueForKey( self:getUserInfo(), "ip" ), ":%d*","")
end

--- Gets Client's password that they connected with
function ClientObject:getPassword()
    return et.Info_ValueForKey( self:getUserInfo(), "password" )
end

--- Gets Client's name with color codes
function ClientObject:getName()
    return et.Info_ValueForKey( self:getUserInfo(), "name" )
end

-- Get client name without color codes
function ClientObject:getCleanName()
    return Misc:trim( et.Q_CleanStr( self:getName() ) )
end

-- Get client name with rank
function ClientObject:getNameWithRank()
    local name  = self:getName()
    local level = self:getLevel()
    if level < Config.Level.Admin then
        return name
    elseif level >= Config.Level.Admin and level < Config.Level.Leader then
        return "ranking member " .. name
    elseif level >= Config.Level.Leader then
        return "ranking officer " .. name
    end
end

--- Sets Client's name to [name]
function ClientObject:setName(name)
    if ( name == nil ) then return end
    if ( string.len(Misc:trim(et.Q_CleanStr(name))) <= 0 ) then return end
    local userinfo = self:getUserInfo()
    userinfo = et.Info_SetValueForKey( userinfo, "name", name )
    et.trap_SetUserinfo( self.id, userinfo )
    et.ClientUserinfoChanged( self.id )
end

--- Gets cl_guid (etkey) guid
function ClientObject:getGuid()
    return Misc:trim(et.Info_ValueForKey( self:getUserInfo(), "cl_guid" ))
end

--- Gets silentid guid from entity data
function ClientObject:getSilentId()
    if Game:Mod() ~= "silent" then return nil end
    local sil_guid = self:entGet("sess.guid")
    sil_guid       = Misc:trim(string.gsub(sil_guid, ":%d*",""))
    if string.len(sil_guid) == 32 then
        return sil_guid
    else
        return nil
    end
end

--- Gets silentid guid from userinfo
function ClientObject:getUiSilentId()
    if ( Game:Mod()~= "silent" ) then return end
    local sil_guid = Misc:trim(et.Info_ValueForKey(self:getUserInfo(), "sil_guid"))
    sil_guid = string.gsub(sil_guid, ":%d*","")
    if ( string.len(sil_guid) == 32 ) then
        return sil_guid
    else
        return nil
    end
end

--- Checks if Client is a bot or not
function ClientObject:isBot()
    if (string.lower(self:getIp()) == "localhost") then
        return true
    else
        return false
    end
end

--- Gets country name from country code
-- ( Requires GeoIp.dat , g_countryFlags 1 , LESM/extra/Countries.json )
function ClientObject:getCountry()
    local uci = tonumber(self:entGet("sess.uci"))

    if (Countries[uci] ~= nil) then
        return Countries[uci]
    else
        return "Unknown"
    end
end
-----------------------------------------
-- # Client User Profile Information # --
-----------------------------------------

--- Checks whether a UserObject is linked to this ClientObject
function ClientObject:isOnline()
    if ( self.User ~= nil ) then
        return true
    else
        return false
    end
end

--- Checks whether user is online and has authorize key == true
function ClientObject:isAuth()
    if not ( self:isOnline() ) then return false end
    return self.User:getKey("authorize")
end

-- TODO Figure out actual line length
function ClientObject:getMaxRows()
    local maxrows = 25
    if self:isOnline() then
        maxrows = self.User:getKey('maxrows')
    end
    if maxrows < 10 then maxrows = 10 end
    return maxrows
end

function ClientObject:getMaxChars()
    local maxchars = self.maxChars
    if self:isOnline() then
        maxchars = self.User:getKey('maxchars')
        if maxchars <= 10 then
            maxchars = self.maxChars -- This will set it to the automatic amount
        end
    end
    if maxchars < 64 then maxchars = 64 end
    return maxchars - 2 -- NOTE -2 because of new line character
end

-- # Client Logs # --

function ClientObject:logConnect()
    if self:isBot() then
        Logger:Log( "bot_connect" , self.id .. " " .. et.Q_CleanStr(self:getName()))
        return
    end
    local log_guid = "NO_GUID"
    local eGuid,sGuid,etproGuid = self:getGuids()
    if sGuid ~= '' then
        log_guid = sGuid
    elseif etproGuid ~= '' then
        log_guid = etproGuid
    elseif eGuid ~= '' then
        log_guid = sGuid
    end
    local country = string.gsub(self:getCountry()," ","_")
    Logger:Log( "connect" , self.id .. " " .. log_guid .. " " .. self:getIp() .. " " .. country .. " " .. self:getName() )
end

function ClientObject:logBegin()
    if self:isBot() then
        Logger:Log("bot_begin",self.id)
        return
    end
    local username = ""
    if self:isOnline() then username = self.User:getKey("username") end
    Logger:Log("begin",self.id .. " " .. username)
end
-------------------------------
-- # Printing/Chat Methods # --
-------------------------------

local _printOneOrMultiLine = function( message , length , Client ) -- Now using va instead of the ... method too much incompatibility
    length = tonumber(length)
    if not length or not message then return nil end
    
    -- Not sure whats going on here one of these is having too many args
    -- but seperating them made it not error
    local y = MessageHandler:sformat('',Client)
    local x = et.Q_CleanStr( y )
    local paddingLength = string.len( x )
    
    if not string.find( message , "\n" ) and ( string.len( et.Q_CleanStr( message ) ) + paddingLength ) <= length then
        return message
    else
        return Misc:FormatLongString( message , length - paddingLength )
    end
end

function ClientObject:Chat( message )
    message = _printOneOrMultiLine( message , et.MAXCHARS_CHAT , self  )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( self.id, "chat \"" .. MessageHandler:sformat(message , self ) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( self.id, "chat \"" .. MessageHandler:sformat(message[k] , self ) .. Color.WHITE .. "\"")
        end
    end
end

function ClientObject:Print( message )
    message = _printOneOrMultiLine( message , et.MAXCHARS_PRINT , self )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( self.id, "print \"" .. MessageHandler:sformat(message , self ) .. Color.WHITE .. "\n\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( self.id, "print \"" .. MessageHandler:sformat(message[k] , self ) .. Color.WHITE .. "\n\"")
        end
    end
end

function ClientObject:Echo( message )
    message = _printOneOrMultiLine( message , et.MAXCHARS_ECHO , self )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( self.id, "echo \"" .. MessageHandler:sformat(message , self ) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( self.id, "echo \"" .. MessageHandler:sformat(message[k] , self ) .. Color.WHITE .. "\"")
        end
    end
end

function ClientObject:Banner( message )
    message = _printOneOrMultiLine( message , et.MAXCHARS_BANNERPRINT , self )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( self.id, "bp \"" .. MessageHandler:sformat(message , self ) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( self.id, "bp \"" .. MessageHandler:sformat(message[k] , self ) .. Color.WHITE .. "\"")
        end
    end
end

function ClientObject:CenterPrint( message )
    message = _printOneOrMultiLine( message , et.MAXCHARS_CENTERPRINT , self )
    
    if type( message ) == "string" then
        et.trap_SendServerCommand( self.id, "cp \"" .. MessageHandler:sformat(message,self) .. Color.WHITE .. "\"")
    elseif type( message ) == "table" then
        for k=1,tlen(message) do
            et.trap_SendServerCommand( self.id, "cp \"" .. MessageHandler:sformat(message[k] , self ) .. Color.WHITE .. "\"")
        end
    end
end

function ClientObject:ChatClean(message)
    et.trap_SendServerCommand( self.id, "chat \"" .. message .. Color.WHITE .. "\"")
end

function ClientObject:PrintClean(message)
    et.trap_SendServerCommand( self.id, "print \"" .. message .. Color.WHITE .. "\n\"")
end

function ClientObject:EchoClean(message)
    et.trap_SendServerCommand( self.id, "cpm \"" .. message .. Color.WHITE .. "\"")
end

function ClientObject:BannerClean(message )
    et.trap_SendServerCommand( self.id, "bp \"" .. message .. Color.WHITE .. "\"")
end

function ClientObject:CenterPrintClean(message )
    et.trap_SendServerCommand( self.id, "cp \"" .. message .. Color.WHITE .. "\"")
end

function ClientObject:Error(message )
    et.trap_SendServerCommand( self.id, "print \"".. Color.Tertiary .."[" .. Color.Error .."ERROR"..Color.Tertiary.."] " ..Color.Primary ..  message .. Color.WHITE .. "\n\"")
end
function ClientObject:ErrorChat(message )
    et.trap_SendServerCommand( self.id, "chat \"".. Color.Tertiary .."[" .. Color.Error .."ERROR"..Color.Tertiary.."] " ..Color.Primary ..  message .. Color.WHITE .. "\"")
end
function ClientObject:AWChat(message)
    self:ChatClean("^zAdmin^9Watch" .. Color.Tertiary .. "#" .. Color.Primary .. message)
end
function ClientObject:AWPrint(message)
    self:PrintClean("^zAdmin^9Watch" .. Color.Tertiary .. "#" .. Color.Primary .. message)
end

function ClientObject:Say(message )
    et.G_Say( self.id, et.SAY_ALL,message)
end

function ClientObject:SayTeam(message )
    et.G_Say( self.id, et.SAY_TEAM,message)
end

function ClientObject:SayFireTeam(message )
    et.G_Say( self.id, et.SAY_BUDDY,message)
end

function ClientObject:PrintLocation(str,location,key)
    if not str then return end
    if not location then location = "printclean" end
    location = Misc:triml(location)
    local cl_location = location
    if key and self:isOnline() then cl_location = Misc:triml(self.User:getKey(key)) end
    if cl_location == "" or cl_location == "server" then cl_location = location end
    if cl_location == "none" then return end
    if cl_location == "chat" then
        self:Chat(str)
    elseif cl_location == "chatclean" then
        self:ChatClean(str)
    elseif cl_location == "print" then
        self:Print(str)
    elseif cl_location == "printclean" then
        self:PrintClean(str)
    elseif cl_location == "echo" or cl_location == "cpm" then
        self:Echo(str)
    elseif cl_location == "echoclean" or cl_location == "cpmclean" then
        self:EchoClean(str)
    elseif cl_location == "banner" or cl_location == "bp" then
        self:Banner(str)
    elseif cl_location == "bannerclean" or cl_location == "bpclean" then
        self:BannerClean(str)
    elseif cl_location == "centerprint" or cl_location == "cp" then
        self:CenterPrint(str)
    elseif cl_location == "centerprintclean" or cl_location == "cpclean" then
        self:CenterPrintClean(str)
    else
        self:PrintClean(str)
    end
end

function ClientObject:TimedPrint(t,func)
    if not t then return end
    if not func then return end
    if not next(t) then return end
    self.printTable[tlen(self.printTable)+1] = {
        func = func,
        message = t,
    }
end

function ClientObject:runTimedPrint()
    local pt = self.printTable[1]
    if not pt or not next(pt) then return end
    local max = 10
    if tlen(pt.message) < 10 then max = tlen(pt.message) end
    if not next(pt.message) then
        table.remove(self.printTable,1)
    end
    local k=1
    while k <= max do
        pt.func(self,pt.message[k])
        table.remove(pt.message[1])
        k = k + 1
    end
    if not next(pt.message) then
        table.remove(self.printTable,1)
    end
end



function ClientObject:isGrammarChecked()
    if Config.Message.GrammarCheck then return true end
    if self.rudclient and self.rudclient.grammarcheck then return true end
    if self:isOnline() and self.User:getKey('grammarcheck') then return true end
    return false
end

function ClientObject:isNoCapped()
    if Config.Message.NoCaps then return true end
    if self.rudclient and self.rudclient.nocaps then return true end
    if self:isOnline() and self.User:getKey('nocaps') then return true end
    return false
end

function ClientObject:isNoColored()
    if Config.Message.NoColors then return true end
    if self.rudclient and self.rudclient.nocolors then return true end
    if self:isOnline() and self.User:getKey('nocolors') then return true end
    return false
end

function ClientObject:isSwearFiltered()
    if Config.Message.SwearFilter then return true end
    if self.rudclient and self.rudclient.swearfilter then return true end
    if self:isOnline() and self.User:getKey('swearfilter') then return true end
    return false
end

