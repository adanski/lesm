EntityHandler = { }
--[[
MapEnt.map        = string       - "all" for all maps, "frostbite" for frosbite map etc.
MapEnt.timeleft   = (false/int)  - False to not use, percentage of time passed in map to needed to run the function, negative is run until that amount of percentage
MapEnt.players    = (false/int)  - False to not use, number of players needed to run the function, negative is to run until that amount of players is active
MapEnt.scriptname = string       - scriptName of ent - uses entity commands to search for what you want
MapEnt.classname  = string       - classname of ent (NOT USED ATM)
MapEnt.find       = (true/false) - True to use a find function instead of exact match in scriptname
MapEnt.free       = (true/false) - True to free the ent when ran (Meaning there is no use for it anymore)
MapEnt.loop       = (true/false) - True if run every loop, false runs once until completed.
MapEnt.state      = See MAPSTATE BELOW
MapEnt.startmessage = ""
MapEnt.endmessage = ""
MapEnt.distancemessage = ""
MapEnt.distanceorigin = false (uses scriptname origin) { x , y , z }
MapEnt.distanceclass = false or class
MapEnt.distancehasobjective = true/false
MapEnt.distanceteam = false or team

TODO Do I want players compared to total players or just the highest team number?
]]--
local MAPSTATE = {
    ERROR            = 5,
    COMPLETED        = 4,
    RUNNING          = 3,
    RUNNING_LOOP     = 2,
    PENDING_COMPLETE = 1,
    HAS_NOT_RAN      = 0,
}

local _validate = function() -- Probably want to check for nil values here
    for k=1,tlen(Entities) do
        local mapent = Entities[k]
        mapent.map             = Misc:triml(mapent.map)
        mapent.scriptname      = Misc:triml(mapent.scriptname)
        mapent.startmessage    = Misc:trim(mapent.startmessage)
        mapent.endmessage      = Misc:trim(mapent.endmessage)
        mapent.distancemessage = Misc:trim(mapent.distancemessage)
        if not mapent.distanceorigin or not next(mapent.distanceorigin) then mapent.distanceorigin = false end
        if mapent.distanceclass ~= false then mapent.distanceclass = tonumber(mapent.distanceclass) or false end
        if mapent.distanceteam ~= false then mapent.distanceteam = tonumber(mapent.distanceteam) or false end
        if ( mapent.timeleft ~= false ) then mapent.timeleft = tonumber(mapent.timeleft) or false end
        if ( mapent.players ~= false ) then mapent.players   = tonumber(mapent.players) or false end
        if ( mapent.find ~= true ) then mapent.find = false end
        if ( mapent.free ~= true ) then mapent.free = false end
        if ( mapent.loop ~= true ) then mapent.loop = false end
        if mapent.distancehasobjective ~= true then mapent.distancehasobjective = false end
        mapent.state = MAPSTATE.HAS_NOT_RAN
    end
end

--- Loads necessary entity files
-- Also validates the map entities
function EntityHandler:Init()
    Console:Info("Loading entity data")
    for k=1,tlen(FieldNames) do FieldNames[k].clientonly = false end
    for k=1,tlen(ClientFieldNames) do
        ClientFieldNames[k].client = true
        FieldNames[tlen(FieldNames)+1] = ClientFieldNames[k]
    end
    _validate()
end

function EntityHandler:isClientSlot(ent)
    ent = tonumber(ent)
    if not ent then return false end
    if ( ent >= self.ENT_START and ent < self.ENT_CLIENTS ) then return true end
    return false
end

function EntityHandler:isClient(ent)
    ent = tonumber(ent)
    if not ent then return false end
    if not self:isClientSlot(ent) then return false end
    for k=1,tlen(Clients) do
        if ( Clients[k].id == ent ) then return true end
    end
    return false
end

-- NOTE Can probably make these local
-- NOTE Not sure if this should be 1 & 1024

EntityHandler.ENT_START = 0
EntityHandler.ENT_END = 1023
EntityHandler.ENT_CLIENTS = 64 -- NUMBER OF CLIENT SLOTS

--- Gets distance between two Entities 
-- Can be used on clientNums as well
-- returns distance in quake units
function EntityHandler:getDistance(target1,target2)
    if not target1 or not target2 then return end
    local origin1 = { }
    local origin2 = { }
    if ( type(target1) == "table" ) then
        origin1 = target1
    else
        target1 = tonumber(target1)
        if not target1 then return end
        origin1 = self:safeEntGet(target1,"r.currentOrigin")
    end
    if ( type(target2) == "table" ) then
        origin2 = target2
    else
        target2 = tonumber(target2)
        if not target2 then return end
        origin2 = self:safeEntGet(target2,"r.currentOrigin")
    end
    
    local xCord = ( origin2[1] - origin1[1] )
    local yCord = ( origin2[2] - origin1[2] )
    local zCord = ( origin2[3] - origin1[3] )
    if string.find( _VERSION , "5.0" ) then
        return math.sqrt( math.pow(xCord,2) + math.pow(yCord,2) + math.pow(zCord,2) )
    else
        return math.sqrt( xCord^2 + yCord^2 + zCord^2 )
    end
end

--- Converts quake units to a feet/inches
function EntityHandler:DistanceToFeet(distance)
    -- The scale looks is supposedly UNITS == Inches But It just seems completely unreal when person right in front of you is 30 feet away
    -- Going to attempt half distance seems more realistic. ( There was one source that said it is one unit = half inch but most said one inch )
    distance = math.floor(distance / 2) -- Halved
    local feet = math.floor( distance / 12)
    local inches = math.floor( math.mod(distance,12) )
    return feet,inches
end

--- Converts quake units to a meters
function EntityHandler:DistanceToMeters(distance)
    return Misc:round(( distance * 2.54 ) / 100,2)
end
--- Iterate through all entities and returns table that matches [EntityCallBack](function)
function EntityHandler:Iterate(EntityCallback)
    local Data = { }
    for ent=self.ENT_START,self.ENT_END do
        if ( et.G_GetSpawnVar( ent , "classname" ) ~= nil ) then -- Safety check, to make sure ent is an ent
            Data[tlen(Data)+1] = EntityCallback(ent)
        end
    end
    return Data
end

--- Get All entities within [distance] of [Entity]
-- returns Table of entNums getEntitesNear
function EntityHandler:getEntitiesNear(Entity,distance)
    if ( distance == nil ) or ( tonumber(distance) == nil ) then
        distance = 250
    end
    local EntsNear = { }
    EntsNear = self:Iterate(function (entity)
        if ( self:getDistance(Entity,entity) <= distance ) then
            return entity
        end
    end)
    return EntsNear
end

--- Teleports an entity to specific origin
function EntityHandler:TeleportToOrigin(Entity,Origin)
    Entity = tonumber(Entity)
    if ( Entity == nil ) then
        return
    end
    et.gentity_set(Entity,"ps.origin",Origin)
end

--- Teleports Entity to another entitiy
function EntityHandler:Teleport(Entity_1,Entity_2)
    Entity_1 = tonumber(Entity_1)
    Entity_2 = tonumber(Entity_2)
    if ( Entity_1 == nil ) or ( Entity_2 == nil ) then
        return
    end
    local target_fn = "ps.origin"
    if not ( self:isClient(Entity_2) ) then
        target_fn = "s.origin2"
    end
    local Origin = self:safeEntGet(Entity_2,target_fn)
    if (Origin == nil ) or ( type(Origin) ~= "table" ) then
        return
    end
    local fn = "ps.origin"
    if not ( self:isClient(Entity_1) ) then
        fn = "s.origin2"
    end
    et.gentity_set(Entity_1,fn,Origin)
end

--- Gets total ent count in the map
function EntityHandler:FN_GetEntCount()
    local FoundEnts = self:Iterate(function(ent)
        local status,classname = pcall(et.gentity_get,ent,"classname")
        if ( status ) and ( classname ~= nil ) and ( classname ~= "" ) then
            return ent
        end
    end)
    if ( FoundEnts == nil ) or ( next(FoundEnts) == nil ) then return 0 end
    return tlen(FoundEnts)
end

--- Gets all Entity Data
-- Uses FieldNames file
function EntityHandler:FN_GetData(ent)
    local FN_EntData = { }
    local isClient = self:isClientSlot(ent)
    for k=1,tlen(FieldNames) do
        if isClient and not FieldNames[k].clientonly then
            -- skip
        elseif not isClient and FieldNames[k].clientonly then
            -- skip
        else
            local EntData = { }
            EntData[1] = FieldNames[k].name
            EntData[2] = self:FN_ConvertType(self:safeEntGet( ent, FieldNames[k].name ),FieldNames[k])
            EntData[3] = FieldNames[k].readonly
            if ( EntData[2] ~= nil ) then
                FN_EntData[tlen(FN_EntData)+1] = EntData
            end
        end
    end
    return FN_EntData
end

--- Find all ents with [fieldname]
function EntityHandler:FN_FindAll(fieldname)
    fieldname = Misc:trim(fieldname)
    local FoundEnts = self:Iterate(function (ent)
        local status,value = pcall(et.gentity_get,ent,fieldname)
        if ( status and value ~= nil and type(value) == "string" ) then
            value = Misc:trim(value)
            if ( value ~= "" ) then
                return ent
            end
        end
        -- Check for tables? Is that possible? I had it before for some reason
        -- Also if an error happens in pcall do I just want to return none
    end)
    return FoundEnts
end

--- Find all ents where [text] is in value of [fieldname]
-- if [specific] then [text] must match value of [fieldname]
function EntityHandler:FN_Find(fieldname,text,specific)
    fieldname = Misc:trim(fieldname)
    text = Misc:trim(text)
    if ( specific == nil ) then specific = false end
    local FoundEnts = self:Iterate(function (ent)
        local status,value = pcall(et.gentity_get,ent,fieldname)
        if ( status ) and ( value ~= nil ) and ( type(value) == "string" ) then
            value = Misc:trim(value)
            if ( specific ) then
                if ( string.lower(value) == string.lower(text) ) then -- Do i want string lower?
                    return ent
                end
            else
                if ( string.find(value,text,1,true) ) then
                    return ent
                end
            end
        end
        -- Check for tables? Is that possible? I had it before for some reason
        -- Also if an error happens in pcall do I just want to return none
    end)
    return FoundEnts
end

--- Checks if a fieldname is defined
function EntityHandler:FN_Exists(fieldname)
    for k=1,tlen(FieldNames) do
        if ( FieldNames[k].name == fieldname ) then
            return true
        end
    end
    return false
end

--- Gets the type of fieldname
function EntityHandler:FN_GetType(fieldname)
    for k=1,tlen(FieldNames) do
        if ( FieldNames[k].name == fieldname ) then
            return FieldNames[k].type
        end
    end
    return nil
end

--- Checks if fieldname has the readonly flag
function EntityHandler:FN_IsReadOnly(fieldname)
    for k=1,tlen(FieldNames) do
        if ( FieldNames[k].name == fieldname ) then
            return FieldNames[k].readonly
        end
    end
    return nil
end

--- Converts fieldname data to lua type
function EntityHandler:FN_ConvertType(entdata,EntFieldName)
    if ( EntFieldName == nil ) then return nil end
    if ( EntFieldName.type == nil ) then return nil
    elseif ( EntFieldName.type == "FIELD_ENTITY" ) then return tonumber(entdata)
    elseif ( EntFieldName.type == "FIELD_INT" ) then return tonumber(entdata)
    elseif ( EntFieldName.type == "FIELD_STRING" ) then return Misc:trim(entdata)
    elseif ( EntFieldName.type == "FIELD_FLOAT" ) then return tonumber(entdata)
    else
        -- VEC3 and TRAJECTORY
        -- Is Table probably
        -- Check to make sure it is a table if it needs to be?
        -- This will need to be done in :GetData ( Iterate through values )
        return entdata
    end
end

-- Probably over kill on the if statements here... lmao
function EntityHandler:MapMessage(Client)
    for k=1,tlen(Entities) do
        local mapent = Entities[k]
        local found  = false
        if mapent.map == "all" or mapent.map == Game:Map() then
            local mapentorigin = mapent.distanceorigin
            if not mapentorigin then
                local FoundEnts = self:FN_Find("scriptName",mapent.scriptname,mapent.find)
                if not FoundEnts or not next(FoundEnts) then mapentorigin = nil end
            end
            if mapentorigin then
                if self:getDistance(Client.id,mapentorigin) <= 500 then
                    if Misc:trim(mapent.distancemessage) ~= "" then
                        if mapent.distanceteam then
                            if mapent.distanceteam == Client:getTeam() then
                                if mapent.distancehasobjective then
                                    if Client:hasObjective() then
                                        if mapent.distanceclass then
                                            if Client:getClass() == mapent.distanceclass then found = true end
                                        else
                                            found = true
                                        end
                                    end
                                else
                                    if mapent.distanceclass then
                                        if Client:getClass() == mapent.distanceclass then found = true end
                                    else
                                        found = true
                                    end
                                end
                            end -- Team Check 2
                        else
                            if mapent.distancehasobjective then
                                if Client:hasObjective() then
                                    if mapent.distanceclass then
                                        if Client:getClass() == mapent.distanceclass then found = true end
                                    else
                                        found = true
                                    end
                                end
                            else
                                if mapent.distanceclass then
                                    if Client:getClass() == mapent.distanceclass then found = true end
                                else
                                    found = true
                                end
                            end
                        end -- Team Check
                    end -- Message check
                end -- Distance check
            end -- ENT ORIGIN
        end -- END MAP CHECK
        if found then
            Client:Chat(mapent.distancemessage)
            Client.entityMessage = 5.0
        end
    end -- END FOR LOOP
end

--- Checks if there are any map entities to run
-- Runs them if conditions are met
-- FEATURE MapEnts
function EntityHandler:Map()
    Game:getTeams()
    
    if ( Game:getTotalPlayingPlayers() <= 0 ) then return end
    if ( Game:Gamestate() ~= 0 ) then return end
    --Console:Info("test0")
    local mapname = Game:Map()
    for k=1,tlen(Entities) do --Start at 1? Entities Table? or Maps...
        local mapent = Entities[k]
        
        if ( mapent.state ~= MAPSTATE.ERROR ) and ( mapent.state ~= MAPSTATE.COMPLETED ) then -- skip ents that have completed or failed
            if ( mapent.map == "all" ) or ( mapent.map == mapname ) then -- Only do ents that are for this map
                local run_map_ent = true
                if ( mapent.players ~= false ) then -- Player amount check
                    if ( mapent.players <= 0 ) then
                        if ( math.abs(mapent.players) < Game:getTotalPlayingPlayers() ) then -- Run until x amount of players is reached
                            --Console:Info("test1")
                            if ( mapent.state ~= MAPSTATE.RUNNING ) and ( mapent.state ~= MAPSTATE.RUNNING_LOOP ) then
                                run_map_ent = false
                            else
                                mapent.state = MAPSTATE.PENDING_COMPLETE
                            end
                        end
                    else
                        if ( mapent.players > Game:getTotalPlayingPlayers() ) then -- When x amount of players is met - run
                            --Console:Info("test2")
                            if ( mapent.state ~= MAPSTATE.RUNNING ) and ( mapent.state ~= MAPSTATE.RUNNING_LOOP ) then
                                run_map_ent = false
                            else
                                mapent.state = MAPSTATE.PENDING_COMPLETE
                            end
                        end
                    end
                end
                if ( mapent.timeleft ~= false ) then
                    if ( mapent.timeleft <= 0 ) then
                        if ( ( math.abs(mapent.timeleft) * 0.01 ) < Game:getMapPercentComplete() ) then
                            --Console:Info("test3")
                            if ( mapent.state ~= MAPSTATE.RUNNING ) and ( mapent.state ~= MAPSTATE.RUNNING_LOOP ) then
                                run_map_ent = false
                            else
                                mapent.state = MAPSTATE.PENDING_COMPLETE
                            end
                        end
                    else
                        if ( ( mapent.timeleft * 0.01 ) > Game:getMapPercentComplete() ) then
                            --Console:Info("test4")
                            if ( mapent.state ~= MAPSTATE.RUNNING ) and ( mapent.state ~= MAPSTATE.RUNNING_LOOP ) then
                                run_map_ent = false
                            else
                                mapent.state = MAPSTATE.PENDING_COMPLETE
                            end
                        end
                    end
                end
                if ( run_map_ent ) then
                    --Console:Info("test5")
                    self:RunMapEnt(mapent)
                end
            end
        end
    end
end

--- Executes the mapent script
function EntityHandler:RunMapEnt(mapent)
    --Console:Info(tostring(mapent.timeleft) .. " - " .. Game:getMapPercentComplete())
    if ( mapent.state == MAPSTATE.RUNNING ) then return end
    local FoundEnts = self:FN_Find("scriptName",mapent.scriptname,mapent.find)
    if ( FoundEnts == nil  or next(FoundEnts) == nil ) then
        mapent.state = MAPSTATE.ERROR
        Console:Error("EntityHandler:RunMapEnt mapent error")
        return
    end
    for k = 1 , tlen(FoundEnts) do 
        if ( mapent.state == MAPSTATE.PENDING_COMPLETE ) then
            if not mapent.free then
                et.trap_LinkEntity(FoundEnts[k])
            end
        else
            et.trap_UnlinkEntity(FoundEnts[k])
            if mapent.free then
                et.G_FreeEntity( FoundEnts[k] )
            end
        end
    end
    if ( mapent.state == MAPSTATE.PENDING_COMPLETE ) then
        mapent.state = MAPSTATE.COMPLETE
        Console:Info(mapent.scriptname .. " is complete")
        if not mapent.loop and mapent.endmessage ~= "" then
            Console:Chat(mapent.endmessage)
        end
    else
        if mapent.loop then
            mapent.state = MAPSTATE.RUNNING_LOOP
        else
            mapent.state = MAPSTATE.RUNNING
            if mapent.startmessage ~= "" then
                Console:Chat(mapent.startmessage)
            end
            Console:Info(mapent.scriptname .. " is running")
        end
    end
    
end

function EntityHandler:safeEntSet(ent,fieldname,value)
    if ( string.lower(fieldname) == "s.eflags" ) then return end
    local status,err = pcall(et.gentity_set,ent,fieldname,value)
    if not status then
        Console:Debug(err,"entspawn")
    end
end

function EntityHandler:safeEntGet(ent,fieldname,index)
    local status,value = pcall(et.gentity_get,ent,fieldname,index)
    
    if not status then
        Console:Debug(value,"entspawn")
        return nil
    end
    return value
end

function EntityHandler:FN_GetDifferences(ent1,ent2)
    local Ent1Data = EntityHandler:FN_GetData(ent1)
    local Ent2Data = EntityHandler:FN_GetData(ent2)
    local Data = {}
    if not Ent1Data or not Ent2Data or not next(Ent1Data) or not next(Ent2Data) then
        return Data
    end
    for k=1,tlen(FieldNames) do
        local fname = string.lower(FieldNames[k].name)
        for x=1,tlen(Ent1Data) do
            local fname1 = string.lower(Ent1Data[x][1])
            if ( fname == fname1 ) then
                for y=1,tlen(Ent2Data) do
                    local fname2 = string.lower(Ent2Data[y][1])
                    if ( fname == fname2 ) then
                        if ( Ent2Data[y][2] ~= Ent1Data[x][2] ) then
                            Data[tlen(Data)+1] = Ent2Data[y]
                        end
                    end
                end
            end
        end
    end
    return Data
end

function EntityHandler:Item_Health(Client)
    local item_health  = et.G_Spawn()
    self:safeEntSet(item_health,"classname","item_healthh") -- 2 h's because lua cuts off last char in strings
    self:safeEntSet(item_health,"scriptName","lua/healthpackk")
    self:safeEntSet(item_health,"targetName","lua/healthpackk")
    self:safeEntSet(item_health,"s.origin",Client:getOrigin())
    --self:safeEntSet(item_health,"angle",180)
    
    local trigger_heal = et.G_Spawn()
    self:safeEntSet(trigger_heal,"classname","trigger_heall")
    self:safeEntSet(trigger_heal,"scriptName","lua/healthpack/triggerr")
    self:safeEntSet(trigger_heal,"target","lua/healthpackk")
    self:safeEntSet(trigger_heal,"health",400)
    self:safeEntSet(trigger_heal,"s.origin",Client:getOrigin())
    self:safeEntSet(trigger_heal,"r.mins", { -27 , -25 ,  0 } )
    self:safeEntSet(trigger_heal,"r.maxs", {  27 ,   0 , 64 } )
    
    
    Client:Chat("item_health:" .. tostring(item_health) .." trigger_heal:" .. tostring(trigger_heal))
end


function EntityHandler:Copy(Client,ent,classname)
    if not ( classname ) then classname = "item_health" end
    Console:Debug("Starting copy...","entspawn")
    Console:Debug("G_Spawn START","entspawn")
    local newent = et.G_Spawn()
    Console:Debug("G_Spawn END","entspawn")
    
    --Console:Debug("Set ClassName START","entspawn")
    --et.G_SetSpawnVar( newent, "classname", classname )
    --Console:Debug("Set ClassName END","entspawn")

    Console:Debug("Unlink START","entspawn")
    et.trap_UnlinkEntity(newent)
    Console:Debug("Unlink END","entspawn")
    
    Console:Debug("Copy FieldNames START","entspawn")
    local diff = self:FN_GetDifferences(newent,ent)
    for k=1,tlen(diff) do
        self:safeEntSet(newent,diff[k][1],diff[k][2])
    end
    Console:Debug("Copy FieldNames END","entspawn")
    
    local clientorigin = Client:getOrigin()
    for k=1,tlen(clientorigin) do
        Console:Debug(clientorigin[k],"entspawn")
    end
    Console:Debug("1","entspawn")
    _safeEntSet(newent,"r.currentOrigin",clientorigin)
    Console:Debug("2","entspawn")
    _safeEntSet(newent,"s.origin",clientorigin)
    Console:Debug("3","entspawn")
    _safeEntSet(newent,"s.origin2",clientorigin)
    Console:Debug("4","entspawn")
    _safeEntSet(newent,"s.pos",clientorigin)
    Console:Debug("5","entspawn")
    _safeEntSet(newent,"origin",clientorigin)
    Console:Debug("6","entspawn")
    _safeEntSet(newent,"ps.origin",clientorigin)
    Console:Debug("7","entspawn")
    et.G_SetSpawnVar( newent, "origin", clientorigin )
    Console:Debug("8","entspawn")
    
    --Console:Debug("trap_LinkEntity START","entspawn")
    --et.trap_LinkEntity(ent)
    --Console:Debug("trap_LinkEntity END","entspawn")
    
    -- Testwithout unlink, lookup my old function, Ohhhhhhh yetah there is something like eflags or something that crashes when set checkout that.
    
    Client:Chat(newent)
end
--[[
function EntityHandler:Spawn(Client,Args)
    Console:Debug("Starting spawn...","entspawn")
    Console:Debug("G_Spawn START","entspawn")
    local ent = et.G_Spawn()
    Console:Debug("G_Spawn END","entspawn")

    Console:Debug("Unlink START","entspawn")
    et.trap_UnlinkEntity(ent)
    Console:Debug("Unlink END","entspawn")

    Console:Debug("ClientOrigin START","entspawn")
    local clientOrigin = Client:getOrigin()
    Console:Debug("ClientOrigin END","entspawn")

    Console:Debug("setEntOrigin START","entspawn")
    et.gentity_set(ent,"s.origin", clientOrigin)
    Console:Debug("setEntOrigin END","entspawn")
        
    Console:Debug("rmins START","entspawn")
    et.gentity_set(ent,"r.mins",{-20,-20,-20})
    Console:Debug("rmins END","entspawn")
    
    Console:Debug("rmaxs START","entspawn")
    et.gentity_set(ent,"r.maxs",{20,20,20})
    Console:Debug("rmaxs END","entspawn")
    
    Console:Debug("seType START","entspawn")
    et.gentity_set(ent,"s.eType", 2) --ET_ITEM = 2
    Console:Debug("seType END","entspawn")
    
    --Console:Debug("modelindex START","entspawn")
    --et.gentity_set(ent,"s.modelindex",) --store item number in modelindex
    --Console:Debug("modelindex END","entspawn")
    
    Console:Debug("modelindex2 START","entspawn")
    et.gentity_set(ent,"s.modelindex2",0) --zero indicates this isn't a dropped item
    Console:Debug("modelindex2 END","entspawn")
    
    Console:Debug("contents START","entspawn")
    et.gentity_set(ent,"r.contents", 0x40000000) -- CONTENTS_TRIGGER
    Console:Debug("contents END","entspawn")
    
    Console:Debug("entcurrentOrigin START","entspawn")
    et.gentity_set(ent,"r.currentOrigin", clientOrigin)
    Console:Debug("entcurrentOrigin END","entspawn")

    --Console:Debug("classname START","entspawn")
    --et.gentity_set(ent,"classname", "weapon_sten")
    --Console:Debug("classname END","entspawn")
    
    Console:Debug("trap_LinkEntity START","entspawn")
    et.trap_LinkEntity(ent)
    Console:Debug("trap_LinkEntity END","entspawn")
end]]--

--[[ 
Whats in the table...
        MapUpdate = function(self)
        if ( ET_CurrentPlayers <= 1 ) then return end -- If players
        if ( game.Gamemode() == 3 )  then return end
        local mapname = game.Map()
        for k = 1 , tlen(MapEnts) do 
            local MapEnt = MapEnts[k]
            local doEntUpdate = true
            local doWhat = 0
            if ( MapEnt.mapname ~= "all" ) and ( mapname ~= string.lower(MapEnt.mapname) ) then
                doEntUpdate = false
            end
            if ( MapEnt.timeleft ~= false ) then
                if ( game.GetMapPercentage() < MapEnt.timeleft ) then
                    doWhat = 1
                    if ( ET_CurrentPlayers <= 1 ) then doWhat = 2 end -- If its just them
                else
                    doWhat = 2
                end
            end
            if ( MapEnt.players ~= false ) then
                if ( ET_AxisPlayers < MapEnt.players or ET_AlliedPlayers < MapEnt.players ) then
                    doWhat = 1
                    if ( ET_CurrentPlayers <= 1 ) then doWhat = 2 end -- If its just them
                else
                    doWhat = 2
                end
            end
            if ( doEntUpdate ) and ( doWhat == 1 or doWhat == 2 ) then
                local EntUpdate = true
                local EntFound = false
                self.IterateEnts(function (entity,ent_name)
                    local scriptName = et.G_GetSpawnVar( entity , "scriptName" )
                    local script = false
                    local found = false
                    if ( MapEnt.classname == nil ) then
                        script = true
                    end
                    if ( script ) then
                        if ( scriptName ~= nil ) then
                            if ( string.lower(scriptName) == string.lower(MapEnt.scriptName) ) or ( MapEnt.find == true and string.find(string.lower(scriptName),string.lower(MapEnt.scriptName),1,true) ) then
                                found = true
                            end
                        end
                    else
                        if ( string.lower(ent_name) == string.lower(MapEnt.classname) ) or ( MapEnt.find == true and string.find(string.lower(ent_name),string.lower(MapEnt.classname),1,true) ) then
                            found = true
                        end
                    end
                    if ( found == true ) then
                        if ( doWhat == 1 ) then
                            Print.Debug("ent.MapUpdate() doWhat=unlink entity=" .. entity .." mapentcount="..tostring(MapEnt.count))
                            if ( MapEnt.count == nil ) or ( MapEnt.count <= 1 ) then
                                Print.Debug("ent.MapUpdate() unlinked " .. entity)
                                et.trap_UnlinkEntity(entity)
                                if ( MapEnt.free ) then
                                    et.G_FreeEntity( entity )
                                end
                                EntUpdate = true
                                EntFound = true
                            end
                        elseif ( doWhat == 2 ) then
                            Print.Debug("ent.MapUpdate() doWhat=link entity=" .. entity .." mapentcount="..tostring(MapEnt.count))
                            if (MapEnt.count ~= nil ) and ( MapEnt.count >= 1 ) then
                                if not ( MapEnt.free ) then
                                    Print.Debug("ent.MapUpdate() linked " .. entity)
                                    et.trap_LinkEntity(entity)
                                    EntUpdate = false
                                    EntFound = true
                                end
                            end
                        end
                    end
                end)
                if ( EntUpdate ) then
                    if ( EntFound ) then
                        if ( MapEnt.count == nil ) then
                            MapEnt.count = 0
                        end
                        MapEnt.count = MapEnt.count + 1
                    end
                else
                    MapEnt.count = nil
                end
            end
        end
    end,
--      Defintely need to rewrite to a better format

    GetEntityData = function (ENTITY,FILTER)
        local EntityData = { }
        for _ , spawnflag in pairs(SpawnFlags) do
            local value = et.G_GetSpawnVar( ENTITY, spawnflag )
            if ( spawnflag == "classname" ) and ( value == nil ) then
                break
            end
            if ( value ~= nil ) and ( spawnflag ~= nil ) then
                EntityData[spawnflag] = value
            end
        end
        local EntityFieldData = { }
        for _ , fieldname in pairs(FieldNames) do
            local value = et.gentity_get(ENTITY,fieldname)
            if ( value ~= nil ) and ( fieldname ~= nil ) then
                if ( FILTER ) then
                    if not ( fieldname == "tagParent" ) then
                        EntityFieldData[fieldname] = value
                    end
                else
                    EntityFieldData[fieldname] = value
                end
            end
        end
        return EntityData,EntityFieldData
    end,
    
    GetEntitiesWith = function (self,SearchStr,flag)
        if flag == "" then flag = "classname" end
        local FoundEnts = { }
        FoundEnts = self.IterateEnts(function (entity,ent_name)
            local FlagValue = et.G_GetSpawnVar( entity , flag )
            if ( FlagValue ~= nil ) then
                if ( type(FlagValue) == "string" ) then
                    if ( string.find(FlagValue,SearchStr) ) then
                        return entity
                    end
                elseif ( type(FlagValue) == "table" ) then
                    --for _ ,v in pairs(FlagValue) do
                    for k = 1 , tlen(FlagValue) do 
                        if ( FlagValue[k] ~= nil ) then
                            if (type(FlagValue[k]) == "string") then
                                if ( string.find(FlagValue[k],SearchStr) ) then
                                    return entity
                                end
                            end
                        end
                    end
                end
            end
        end)
        return FoundEnts
    end,
UNUSED
function EntityHandler:GetSpawnData(Entity)
    local EntityData = { }
    
    for k = 1 , tlen(SpawnFlags) do 
        local SpawnFlag = { }
        SpawnFlag.name = SpawnFlags[k]
        SpawnFlag.data = et.G_GetSpawnVar( Entity, SpawnFlags[k] )
        if ( SpawnFlag.data ~= nil ) then
            EntityData[tlen(EntityData+1]) = SpawnFlag
        end
    end
end]]--

--  Working on FieldNames first it works best I think.
-- ent getallsv <ent>
-- ent getallfn <ent>
-- ent getsv <ent> <spawnvar>
-- ent getfn <ent> <fname>
-- ent setsv <ent> <spawnvar> <value>
-- ent setfn <ent> <fname> <value>
-- ent findsv <fname> [text]
-- ent findfn <spawnvar> [text]
-- ent atorigin <x> <y> <z> [distance]
-- ent atent <ent> [distance]
-- ent atself [distance]
-- ent spawnvar <spawnvar> <value> -- possible crashes
-- ent fieldname <fname> <value> -- possible crashes