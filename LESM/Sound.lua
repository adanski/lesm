Sound = { }
Sound.Sounds = { }
local tonumber,tsort,type = tonumber,table.sort,type
--- Makes sure all the sounds do not have nil values
function Sound:verify()
    for k=1,tlen(self.Sounds) do
        local s   = self.Sounds[k]
        s.src     = Misc:trim(s.src)
        s.title   = Misc:triml(s.title)
        s.message = Misc:trim(s.message)
        s.level   = tonumber(s.level) or 0
        if not s.aliases then s.aliases = { } end
        if not s.tags    then s.tags    = { } end
        for j=1,tlen(s.aliases) do s.aliases[j] = Misc:triml(s.aliases) end
        for j=1,tlen(s.tags) do s.tags[j] = Misc:triml(s.tags) end
    end
    tsort(self.Sounds,function(a,b) return a.title < b.title end)
end

function Sound:Load()
    self.Sounds = FileHandler:Load{ filePath = "LESM/save/" .. Config.JSON.Sounds }
    self:verify()
end

function Sound:Save()
    self:verify()
    FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Sounds, fileData = self.Sounds }
end

function Sound:canUse(Client,soundList)
    if Client:isAuth() then return soundList end
    local level = Client:getLevel()
    local newSoundList = { }
    for k = 1 , tlen(soundList) do 
        local s = soundList[k]
        if level >= s.level then
            newSoundList[tlen(newSoundList)+1] = s
        end
    end
    return newSoundList
end

function Sound:find(soundname)
    soundname = Misc:triml(soundname)
    if soundname == "" then return { } end
    local found = { }
    for k=1,tlen(self.Sounds) do
        local s = self.Sounds[k]
        if s.title == soundname then
            local found = { }
            found[tlen(found)+1] = s
            return found
        end -- Found exact match
        if string.find(s.title,soundname,1,true) then
            found[tlen(found)+1] = s
        else
            for j=1,tlen(s.aliases) do
                if string.find(s.aliases[j],soundname,1,true) then found[tlen(found)+1] = s end -- Do I want string.find here?
            end
        end
    end
    return found
end

function Sound:getTagList()
    local tags = { }
    for k=1,tlen(self.Sounds) do
        for j=1,tlen(self.Sounds[k].tags) do Misc:table_addNotExist(tags,self.Sounds[k].tags[j]) end
    end
    return tags
end

function Sound:findWithTag(tags)
    local found = { }
    for x = 1 , tlen(tags) do tags[x] = Misc:triml(tags[x]) end
    for k=1,tlen(self.Sounds) do
        local foundSound = false
        for j=1,tlen(self.Sounds[k].tags) do
            for h = 1 , tlen(tags) do 
                if self.Sounds[k].tags[j] == tags[h] then -- im so deep yo
                    foundSound = true
                    break
                end
            end
            if foundSound then break end
        end
        if foundSound then found[tlen(found)+1] = self.Sounds[k] end
    end
    return found
end

--- SOUNDS IN SPECTATOR PLAY AT SPECTATOR ORIGIN
-- Possible to make client only sound?
function Sound:play(Client,sound,From)
    if not Config.Sound.Enabled then return end
    if not Client or not sound or not Client.rudclient then return end
    if not Client.rudclient.sounds then return end
    if Client:isOnline() and not Client.User:getKey('sounds') then return end
    if From and not From:isConsole() and Client:isSoundIgnored(From) then return end
    local soundpath = ""
    if type(sound) == "table" then
        if sound.message and sound.message ~= "" then
            Client:PrintLocation( MessageHandler:replace( sound.message , {
            map=true,mod=true,teams=true,colors=true,
            clientnum=true,clientname=true,clientcountry=true,clientclass=true,
            clientteam=true,clientlevel=true,clientetversion=true,clientprotocol=true,clientping=true,
        } , Client ) , "centerprint" , "playsoundlocation" )
        end
        soundpath = Misc:trim(sound.src)
    elseif type(sound) == "string" then
        soundpath = Misc:trim(sound)
    end
    if soundpath ~= "" then
        if ( Game:Mod() == "silent" or Game:Mod() == "legacy" ) then
            et.G_ClientSound( Client.id , et.G_SoundIndex( soundpath ) )
        else
            et.G_Sound ( Client.id , et.G_SoundIndex( soundpath ) )
        end
    end
end

function Sound:playAll(sound,Client)
    if not Config.Sound.Enabled or not sound then return end
    if not Client then Client = Console end
    for k = 1 , tlen(Clients) do self:play(Clients[k],sound,Client) end
end
