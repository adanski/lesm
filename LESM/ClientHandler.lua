ClientHandler = { }

--- Return ClientObject if exists else creates new one and returns it
-- took out the pcall, basically if its going to error here then it probably should
function ClientHandler:add( clientNum )
    local Client = self:getWithId(clientNum)
	
	if Client then return Client end
	
	Client = ClientObject( clientNum )
	if not Client then
		Console:Error("Creating ClientObject from client number " .. tostring(clientNum))
		return nil
	end
	Console:Debug("Creating new client " .. Client.id,"core")
	Clients[tlen(Clients)+1] = Client
	table.sort(Clients,function(a,b) return a.id < b.id end )
	return self:getWithId(clientNum)
end

--- Removes the client from the Clients table
function ClientHandler:delete( clientNum )
    local Client , clientIndex = self:getWithId(clientNum)
    if ( Client ~= nil and clientIndex ~= nil ) then table.remove(Clients,clientIndex) end
    table.sort(Clients,function(a,b) return a.id < b.id end)
    Console:Debug("Deleting client " .. clientNum,"core")
end

--- Get ClientObject if clientNum matches an id ( Also returns the index )
function ClientHandler:getWithId( clientNum )
    clientNum = tonumber(clientNum)
    if not clientNum then return nil end
    
    for k = 1 , tlen(Clients) do
        if Clients[k].id == clientNum then
            return Clients[k] , k
        end
    end
    
    return nil
end

--- Gets ClientObject by their name
-- Uses et.ClientNumberFromName if exists
function ClientHandler:getWithName( name )
    if not et.ClientNumberFromString then
        name = Misc:trim(name,true,true)
        if name == "" then return nil end
		
		-- Check for clientNum
        if tonumber(name) and EntityHandler:isClient(tonumber(name)) then
			return self:getWithId(tonumber(name))
		end
        for k=1,tlen(Clients) do
            local Client     = Clients[k]
            local clientname = Misc:trim(Client:getName(),true,true)
            if name == clientname then return Client end
            if string.find(clientname,name,1,true) then
				return Client
			end
        end
		
        return nil
    end
    
	local clientNum = tonumber( et.ClientNumberFromString( name ) ) or -1
    if clientNum < 0 then return nil end
	
    return self:getWithId(clientNum)
end

--- Gets ClientObject if client is a name or clientNum
function ClientHandler:getWith( data )
    if data == nil then return nil end
	
    if tonumber(data) then
        return self:getWithId(tonumber(data))
    else
        return self:getWithName(data)
    end
	
    return nil
end

--- Get ClientObject of a client or online username
function ClientHandler:getClient( clientData )
    local Client = self:getWith(clientData)
	if Client then return Client end
    
	local User = UserHandler:getWithUsername(clientData)
	
    if User then
		return User:getOnlineClient() -- Get Client if online.
	else
		return nil
	end
end

--- Iterate over clients
-- Not sure if this is a good method to do this.
function ClientHandler:iterate( func )
	for k=1,tlen(Clients) do
		func(Clients[k])
	end
end


