function tlen( t )
    return #t
end

if not math.mod then
	function math.mod( num , div )
		return num - ( math.tointeger(math.floor( num / div )) * div )
	end
end

function va(...)
    return string.format(...)
end