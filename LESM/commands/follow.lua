local FollowCommand = Command("Follow")

FollowCommand.ALIASES        = { "f","spectate","spec", }
FollowCommand.CORE           = true
FollowCommand.ADMIN          = true
FollowCommand.CHAT           = true
FollowCommand.LUA            = true
FollowCommand.AUTH           = true
FollowCommand.DESCRIPTION    = { "Will spectate the player even after death.", "Auth spec can only be done while you are dead." }
FollowCommand.SYNTAX         = { 
    {  "" ,"If following stops following whoever you are following."},
    {  "" ,"If not following moves you spectator."},
    { "<client>", "Follow a particular client."},
    { "<client> auth", "Follow a particular client, without going spectator."},
}
FollowCommand.EXAMPLES       = {
    { "Bob" , "Moves you to spectator and follows bob when he is alive" },
}

function FollowCommand:Run(Client,Args)
    if ( Game:Mod() == "noquarter" ) then
        self:Error("Not supported in " .. Game:Mod())
        return self.ERROR
    end
    if not Args[1] then
        if ( EntityHandler:isClient(Client.following) or EntityHandler:isClient(Client.authFollowing) ) then
            Client.following     = -1
            Client.authFollowing = -1
            self:Info("No longer following")
            return self.SUCCESS
        else
            Client:setTeam("spectator")
            self:Info("Moved to spectator, following no one")
            return self.SUCCESS
        end
    end

    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not get client")
        return self:getHelp()
    end
    if ( Target.id == Client.id ) then
        self:Error("Cannot follow yourself. Not sure why you tried.")
        return self:getHelp()
    end
    
    if Client:isAuth() and ( Args[2] == "auth" or Args[2] == "-auth" ) then
        Client.authFollowing = Target.id
        self:Info("Now auth following " .. Target:getName())
        Client:entSet("sess.spectatorState", 2)
        Client:entSet("sess.spectatorClient", Target.id)
        return self.SUCCESS
    end
    if Client:getTeam() ~= 3 then Client:setTeam("spectator") end
    Client.following = Target.id
    Client:entSet("sess.spectatorState", 2)
    Client:entSet("sess.spectatorClient", Target.id)
    self:Info("Following " .. Target:getName() )
    return self.SUCCESS
end

return FollowCommand