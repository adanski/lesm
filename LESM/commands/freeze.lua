local Freeze = Command("Freeze")

if not Game:Shrubbot() then
    Freeze.CHAT           = true
    Freeze.ADMIN          = true
end
Freeze.LUA            = true
Freeze.AUTH           = true
Freeze.CONSOLE        = true
Freeze.REQUIRE_LEVEL  = Config.Level.Leader
Freeze.DESCRIPTION    = { "Freezes user in place" ,}
Freeze.SYNTAX         = { { "<client>" , "Freezes client in place" ,}, }

function Freeze:Run(Client,Args)
    if not Args[1] then
        return self:getHelp()
    end
    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not find client")
        return self:getHelp()
    end
    if not Client:isConsole() and ( Target:getLevel() > Client:getLevel() and not Client:isAuth() ) or ( Target:isAuth() and not Client:isAuth() ) then
        self:Error("Target is higher level")
        return self.SUCCESS
    end
    
    if Target.freeze then
        Target.freeze = false
        Target.freezeLocation = false
        self:Info("Defrosted " .. Target:getName())
    else
        Target.freeze = true
        Target.freezeLocation = Target:getOrigin()
        self:Info("Froze " .. Target:getName())
    end
    return self.SUCCESS
end

return Freeze