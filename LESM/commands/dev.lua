local DevCommand = Command("Dev")


DevCommand.ALIASES        = { "development", }
DevCommand.ADMIN          = true
DevCommand.CHAT           = true
DevCommand.LUA            = true
DevCommand.AUTH           = true
DevCommand.CONSOLE        = true
DevCommand.REQUIRE_AUTH   = true
DevCommand.DESCRIPTION    = { "Used to test features of LESM.", }
DevCommand.SYNTAX         = {
	{ "prune rud seen [days]", "Remove all clients not seen within the last <days> from RegularUserData. (Defaults to 60 days)" },
	{ "leveltime [seconds]", "Tests various leveltime values with RoutineHandler functions"},
	{ "stringlength [length] [clean]", "Tests string messages through print function, add 'clean' to use non formmated string"},
	{ "lorem", "Tests multi line strings with lorem place holder string" },
	{ "getcvar", "Queries r_mode cvar" },
	{ "resetcommands", "Rests command.json to default values (WARN: Erases old Commands.json)" },
}

local _lower = function(s)
	return Misc:trim(s,true)
end
function DevCommand:Run(Client,Args)

	if _lower(Args[1]) == "prune" then -- PRUNE DATABASES
		if _lower(Args[2]) == "rud" and _lower(Args[3]) == "seen" then -- PRUNE RegularUserData based off last seen date
			local currentmax = tlen(RegularUserData)
			local days = tonumber(Args[4]) or 60
			local seconds = 86400 * days -- 86400 = seconds in day
			local t = { }
			local ctime = os.time() -- current time
			for k=1, tlen(RegularUserData) do
				local rudclient = RegularUserData[k]
				local lastseen = ctime - rudclient.lastseen
				if lastseen < seconds then
					t[tlen(t)+1] = rudclient -- Create a list of only those that were seen within the time frame
				end
			end
			RegularUserData = t -- Set the RUD table to the new table of users.
			return self:Info("Pruned " .. currentmax - tlen(RegularUserData) .. " users")
		end
	elseif _lower(Args[1]) == "leveltime" then
		local clt = Game:getLevelTime()
		self:Info(va("Begin test currentLevelTime[%s] GameStartTime[%s] GameLevelTimeDiff[%s]", tostring(clt), tostring(Game.StartTime), tostring(Game.LTDiff)))

		RoutineHandler:add( function ()
			self:Info(va("End test currentLevelTime[%s]", tostring(clt)))
		end,0,tonumber(Args[2]) or 1, "Testing leveltime" )
		return self.SUCCESS
	elseif _lower(Args[1]) == "stringlength" then
		local message = ""
		local num     = tonumber(Args[2]) or 0
		--message = string.rep(message,num)
		for _=1, num do 
			message = message .. "0"
		end
		-- WIDTH[1280] CHAT[64] PRINT[155]
		-- WIDTH[800]  CHAT[64] PRINT[95]
		self:Info(va("Testing string with length %d arg = %d, your maxchars is %d", string.len(message), num, Client:getMaxChars()))
		if _lower(Args[3]) == "clean" then
			Client:ChatClean(message)
			Client:PrintClean("------------- SEPERATOR -------------")
			Client:PrintClean(message)
		else
			Client:Chat(message)
			Client:Print("------------- SEPERATOR -------------")
			Client:Print(message)
		end
		return self.SUCCESS
	elseif _lower(Args[1]) == "lorem" then
		local _placeholderstr = [[But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?]]
		Client:Print(_placeholderstr)
		Client:Print( va("Testing your name is: %s and map is: %s" , Client:getName() , Game:Map() ) )
		return self.SUCCESS
	elseif _lower(Args[1]) == "getcvar" then
		et.G_QueryClientCvar( Client.id, "r_mode")
		self:Info("Querying r_mode...")
		return self.SUCCESS
	elseif _lower(Args[1]) == "resetcommands" then
		local tmp_defaults = dofile(Core.basepath .. "LESM/util/defaults.lua").commands
		local tmp_list = { }
		for k=1,tlen(tmp_defaults) do
			tmp_list[tlen(tmp_list)+1] =  { path = tmp_defaults[k], }
		end
		table.sort(tmp_list,function(a,b) return a.path<b.path end)
		FileHandler:Save{ filePath = "LESM/save/Commands.json", fileData = tmp_list }
		self:Info("Rewrote commands.json")
		return self.SUCCESS
	end
	return CommandHandler:getHelp(Client, self.NAME)
end

return DevCommand