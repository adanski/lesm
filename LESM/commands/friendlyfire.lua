local FriendlyFireCommand = Command("FriendlyFire")

FriendlyFireCommand.ADMIN          = true
FriendlyFireCommand.CHAT           = true
FriendlyFireCommand.LUA            = true
FriendlyFireCommand.AUTH           = true
FriendlyFireCommand.REQUIRE_LEVEL  = Config.Level.Leader
FriendlyFireCommand.DESCRIPTION    = { "Toggles friendly fire settings", "Friendly fire needs to be for half and reflect to work." }
FriendlyFireCommand.ALIASES        = { "ff","teamdamage", }
FriendlyFireCommand.SYNTAX         = {
    { "","Toggles friendly fire on and off",},
    { "half" , "Toggles half friendly fire damage" },
    { "reflect" , "Toggles reflect friendly fire damage" },
}

FriendlyFireCommand.REQUIRE_MOD    = { "silent", "nq" ,"noquarter"}

function FriendlyFireCommand:Run(Client,Args)
    if ( Game:Mod() == "nq" ) or ( Game:Mod() == "noquarter" ) then
        if ( Args[1] == nil ) then
            if ( Game:bitCvarToggle("g_friendlyFire",1) ) then
                self:InfoAll("Friendly Fire Disabled!")
            else
                self:InfoAll("Friendly Fire Enabled!")
            end
            return self.SUCCESS
        end
        Args[1] = string.lower(Args[1])
        if ( Args[1] == "half" ) then
            if ( Game:bitCvarToggle("g_friendlyFire",2) ) then
                self:InfoAll("Friendly Fire Half Damage Disabled!")
            else
                self:InfoAll("Friendly Fire Half Damage Enabled!")
            end
        elseif ( Args[1] == "reflect" ) then
            if ( Game:bitCvarToggle("g_friendlyFire",4) ) then
                self:InfoAll("Friendly Fire Reflect Disabled!")
            else
                self:InfoAll("Friendly Fire Reflect Enabled!")
            end
        else
            self:Error("Mod version doesnt support this sub-command")
            return self:getHelp()
        end
    elseif ( Game:Mod() == "silent" ) then
        if ( Args[1] == nil ) then
            local ff = tonumber(et.trap_Cvar_Get("g_friendlyFire"))
            if ( ff == 1 ) then
                et.trap_Cvar_Set("g_friendlyFire",0)
                self:InfoAll("Friendly Fire Disabled!")
            else
                et.trap_Cvar_Set("g_friendlyFire",0)
                self:InfoAll("Friendly Fire Enabled!")
            end
            return self.SUCCESS
        end
        Args[1] = string.lower(Args[1])
        if ( Args[1] == "half" ) then
            local ff_reflect = tonumber(et.trap_Cvar_Get("g_reflectFriendlyFire"))
            if ( ff_reflect ~= 0.5 ) then
                et.trap_Cvar_Get("g_reflectFriendlyFire",0.5)
                Console:Chat("Friendly Fire Reflect Half Damage!")
            else
                et.trap_Cvar_Get("g_reflectFriendlyFire",0.0)
                Console:Chat("Friendly Fire Reflect No Damage!")
            end
        elseif ( Args[1] == "reflect" ) then
            local ff_reflect = tonumber(et.trap_Cvar_Get("g_reflectFriendlyFire"))
            if ( ff_reflect ~= 0.5 ) then
                et.trap_Cvar_Get("g_reflectFriendlyFire",1.0)
                Console:Chat("Friendly Fire Reflect Full Damage!")
            else
                et.trap_Cvar_Get("g_reflectFriendlyFire",0.0)
                Console:Chat("Friendly Fire No Damage!")
            end
        else
            self:Error("Mod version doesn't support this sub-command")
            return self:getHelp()
        end
    else
        self:Error("Mod not supported")
        return self:getHelp()
    end
end

return FriendlyFireCommand