local ByeCommand = Command("Bye")

ByeCommand.CORE           = true
ByeCommand.ADMIN          = true
ByeCommand.CHAT           = true
ByeCommand.LUA            = true
ByeCommand.AUTH           = true
ByeCommand.CONSOLE        = true
ByeCommand.GLOBALOUTPUT   = true
ByeCommand.DESCRIPTION    = { "Says goodbye!!","Also plays a hello sound" }
ByeCommand.ALIASES        = {"bai","goodbye","cya", }
ByeCommand.SYNTAX         = {
    { "","Say goodbye!" },
    { "<client>", "Say goodbye to specified person" },
    { "@last", "Say goodbye to last person that connected" },
}
ByeCommand.EXAMPLES       = {
    { "Bob", "Says goodbye to bob!"},
    { "@last", "Says goodbye to steve! ( Since he was the last person that connected",},
}

function ByeCommand:Run(Client,Args)
    local message = Client:getName() .. Color.Primary .. " says goodbye"
    Sound:playAll(Config.Sound.Bye,Client)
    if Args[1] then
        local bye_message = Misc:trim(table.concat(Args, " "))
        if bye_message ~= "" then
            bye_message = MessageHandler:filter(bye_message,Client)
            self:InfoAll(message .. " to " .. Color.Primary .. bye_message .. Color.Tertiary .. "!")
            return self.SUCCESS
        end
    end
    self:InfoAll(message .. Color.Tertiary .. "!")
    return self.SUCCESS
end

return ByeCommand