local MapListCommand = Command("MapList")

MapListCommand.ADMIN          = true
MapListCommand.CHAT           = true
MapListCommand.LUA            = true
MapListCommand.AUTH           = true
MapListCommand.CORE           = true
MapListCommand.CONSOLE        = true
MapListCommand.DESCRIPTION    = { "Gets a list of maps in file.", }
MapListCommand.ALIASES        = { "map", "maps", }
MapListCommand.SYNTAX         = {
    { "","Get a list of maps in the cycle" }, 
    { "<mapnum>","Loads <mapnum>" }, 
    { "<mapname>","Loads <mapname> , must be in Maps.json" }, 
}

function MapListCommand:Run(Client,Args)
    local maps,current_map = Core:MapList()
    if not current_map or not current_map.id then
        current_map    = { }
        current_map.id = 999 -- will just not show
    end
    local longest = Misc:table_longestString(maps,"mapname")
    if not Args[1] or Misc:triml(Args[1]) == 'list' then -- READ ALL MAPS
        self:Info(Color.Secondary .. tlen(maps) .. Color.Primary ..  " maps in cycle ^zSee console for list" )
        for k = 1 , tlen(maps) do 
            if maps[k].vstr == current_map.id then
                --Misc:GetStaticString(maps[k].mapname,longest)
                self:InfoClean(Color.Tertiary .. "#" .. Color.Secondary .. Misc:GetStaticString(tostring(maps[k].id),3) .. "^2 " .. maps[k].mapname )
            else
                self:InfoClean(Color.Tertiary .. "#" .. Color.Secondary .. Misc:GetStaticString(tostring(maps[k].id),3) .. "^7 " .. maps[k].mapname )
            end
        end
        if next(Game.Maps) then
            self:InfoClean("Total map pool: " .. Color.Secondary .. tlen(Game.Maps))
            local PT = Misc:FormatList( Game.Maps , false , Client:getMaxChars() , Client:getMaxRows() , tonumber(Args[2]) )
            for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        end
        return self.SUCCESS
    end
    if Misc:triml(Args[1]) == "restart" or Misc:triml(Args[1]) == "reset" then
        self:Info("Restarting map")
        et.trap_SendConsoleCommand( et.EXEC_APPEND , "map_restart\n" )
        return self.SUCCESS
    end
    if tonumber(Args[1]) and ( Client:isConsole() or Client:isAuth() or Client:getLevel() >= Config.Level.Leader ) then
        local f = false
        for k = 1 , tlen(maps) do 
            if maps[k].id == tonumber(Args[1]) then
                self:Info("Loading map " .. Color.Tertiary .."#" .. maps[k].id .. Color.Secondary .. maps[k].mapname)
                et.trap_SendConsoleCommand( et.EXEC_APPEND , "vstr " .. Config.MapListVarName .. tostring(maps[k].id) .. "\n" )
                break
            end
        end
        return self.SUCCESS
    end
    local mapname = Misc:triml(Args[1])
    if Misc:table_find(mapname) then
        self:InfoAll("Loading map " .. Color.Secondary .. mapname)
        et.trap_SendConsoleCommand( et.EXEC_APPEND , "map " ..mapname .. "\n" )
        return self.SUCCESS
    else
        return self:Error("Could not find map")
    end
    return self:getHelp()
end

return MapListCommand