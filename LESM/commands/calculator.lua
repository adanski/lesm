local CalculatorCommand = Command("Calculator")

CalculatorCommand.ADMIN          = true
CalculatorCommand.CHAT           = true
CalculatorCommand.LUA            = true
CalculatorCommand.AUTH           = true
CalculatorCommand.CONSOLE        = true
CalculatorCommand.GLOBALOUTPUT   = true
CalculatorCommand.DESCRIPTION    = { "Simple calculator.",
                                     "( X Operator Y ) = Answer.",
                                     "Operators: ( + - x / % ^7^ " .. Color.Secondary ..")",
                                   }
CalculatorCommand.ALIASES        = { "calc", "calculate", }
CalculatorCommand.SYNTAX         = {
    { "<x> <operator> <y>", "Does simple math " },
}
CalculatorCommand.EXAMPLES       = {
    { "2 + 3","Example would print 5", }
}

function CalculatorCommand:Run(Client,Args)
    local equation = table.concat(Args)
    local _pattern = "(%d+)([%.%d]*)([%-%+%^%%%*/modsqr]+)(%d+)([%.%d]*)"
    if not equation or equation == "" or not string.find(equation,_pattern)  then
        self:Error("Missing equation")
        return self:getHelp()
    end
    local x1,x2,op,y1,y2 = Misc:match(equation,_pattern)
    local x = x1
    local y = y1
    if x2 then x = tonumber( tostring(x1) .. tostring(x2)) end
    if y2 then y = tonumber( tostring(y1) .. tostring(y2)) end
    local equals = 0
    if op == "+" then
        equals = x + y
    elseif op == "-" then
        equals = x - y
    elseif op == "/" then
        equals = x / y
    elseif ( op == "*" or op == "x" ) then
        equals = x * y
    elseif op == "^" or op == "sqr" then
        op = "SquareRoot"
        equals = math.sqrt( x , y )
    elseif op == "%" or op == "mod" then
        op = "Modulus"
        equals = math.mod( x , y )
    else
        self:Error("Unknown operator")
        return self:getHelp()
    end
    local Shift = 10 ^ 4
    equals = math.floor( equals*Shift + 0.5 ) / Shift -- Do I want floor?
    self:InfoAll(Color.Secondary .. x .. Color.Tertiary .. " " .. op .. " " .. Color.Secondary .. y .. Color.Tertiary .. " = " .. Color.Primary .. equals)
    return self.SUCCESS
end


return CalculatorCommand