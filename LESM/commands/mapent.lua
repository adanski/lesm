local MapEnt = Command("MapEnt")

MapEnt.CORE           = true
MapEnt.ADMIN          = true
MapEnt.CHAT           = true
MapEnt.LUA            = true
MapEnt.AUTH           = true
MapEnt.REQUIRE_LOGIN  = true
MapEnt.REQUIRE_AUTH   = true
MapEnt.DESCRIPTION    = { "Creates a map entity",}
MapEnt.SYNTAX         = {
    { "" , "Lists all current map ents"},
    { "<mapent>" , "Lists all map ent keys"},
    { "add <scriptName>" , "Creates a mapent with scriptname for this map"},
    { "delete <mapent>" , "Deletes a mapent"},
    { "edit <mapent> <key> <value>" , "Edits a mapent"},
}

function MapEnt:Run(Client,Args)
    if not Args[1] then
        self:Info("Found " .. Color.Secondary .. tlen(Entities) .. Color.Primary .. " map entities")
        for k = 1 , tlen(Entities) do 
            self:InfoClean(Color.Tertiary .. "#" .. Color.Secondary .. k .. Color.Primary .. " " .. Entities[k].scriptname)
        end
        return self.SUCCESS
    end
    if tonumber(Args[1]) then
        local mapent = Entities[tonumber(Args[1])]
        if not mapent then
            self:Error("No mapent with that number")
            return self.SUCCESS
        end
        for i,v in pairs(mapent) do
            self:InfoClean(Color.Secondary .. tostring(i) .. Color.Tertiary .. " : " .. Color.Primary .. tostring(v))
        end
        return self.SUCCESS
    elseif Misc:triml(Args[1]) == "add" then
        if not Args[2] then
            self:Error("Need a scriptname to add")
            return self.SUCCESS
        end
        local newmapent = {
            map                  = Game:Map(),
            scriptname           = Misc:trim(Args[2]),
            find                 = false,
            free                 = false,
            loop                 = false,
            timeleft             = false,
            players              = false,
            startmessage         = "",
            endmessage           = "",
            distancemessage      = "",
            distanceorigin       = false,
            distanceclass        = false,
            distanceteam         = false,
            distancehasobjective = false,
        }
        Entities[tlen(Entities)+1] = newmapent
        FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Entities,fileData =Entities}
        self:Info("Saved new map ent")
        newmapent.state = 0 -- set to not running
        return self.SUCCESS
    elseif Misc:triml(Args[1]) == "edit" then
        local mapentid = tonumber(Args[2])
        if not mapentid or not Entities[mapentid] then
            self:Error("No map ent by that number")
            return self.SUCCESS
        end
        local mapent = Entities[mapentid]
        local key    = Misc:triml(Args[3])
        if key == "" then
            self:Error("Need key")
            return self.SUCCESS
        end
        if key == "find" or key == "loop" or key == "free" or key == "distancehasobjective" then
            if mapent[key] then
                mapent[key] = false
            else
                mapent[key] = true
            end
        FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Entities,fileData =Entities}
            self:Info("Toggled " .. key)
            return self.SUCCESS
        end
        local value  = Misc:trim(table.concat(Args," ",4))
        if value == "" then
            self:Error("Need value")
            return self.SUCCESS
        end
        if key == "scriptname" or key == map or key == "startmessage" or key == "distancemessage" or key == "endmessage" then
            mapent[key] = value
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Entities,fileData =Entities}
            self:Info("Updated " .. key .. " to " .. value)
            return self.SUCCESS
        elseif key == "timeleft" or key == "players" or key == "distanceclass" or key == "distanceteam" then
            local value = tonumber(value) or false
            mapent[key] = value
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Entities,fileData =Entities}
            self:Info("Updated " .. key .. " to " .. tostring(value) )
            return self.SUCCESS
        elseif key == "distanceorigin" then
            mapent[key] = { tonumber(Args[4]) or 0, tonumber(Args[5]) or 0, tonumber(Args[6]) or 0, }
            FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Entities,fileData =Entities}
            self:Info("Updated distance origin to " .. table.concat(mapent[key]," , "))
            return self.SUCCESS
        end
        self:Error("Unknown key")
        return self.SUCCESS
    elseif Misc:triml(Args[1]) == "delete" then
        local mapentid = tonumber(Args[2])
        if not mapentid or not Entities[mapentid] then
            self:Error("No map ent by that number")
            return self.SUCCESS
        end
        table.remove(Entities,mapentid)
        FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Entities,fileData =Entities}
        self:Info("Removed " .. mapentid)
        return self.SUCCESS
    else
        self:Error("unknown sub command")
        return self.SUCCESS
    end
end

return MapEnt