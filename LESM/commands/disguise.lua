local DisguiseCommand = Command("Disguise")

DisguiseCommand.ADMIN          = true
DisguiseCommand.CHAT           = true
DisguiseCommand.LUA            = true
DisguiseCommand.AUTH           = true
DisguiseCommand.REQUIRE_LOGIN  = true
DisguiseCommand.REQUIRE_AUTH   = true
DisguiseCommand.DESCRIPTION    = { "Gives disguise.", "^1This command needs more testing", }
DisguiseCommand.ALIASES        = { "disg", }
DisguiseCommand.SYNTAX         = {
    { "" , "Gives yourself a disguise", },
    { "<clientname>", "Gives <clientname> a disguise", },
    { "as <clientname>", "Disguises you as <clientname>", },
    { "custom <class> <rank> <name>", "Gives you a custom disguise", },
}

function DisguiseCommand:Run(Client,Args)
    if not Args[1] then
        local name = Misc:randomWord()
        Client:setDisguise(math.random(0,4),math.random(0,4),name)
        self:Info("Disguised as " .. Color.Secondary .. name)
        return self.SUCCESS
    end
    if ( Args[1] == "as" ) then
        local Target = ClientHandler:getClient(Args[2])
        if not Target then
            self:Error("Could not find client")
            return self:getHelp()
        end
        local name = Target:getName()
        Client:setDisguise(Target:getClass(),math.random(0,4),name) -- Dunno how to get rank yet
        self:Info("Disguised as " .. Color.Secondary .. name)
        return self.SUCCESS
    elseif ( Args[1] == "custom" ) then
        local class = tonumber(Args[2])
        local rank = tonumber(Args[3])
        local name  = Misc:trim(table.concat(Args," " , 3))
        Client:setDisguise(class,rank,name) -- Dunno how to get rank yet
        self:Info("Disguised as " .. Color.Secondary .. name)
        return self.SUCCESS
    end
    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not find client:")
        return self:getHelp()
    end
    local name = Misc:randomWord()
    Target:setDisguise(math.random(0,4),math.random(0,4),name)
    self:Info("Disguised " .. Target:getName() .. Color.Primary .. " as " .. Color.Secondary .. name)
    return self.SUCCESS
end

return DisguiseCommand