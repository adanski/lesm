local RemoveUserCommand = Command("RemoveUser")

RemoveUserCommand.AUTH           = true
RemoveUserCommand.CONSOLE        = true
RemoveUserCommand.REQUIRE_LOGIN  = true
RemoveUserCommand.REQUIRE_AUTH   = true
RemoveUserCommand.DESCRIPTION    = { "Removes a username", "^1UNSAFE Be careful" , "A backup will be made in save directory", }
RemoveUserCommand.SYNTAX         = {
    { "<username>", "Removes <username> from existence!"}
}

function RemoveUserCommand:Run(Client,Args)
    local TargetUser = UserHandler:getUser(Args[1])
    if not TargetUser then
        self:Error("Could not find user")
        return self:getHelp()
    end
    local TargetUserClient = TargetUser:getOnlineClient()
    if TargetUserClient then
        TargetUserClient:Logout(true)
    end
    local backup = { }
    for k = 1 , tlen(Keys) do 
        backup[Keys[k].name] = Misc:ConvertType(Keys[k].type,TargetUser[Keys[k].name])
    end
    FileHandler:Save{ filePath = "LESM/save/rm_" .. TargetUser:getKey("username") .."_" .. os.time() .. ".json",fileData = backup}
    UserHandler:delete(TargetUser.id)
    self:Info("Removing user " .. tostring(backup.username))
    return self.SUCCESS
end

return RemoveUserCommand