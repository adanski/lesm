local FingerCommand = Command("Finger")


FingerCommand.ADMIN          = true
FingerCommand.CHAT           = true
FingerCommand.CONSOLE        = true
if not Game:Shrubbot() then
    FingerCommand.CORE           = true
    FingerCommand.LUA            = true
    FingerCommand.AUTH           = true
    FingerCommand.REQUIRE_LEVEL  = Config.Level.Admin
    FingerCommand.REQUIRE_LOGIN  = true
    FingerCommand.OVERWRITE      = false
else
    FingerCommand.OVERWRITE      = true
    FingerCommand.CORE           = false
    FingerCommand.LUA            = false
    FingerCommand.AUTH           = false
    FingerCommand.REQUIRE_LEVEL  = 0
    FingerCommand.REQUIRE_LOGIN  = false
    FingerCommand.REQUIRE_FLAG   = 'e'
end

FingerCommand.DESCRIPTION    = { "Fingers client for info." , "Use the list all command to see list of offline ids" }
FingerCommand.ALIASES        = { "whois","aliases", }
FingerCommand.SYNTAX         = {
    { "<client>" , "Fingers client"},
    { "offline <offlineclient>" , "Fingers offline client"},
    { "<client> aliases <page>" , "Gets client's alias table"},
    { "offline <offlineclient> aliases <page>" , "Gets offline client's alias table"},
    { "<client> ips <page>" , "Gets client's ip table"},
    { "offline <offlineclient> ips <page>" , "Gets offline client's ip table"},
    { "<client> guids <page>" , "Gets client's guid table"},
    { "offline <offlineclient> guids <page>" , "Gets offline client's guid table"},
    { "<client> warnings <page>" , "Gets client's warnings"},
    { "offline <offlineclient> warnings <page>" , "Gets offline client's warnings"},
    { "<client> warning <warnid>" , "Gets client's specific warning data"},
    { "offline <offlineclient> warning <warnid>" , "Gets offline client's specific warning data"},
}

function FingerCommand:Run(Client,Args)
    if Game:Shrubbot() and tlen(Args) < 2 then return self.PASS end -- Pass back to shrubbot to do normal finger
    local fingerTable = { }
    local _message = function ( label , data )
        if label and label ~= '' and data and data ~= '' then
            fingerTable[tlen(fingerTable)+1] = { label , data }
        end
    end
    
    local _AliasesTable = function( aliases ,  page )
        if not aliases or not next(aliases) then return self:Error( "No aliases found" ) end
        local PT = Misc:FormatList( aliases , false , Client:getMaxChars() , Client:getMaxRows() , page )
        for k=1,tlen(PT) do
            self:InfoClean(PT[k])
        end
        return self.SUCCESS
    end
    local _IpTable = function( ips ,  page )
        if not ips or not next(ips) then return self:Error( "No ips found" ) end
        local PT = Misc:FormatList( ips , false , Client:getMaxChars() , Client:getMaxRows() , page )
        for k=1,tlen(PT) do
            self:InfoClean(PT[k])
        end
        return self.SUCCESS
    end
    local _WarningsTable = function( warnings ,  page )
        if not warnings or not next(warnings) then return self:Error( "No warnings found" ) end
        local warningdata = { }
        local warnExpireTime = Misc:getTimeFormat(Config.Warn.Expires)
        for k=1,tlen(warnings) do
            local w = warnings[k]
            
            --- CURRENTTIME : WARNTIME[1000] + expiretime[20] - 1010 -  = 10
            local expires = ( w.time + warnExpireTime ) - os.time()
            if expires > 0 then
                expires = Misc:SecondsToClock(expires)
            else
                expires = "Expired"
            end
            warningdata[tlen(warningdata)+1] = {
                w.id,
                w.warner,
                os.date("%c",w.time),
                expires,
                w.reason,
                }
        end
        local PT = Misc:FormatTable(warningdata,{"ID","WARNER","DATE","EXPIRES","REASON"},false,Client:getMaxChars(),Client:getMaxRows(),page)
        if not warningdata or not next(warningdata) then return self:Error( "Incorrect warnings found" ) end
        for j=1,tlen(PT) do self:InfoClean(PT[j]) end
        return self.SUCCESS
    end
    local _WarningTable = function( warnings , warnid )
        if not warnings or not next(warnings) then return self:Error( "No warnings found" ) end
        local warn = RegularUser:getWarnById(warnings,warnid)
        if not warn or not next(warn) then return self:Error("Could not find warning by that id") end
        self:Info( va("Info for warn id: %d" , warn.id))
        self:InfoClean( va("Warned by %s at %s" , Color.Secondary .. warn.warner .. Color.Primary , Color.Secondary .. os.date("%c",warn.time) ))
        local warnExpireTime = Misc:getTimeFormat(Config.Warn.Expires)
        local expires = ( warn.time + warnExpireTime ) - os.time()
        if expires > 0 then
            self:InfoClean(va("Expires in %s", Color.Secondary .. Misc:SecondsToClock(expires) ))
        else
            self:InfoClean("Warn Expired")
        end
        self:InfoClean(va("Reason for warning: %s" , warn.reason ))
        return self.SUCCESS
    end
    local _GuidTable = function( guids , silentids , etproguids ,  page )
        local f = false
        if guids and next(guids) then
            f=true
            local PT = Misc:FormatList( guids , false , Client:getMaxChars() , Client:getMaxRows() , page )
            for k=1,tlen(PT) do self:InfoClean(PT[k]) end
        end
        if silentids and next(silentids) then
            f=true
            local PT = Misc:FormatList( silentids , false , Client:getMaxChars() , Client:getMaxRows() , page )
            for k=1,tlen(PT) do self:InfoClean(PT[k]) end
        end
        if etproguids and next(etproguids) then
            f=true
            local PT = Misc:FormatList( etproguids , false , Client:getMaxChars() , Client:getMaxRows() , page )
            for k=1,tlen(PT) do self:InfoClean(PT[k]) end
        end
        if not f then return self:Error("No guids found!") end
        return self.SUCCESS
    end
    
    if Misc:triml( Args[1] ) == 'offline' then
        local rudclient = RegularUser:find( Args[2] )
        if not rudclient then return self:Error( "Could not find offline client" ) end
        if Misc:triml( Args[3] ) == 'aliases' then
            return _AliasesTable( rudclient.aliases , Args[4] )
        elseif Misc:trim( Args[3] ) == 'ips' then
            return _IpTable( rudclient.ips , Args[4] )
        elseif Misc:trim( Args[3] ) == 'warnings' then
            return _WarningsTable( rudclient.warnings , Args[4] )
        elseif Misc:trim( Args[3] ) == 'warning' or Misc:triml( Args[3] ) == 'warn' then
            return _WarningTable( rudclient.warnings , Args[4] )
        elseif Misc:trim( Args[3] ) == 'guids' then
            return _GuidTable( rudclient.guids , rudclient.silentids , rudclient.etproguids , Args[4] )
        end
        local warnings = 0
        for k=1,tlen(rudclient.warnings) do
            if not RegularUser:checkWarnExpired(rudclient.warnings[k]) then
                warnings = warnings + 1
            end
        end
        _message( "Id"           , rudclient.id )
        _message( "Ident"        , rudclient.ident )
        _message( "Name"         , rudclient.lastname )
        _message( "isBot"        , rudclient.bot )
        _message( "Last Seen"    , os.date( "%c" , rudclient.lastseen ) )
        _message( "Autolevel"    , rudclient.autolevel )
        _message( "SwearFilter"  , rudclient.swearfilter )
        _message( "NoCaps"       , rudclient.nocaps )
        _message( "NoColors"     , rudclient.nocolors )
        _message( "GrammarCheck" , rudclient.grammarcheck )
        _message( "Warnings"     , warnings )
        if rudclient.mutestrict then
            _message( "Muted"     , "Strict" )
        elseif rudclient.mutetime == -1 then
            _message( "Muted"     , "Permanent" )
        elseif rudclient.mutetime - os.time() > 0 then
            _message( "Muted"     , Misc:SecondsToClock(rudclient.mutetime-os.time()) )
        end
        if not next(fingerTable) then return self:Error( va("No data found for %s" , tostring(rudclient.lastname) )) end
    else
        local Target = ClientHandler:getClient(Args[1])
        if not Target then return self:Error("Could not find client") end
        if Target.rudclient then -- There is no error
            if Misc:triml( Args[2] ) == 'aliases' then
                return _AliasesTable( Target.rudclient.aliases , Args[3] )
            elseif Misc:trim( Args[2] ) == 'ips' then
                return _IpTable( Target.rudclient.ips , Args[3] )
            elseif Misc:trim( Args[2] ) == 'guids' then
                return _GuidTable( Target.rudclient.guids , Target.rudclient.silentids , Target.rudclient.etproguids , Args[3] )
            elseif Misc:trim( Args[2] ) == 'warnings' then
                return _WarningsTable( Target.rudclient.warnings , Args[3] )
            elseif Misc:trim( Args[2] ) == 'warning' or Misc:triml( Args[2] ) == 'warn' then
                return _WarningTable( Target.rudclient.warnings , Args[3] )
            end
        end
        local guid,silent,etpro = Target:getGuids()
        local connection        = tonumber(Target:entGet("pers.connected")) or -1
        local connectionStatus  = "UNKNOWN"
        
        if connection == 0 then
            connectionStatus = "Disconnected"
        elseif connection == 1 then
            connectionStatus = "Connecting"
        elseif connection == 2 then
            connectionStatus = "Connected"
        end
        
        _message( "Slot"            , Target.id )
        _message( "Name"            , Target:getName() )
        _message( "IP"              , Target:getIp() )
        _message( "GUID"            , guid )
        _message( "Silent Guid"     , silent )
        _message( "ETPro Guid"      , etpro )
        _message( "Country"         , Target:getCountry() )
        _message( "ET Protocol"     , Target:getProtocol() )
        _message( "ET Version"      , Target:getVersion() )
        _message( "Total XP"        , Target:getTotalXp() )
        _message( "Connection"      , connectionStatus )
        _message( "Server Password" , Target:getPassword() )
        if Target.rudclient then
            local warnings = 0
            for k=1,tlen(Target.rudclient.warnings) do
                if not RegularUser:checkWarnExpired(Target.rudclient.warnings[k]) then
                    warnings = warnings + 1
                end
            end
            _message( "Offline Id" , Target.rudclient.id )
            _message( "Offline Ident" , Target.rudclient.ident )
            _message( "Warnings"     , warnings )
            if Target.rudclient.mutestrict then
                _message( "Muted"     , "Strict" )
            elseif Target.rudclient.mutetime == -1 then
                _message( "Muted"     , "Permanent" )
            elseif Target.rudclient.mutetime - os.time() > 0 then
                _message( "Muted"     , Misc:SecondsToClock(Target.rudclient.mutetime-os.time()) )
            end
        else
            self:Error("RegularUser data not found")
        end
        if not next(fingerTable) then return self:Error( va("No data found for %s" , Target:getName() )) end
    end
    
    self:Info( "See console for details" )
    local PT = Misc:FormatTable(fingerTable,{"Label","Data"},false,Client:getMaxChars())
    for k=1,tlen( PT ) do self:InfoClean( PT[k] ) end
    
    return self.SUCCESS
end

return FingerCommand