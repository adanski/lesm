local ListCommand = Command("List")

ListCommand.ADMIN          = true
ListCommand.CHAT           = true
ListCommand.CONSOLE        = true
if not Game:Shrubbot() then
    ListCommand.CORE           = true
    ListCommand.LUA            = true
    ListCommand.AUTH           = true
    ListCommand.REQUIRE_LEVEL  = Config.Level.Admin
    ListCommand.REQUIRE_LOGIN  = true
    ListCommand.OVERWRITE      = false
else
    ListCommand.OVERWRITE      = true
    ListCommand.CORE           = false
    ListCommand.LUA            = false
    ListCommand.AUTH           = false
    ListCommand.REQUIRE_LEVEL  = 0
    ListCommand.REQUIRE_LOGIN  = false
    ListCommand.REQUIRE_FLAG   = 'i'
end
ListCommand.DESCRIPTION    = { "List currently connected clients" , }
ListCommand.ALIASES        = { "listplayers","li", }
ListCommand.SYNTAX         = {
    { "[page]" , "List currently connected clients"},
    { "all [page]" , "List offline clients"},
}

function ListCommand:Run(Client,Args)
    if Misc:trim(Args[1],true) ~= "all" and Game:Shrubbot() then return self.PASS end -- pass back to shrubbot
    if not Args[1] or tonumber(Args[1]) then
        local Data = { }
        for k=1 , tlen(Clients) do 
            local ClientData = { }
            local team       = Clients[k]:getTeam()
            if team == et.TEAM_AXIS then
                team = "^1R"
            elseif team == et.TEAM_ALLIES then
                team = "^4B"
            else
                team = "^3S"
            end
            
            local level             = Clients[k]:getLevel()
            local guid,silent,etpro = Clients[k]:getGuids()
            if silent ~= "" then guid = silent end
            if etpro ~= "" then guid = etpro end
            
            if guid == "" then
                guid = "*UNKNOWN"
            else
                if Clients[k]:isBot() then
                    guid = string.upper(string.sub(guid, 1, 8))
                else
                    guid = string.upper(string.sub(guid, -8, -1))
                end
            end
            
            ClientData[tlen(ClientData)+1] = Clients[k].id
            ClientData[tlen(ClientData)+1] = team
            ClientData[tlen(ClientData)+1] = level
            ClientData[tlen(ClientData)+1] = Clients[k]:getName()
            ClientData[tlen(ClientData)+1] = guid
            if not Client:isConsole() then
                ClientData[tlen(ClientData)+1] = tostring(Client:isIgnored(Clients[k]))
            else
                ClientData[tlen(ClientData)+1] = "false"
            end
            Data[tlen(Data)+1] = ClientData
        end
        
        if not next(Data) then
            self:Info(Color.Secondary .. "0" .. Color.Primary .. " connected clients")
            return self.SUCCESS
        end
        
        self:Info(Color.Secondary .. tlen(Clients) .. Color.Primary .. " connected clients")
        local PT = Misc:FormatTable(Data,{"ID","T","L","NAME","GUID","IGNORED"},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[1]))
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    end
    if ( Client:isConsole() or ( Client:getLevel() >= Config.Level.Admin or Client:isAuth() ) ) and ( Misc:trim(Args[1]) == "all" or Misc:trim(Args[1]) == "offline" or Misc:trim(Args[1]) == "list" ) then
        local rud_list = { }
        for k = 1 , tlen(RegularUserData) do 
            local rudclient = RegularUserData[k]
            local ident = rudclient.ident
            if not rudclient.bot then
                rud_list[tlen(rud_list)+1] = {
                    rudclient.id or 0,
                    rudclient.lastname or "",
                    rudclient.level or 0,
                    tlen(rudclient.warnings) or 0,
                    Misc:int(Misc:table_total(rudclient.skills)) or 0,
                    Misc:SecondsToClock( os.time() - rudclient.lastseen ) .. " ago",
                    ident,
                }
            end
        end
        if not next(rud_list) then return self:Error("No clients saved") end
        self:Info(Color.Secondary .. tlen(rud_list) .. Color.Primary .. " total clients")
        local PT = Misc:FormatTable(rud_list,{"ID","NAME","L","W","XP","LAST SEEN","IDENT",},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[2]))
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    else
        return self:Error( "Insufficient privileges" )
    end
    if Game:Shrubbot() then return self.PASS else return self:Error("Unknown sub command") end
end

return ListCommand