local HealthCommand = Command("Health")

HealthCommand.ADMIN          = true
HealthCommand.CHAT           = true
HealthCommand.LUA            = true
HealthCommand.AUTH           = true
HealthCommand.REQUIRE_LOGIN  = true
HealthCommand.REQUIRE_AUTH   = true
HealthCommand.DESCRIPTION    = { "Gives specified health amount." }
HealthCommand.ALIASES        = { "heal","hp" }
HealthCommand.SYNTAX         = {
    { "" , "Heals you to max hp" },
    { "<health>" , "Adds <health> to your health" },
    { "<client>" , "Heals client to max hp" },
    { "<client> <health>" , "Adds <health> to client's health", },
}

function HealthCommand:Run(Client,Args)
    if not Args[1] then
        local maxHp = tonumber(EntityHandler:safeEntGet(Client.id,"ps.stats",4)) or 125
        Client:setHealth(maxHp)
        self:Info("Healed to full health!")
        return self.SUCCESS
    end
    if tonumber(Args[1]) then
        Client:setHealth(Client:getHealth() + tonumber(Args[1]))
        self:Info("Gave yourself " .. Args[1] .." health")
        return self.SUCCESS
    end
    
    local Target = ClientHandler:getClient(Args[1])
    if not Target then
        self:Error("Could not find client")
        return self:getHelp()
    end
    if not tonumber(Args[2]) then
        local maxHp = tonumber(EntityHandler:safeEntGet(Target.id,"ps.stats",4)) or 125
        Target:setHealth(maxHp)
        self:Info("Healed " .. Target:getName() .. Color.Primary .. " to full health!")
        return self.SUCCESS
    end
    Target:setHealth(Target:getHealth() + tonumber(Args[2]))
    self:Info("Gave " .. Target:getName() .. Color.Secondary .. " " .. Args[2] .. Color.Primary .. " health")
    return self.SUCCESS
end

return HealthCommand