local GetKeyCommand = Command("GetKey")

GetKeyCommand.ADMIN          = true
GetKeyCommand.CHAT           = true
GetKeyCommand.LUA            = true
GetKeyCommand.AUTH           = true
GetKeyCommand.CONSOLE        = true
GetKeyCommand.REQUIRE_LOGIN  = true
GetKeyCommand.REQUIRE_AUTH   = true
GetKeyCommand.DESCRIPTION    = { "Gets User Key", }
GetKeyCommand.SYNTAX         = {
    { "<username> <key>", "Gets <key> from <username>", },
}
GetKeyCommand.EXAMPLES       = {
    { "Bob adminwatch", "Checks if bob has adminwatch enabled or not",},
}

function GetKeyCommand:Run(Client,Args)
    local Target = UserHandler:getUser(Args[1])
    if not Target then
        self:Error("Missing username")
        return self:getHelp()
    end
    local value = Target:getKey(Args[2])
    if not value then
        self:Error("Could not find key value with that key")
        return self:getHelp()
    end
    self:Info(Args[2] .. Color.Tertiary .. " = " .. Color.Secondary .. tostring(value))
    return self.SUCCESS
end

return GetKeyCommand