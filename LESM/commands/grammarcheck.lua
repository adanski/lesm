local GrammarCheckCommand = Command("GrammarCheck")

GrammarCheckCommand.ADMIN          = true
GrammarCheckCommand.CHAT           = true
GrammarCheckCommand.LUA            = true
GrammarCheckCommand.AUTH           = true
GrammarCheckCommand.CONSOLE        = true
GrammarCheckCommand.REQUIRE_LEVEL  = Config.Level.Leader
GrammarCheckCommand.DESCRIPTION    = { "Toggle grammarcheck for client" , }
GrammarCheckCommand.ALIASES        = { "gc","grammar",}
GrammarCheckCommand.SYNTAX         = {
    { "<client>", "Enables Grammarcheck for target",},
}

function GrammarCheckCommand:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Could not find target") end
    if ( Target.id == Client.id ) and not Client:isAuth() then return self:Error("Cannot use this on yourself") end
    if ( Target:getLevel() > Client:getLevel() ) and not Client:isAuth() then return self:Error(Target:getName() .. Color.Primary .. " has higher level than you!") end
    if not Target.rudclient then return self:Error("Target does not have rudclient") end
    if Target.rudclient.grammarcheck then
        Target.rudclient.grammarcheck = false
        self:Info("Disabled grammarcheck for " .. Target:getName())
        return self.SUCCESS
    else
        Target.rudclient.grammarcheck = true
        self:Info("Enabled grammarcheck for " .. Target:getName())
        return self.SUCCESS
    end
end

return GrammarCheckCommand