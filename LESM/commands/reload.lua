local ReloadCommand = Command("Reload")

ReloadCommand.ADMIN          = true
ReloadCommand.CHAT           = true
ReloadCommand.LUA            = true
ReloadCommand.AUTH           = true
ReloadCommand.CONSOLE        = true
ReloadCommand.REQUIRE_LOGIN  = true
ReloadCommand.REQUIRE_AUTH   = true
ReloadCommand.DESCRIPTION    = { "Reloads specific parts of the project", }
ReloadCommand.SYNTAX         = {
    { "commands", "Reloads commands" },
}

function ReloadCommand:Run(Client,Args)
    if Misc:triml(Args[1]) == "commands" then
        self:Info("Reloading commands")
        CommandHandler.Commands = nil
        CommandHandler.Commands = { }
        CommandHandler:Init()
        self:Info("Done")
        return self.SUCCESS
    end
    return self:getHelp()
end

return ReloadCommand