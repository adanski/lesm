local Spec999Command = Command("Spec999")

Spec999Command.ADMIN          = true
Spec999Command.CHAT           = true
Spec999Command.LUA            = true
Spec999Command.AUTH           = true
Spec999Command.CONSOLE        = true
Spec999Command.NOSHRUBBOT     = true
Spec999Command.REQUIRE_LEVEL  = Config.Level.Leader
Spec999Command.DESCRIPTION    = { "Move lagged out clients to spectator" }
Spec999Command.ALIASES        = { "spec9","spec99","lag" }
Spec999Command.SYNTAX         = {
    { "","Move lagged out clients to spectator",},
}

function Spec999Command:Run(Client,Args)
	Game:Spec999()
end

return Spec999Command