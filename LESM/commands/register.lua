local RegisterCommand = Command("Register")

local UserNameKey;
local PasswordKey;
for k = 1 , tlen(Keys) do 
    if Keys[k].name == "username" then
        UserNameKey = Keys[k]
    elseif Keys[k].name == "password" then
        PasswordKey = Keys[k]
    end
end
if not UserNameKey or not PasswordKey then
    Console:Error("Missing important keys for login")
end
RegisterCommand.CORE           = true
RegisterCommand.LUA            = true
RegisterCommand.DESCRIPTION    = { 
                        "Registers a username for your profile." , 
                        "Username and password can't not have spaces, and some select characters",
                        "Make sure to use a unique password, as a malicious admin could potentially see it"
                      }
RegisterCommand.SYNTAX         = {
    { "<username> <password>" , "Registers a profile using username and password"}
}

function RegisterCommand:Run(Client,Args)
    if Client:isOnline() then return self:Error("You already signed in!") end
    
    local ipc,guic,silc = UserHandler:checkAutoLoginDuplicates(Client)
    if ipc >= Config.Profile.MaxIp then
        return self:Error("There have been too many usernames on this network")
    elseif guic >= Config.Profile.MaxGuid then
        return self:Error("There have been too many usernames on this guid")
    elseif silc >= Config.Profile.MaxGuid then
        return self:Error("There have been too many usernames on this silentid")
    end
    
    if Args[3] then return self:Error("There can not be any spaces in username or password") end
    
    local username = Misc:trim(Args[1])
    if string.len(username) < UserNameKey.min then
        return self:Error("Username length is lower than minimum allowed value ( " .. UserNameKey.min .. " - " .. UserNameKey.max .. " )")
    elseif ( UserNameKey.max > 0 ) and ( string.len(username) > UserNameKey.max ) then
        return self:Error("Username length is higher than maximum allowed value ( " .. UserNameKey.min .. " - " .. UserNameKey.max .. " )")
    end
    if UserNameKey.pattern ~= "" then
        local f = string.find(username,UserNameKey.pattern)
        if f then return self:Error("Disallowed character: "  .. string.sub(username,f,f)) end
    end
    if UserHandler:exists(username) then return self:Error("Username already exists") end

    local password = Misc:trim(Args[2])
    if string.len(password) < PasswordKey.min then
        return self:Error("Password length is lower than minimum allowed value ( " .. PasswordKey.min .. " - " .. PasswordKey.max .. " )")
    elseif PasswordKey.max > 0 and ( string.len(password) > PasswordKey.max ) then
        return self:Error("Password length is higher than maximum allowed value ( " .. PasswordKey.min .. " - " .. PasswordKey.max .. " )")
    end
    if PasswordKey.pattern ~= "" then
        local f = string.find(password,PasswordKey.pattern)
        if f then return self:Error("Disallowed character: "  .. string.sub(password,f,f)) end
    end
    password = Core:encrypt(password)
    
    local eGuid,sGuid,etproGuid = Client:getGuids()
    local ip                   = Client:getIp()
    local next_id              = UserHandler:getNextId()
    
    local new_user             = { }
    for k = 1 , tlen(Keys) do new_user[Keys[k].name] = Keys[k].default end
    
    new_user.id            = next_id
    new_user.autologinguid = eGuid
    new_user.autologinip   = ip
    new_user.autologinsid  = sGuid
    new_user.autologinetpro  = etproGuid
    new_user.joindate      = tonumber(os.time())
    new_user.lastseen      = tonumber(os.time())
    new_user.level         = Client:getLevel()
    new_user.password      = password
    new_user.timeout       = (os.time() + ( 3 * 60 * 60 ) ) -- 3 Hours
    new_user.username      = username
    
    Users[tlen(Users)+1] = UserObject(new_user)
    Client.User          = UserHandler:getUser(username)
    Client.User:updateNotFound('ip',ip)
    Client.User:updateNotFound('alias',Client:getCleanName())
    Client.User:updateNotFound('silent',sGuid)
    Client.User:updateNotFound('guid',eGuid)
    Client.User:updateNotFound('etpro',etpro)
    
    self:Info("Successfully registered and logged in!")
    self:Info("See ".. Color.Command .."/"..Config.Prefix.Lua .." help" .. Color.Primary.." for all your new commands")
    self:InfoClean("Autologin is on by default if you want to disable it")
    self:InfoClean("Use the " .. Color.Command .. "/autologin" .. Color.Primary .. " command to toggle it on and off")
    Client:Play(Config.Sound.Register)
    
    Console:Info(Client:getName() .. " registered profile to " .. username)
    return self.SUCCESS
end

return RegisterCommand