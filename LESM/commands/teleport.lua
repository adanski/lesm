local TeleportCommand = Command("Teleport")

    TeleportCommand.CORE           = true
    TeleportCommand.ADMIN          = true
    TeleportCommand.CHAT           = true
    TeleportCommand.LUA            = true
    TeleportCommand.AUTH           = true
    TeleportCommand.CONSOLE        = true
    TeleportCommand.REQUIRE_LOGIN  = true
    TeleportCommand.REQUIRE_AUTH   = true
    TeleportCommand.DESCRIPTION    = { "Teleports one client to another" }
    TeleportCommand.ALIASES        = { "tp"}
    TeleportCommand.SYNTAX         = {
        { "<clientname>", "Teleports you to client"},
        { "<clientname> <clientname>", "Teleports client1 to client2"},
    }

function TeleportCommand:Run(Client,Args)
    local Target1 = ClientHandler:getClient(Args[1])
    local Target2 = ClientHandler:getClient(Args[2])
    if not Target1 and not Target2 then
        self:Error("Could not find any client's to teleport")
        return self:getHelp()
    end
    if Target1 and not Target2 then
        EntityHandler:TeleportToOrigin(Client.id, et.gentity_get(Target1.id,"ps.origin"))
        self:Info("Teleported to " .. Target1:getName())
        return self.SUCCESS
    end
    EntityHandler:TeleportToOrigin(Target1.id, et.gentity_get(Target2.id,"ps.origin"))
    self:Info("Teleported " .. Target1:getName() .. " to " .. Target2:getName())
    return self.SUCCESS
end

return TeleportCommand