local ColorsCommand = Command("Colors")

ColorsCommand.CORE           = true
ColorsCommand.ADMIN          = true
ColorsCommand.CHAT           = true
ColorsCommand.LUA            = true
ColorsCommand.AUTH           = true
ColorsCommand.CONSOLE        = true
ColorsCommand.DESCRIPTION    = { "Gets all the color codes" , 
                                 "You can view and change colors here as well" ,
                                 "colortypes: primary secondary tertiary error command console mail default admin lua auth" }
ColorsCommand.ALIASES        = { "color", "colorcodes" }
ColorsCommand.SYNTAX         = {
    { "" , "Displays all colors" },
    { "<colortype>" , "Displays color type (Auth)" },
    { "<colortype> <colorcode>" , "Changed color type to colorcode (Auth)" },
}
-- TODO color help here
function ColorsCommand:Run(Client,Args)
    if Client:isConsole() or Client:isAuth() then
        local colorcode = Misc:trim(Args[2])
        if Misc:triml(Args[1]) == "primary" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.Primary")
                Color.Primary = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.Primary .. "[PRIMARY]/[COLOR1]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "secondary" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.Secondary")
                Color.Secondary = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.Secondary .. "[SECONDARY][COLOR2]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "tertiary" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.Tertiary")
                Color.Tertiary = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.Tertiary .. "[TERTIARY]/[COLOR3]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "error" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.Error")
                Color.Error = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.Error .. "[ERROR]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "command" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.Command")
                Color.Command = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.Command .. "[COMMAND]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "console" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.console")
                Color.console = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.console .. "[CONSOLE]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "auth" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.auth")
                Color.auth = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.auth .. "[AUTH]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "lua" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.lua")
                Color.lua = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.lua .. "[LUA]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "admin" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.admin")
                Color.admin = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.admin .. "[ADMIN]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "default" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.default")
                Color.default = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.default .. "[DEFAULT]")
            return self.SUCCESS
        elseif Misc:triml(Args[1]) == "mail" then
            if colorcode ~= "" and string.find(colorcode,"^%^.$") then
                self:Info("Changed Color.mail")
                Color.mail = colorcode
                FileHandler:Save{ filePath = "LESM/save/" .. Config.JSON.Colors, fileData = Color }
                return self.SUCCESS
            end
            self:Info(Color.mail .. "[MAIL]")
            return self.SUCCESS
        end
    end
    self:Info("^11Q ^22R ^33S ^44T ^55U ^66V ^77W ^88X ^99Y ^00P ^ZZ ^JJ ^II ^HH ^GG ^MM ^OO ^NN ^FF ^DD ^BB ^CC ^EE ^KK ^LL ^AA ^?? ^++ ^@@ ^-- ^//")
    self:InfoClean(Color.Primary .. "[PRIMARY]/[COLOR1] " .. Color.Secondary .. "[SECONDARY][COLOR2] " .. Color.Tertiary .. "[TERTIARY]/[COLOR3]")
    return self.SUCCESS
end


return ColorsCommand