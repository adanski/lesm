local LoadCommand = Command("Load")

LoadCommand.CORE           = true
LoadCommand.ADMIN          = true
LoadCommand.CHAT           = true
LoadCommand.LUA            = true
LoadCommand.AUTH           = true
LoadCommand.REQUIRE_LOGIN  = true
LoadCommand.REQUIRE_AUTH   = true
LoadCommand.DESCRIPTION    = { "Teleports you to your saved location", "You can save names as well per map","See save command for more details" }
LoadCommand.SYNTAX         = {
    { "" , "Load current cached save location", },
    { "<savename>", "Loads <savename> save location to the current map", },
}


function LoadCommand:Run(Client,Args)
    if not Args[1] then
        if not next(Client.savePoint) then
            self:Error("No save in cache")
            return self:getHelp()
        end
        EntityHandler:TeleportToOrigin(Client.id,Client.savePoint)
        self:Info("Cached save loaded")
        return self.SUCCESS
    end
    Args[1] = string.lower(Misc:trim(Args[1]))
    local curmap = string.lower(Misc:trim(Game:Map()))
    for k=1,tlen(Client.User.savepoint) do
        local save = Client.User.savepoint[k]
        if ( save.map == curmap and Args[1] == save.name ) then
            EntityHandler:TeleportToOrigin(Client.id,save.origin)
            self:Info(Color.Secondary .. save.name .. Color.Primary .. " location loaded")
            return self.SUCCESS
        end
    end
    self:Error("Could not find save by that name")
    return self:getHelp()
end

return LoadCommand