local NoCapsCommand = Command("NoCaps")

NoCapsCommand.ADMIN          = true
NoCapsCommand.CHAT           = true
NoCapsCommand.LUA            = true
NoCapsCommand.AUTH           = true
NoCapsCommand.CONSOLE        = true
NoCapsCommand.REQUIRE_LEVEL  = Config.Level.Leader
NoCapsCommand.DESCRIPTION    = { "Toggle nocaps for client" , }
NoCapsCommand.ALIASES        = { "caps","capslock",}
NoCapsCommand.SYNTAX         = {
    { "<target>", "Enables no caps for target",},
}


function NoCapsCommand:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Could not find target") end
    if ( Target.id == Client.id ) and not Client:isAuth() then return self:Error("Cannot use this on yourself") end
    if ( Target:getLevel() > Client:getLevel() ) and not Client:isAuth() then return self:Error(Target:getName() .. Color.Primary .. " has higher level than you!") end
    if not Target.rudclient then return self:Error("Target does not have rudclient") end
    if Target.rudclient.nocaps then
        Target.rudclient.nocaps = false
        self:Info("Disabled nocaps for " .. Target:getName())
        return self.SUCCESS
    else
        Target.rudclient.nocaps = true
        self:Info("Enabled nocaps for " .. Target:getName())
        return self.SUCCESS
    end
end

return NoCapsCommand