local KarmaCommand = Command("Karma")

KarmaCommand.CORE           = true
KarmaCommand.ADMIN          = true
KarmaCommand.CHAT           = true
KarmaCommand.LUA            = true
KarmaCommand.AUTH           = true
KarmaCommand.REQUIRE_LOGIN  = true
KarmaCommand.DESCRIPTION    = {
                               "Gives takes and checks karma of players. ",
                               "You can give or take once an hour.",
                               "You can't give or take the same person twice in a row",
                              }
KarmaCommand.ALIASES        = { "kar"}
KarmaCommand.SYNTAX         = { 
    { "", "Shows your karma status" },
    { "<user>" , "Shows targetclient or targetusernames karma status"  },
    { "<user> +" , "Gives karma to targetclient or targetusername" },
    { "<user> -" , "Takes karma from targetclient or targetusername" },
}

-- karmagoodin
-- karmagoodout
-- karmabadin
-- karmabadout
-- karmalast
-- karmatime
local _GetKarmas = function(User)
    local karmagoodin  = User:getKey('karmagoodin')
    local karmagoodout = User:getKey('karmagoodout')
    local karmabadin   = User:getKey('karmabadin')
    local karmabadout  = User:getKey('karmabadout')
    return karmagoodin,karmagoodout,karmabadin,karmabadout
end
function KarmaCommand:Run(Client,Args)
    local TargetUser;
    if not Args[1] then
        TargetUser = Client.User
    else
        TargetUser = UserHandler:getUser(Args[1])
    end
    if not TargetUser then return self:Error("Could not find target user") end
    if TargetUser and not Args[2] then
        local c1 = Color.Primary
        local c2 = Color.Secondary
        local c3 = Color.Tertiary
        local karmagoodin,karmagoodout,karmabadin,karmabadout = _GetKarmas(TargetUser)
        self:InfoAll(TargetUser:getKey('username') .. c1 .. "'s karma" .. c3 .. ": " .. c1 .. "GOOD"..c3..": " .. c1 .. "IN" .. c3 .. "(" .. c2 ..karmagoodin .. c3 ..") " .. c1 .. "OUT" .. c3 ..
        "(" .. c2 ..karmagoodout .. c3 ..") " .. c2 .. "|"  .. c1 .. " BAD"..c3..": " .. c1 .. "IN" .. c3 .. "(" .. c2 ..karmabadin .. c3 ..") " .. c1 .. "OUT" .. c3 ..
        "(" .. c2 ..karmabadout .. c3 ..") ")
        return self.SUCCESS
    end
    local timeleft = os.time() - Client.User:getKey('karmatime')
    if timeleft < Misc:getTimeFormat(Config.Karma.Time) then return self:Error("You need to wait " .. Color.Secondary .. Misc:SecondsToClock(Config.Karma.Time - timeleft) .. Color.Primary .. " before using this on another person.") end
    if TargetUser.id == Client.User.id then return self:Error("Cannot karma yourself") end
    local karmalast = Client.User.karmalast
    local karmalast = -1
    local karmalast_c = 0
    for k=tlen(Client.User.karmalast),1, -1 do
        if karmalast == -1 then karmalast = Client.User.karmalast[k] end
        if karmalast == Client.User.karmalast[k] then
            karmalast_c = karmalast_c + 1
        end
        karmalast = Client.User.karmalast[k]
    end
    if karmalast_c > Config.Karma.Consecutive then return self:Error("Too many consecutive karmas on this user") end
    local cmd = Misc:triml(Args[2])
    if cmd == "give" or cmd == "g" or cmd == "+" or cmd == "add" then
        TargetUser:update('karmagoodin',TargetUser:getKey('karmagoodin')+1)
        Client.User:update('karmagoodout',Client.User:getKey('karmagoodout')+1)
        Client.User.karmalast[tlen(Client.User.karmalast)+1] = TargetUser:getKey('id')
        Client.User:update("karmatime",os.time())
        self:Info(Client:getName() .. Color.Primary .. " gave karma to " .. Color.Secondary .. TargetUser:getKey('username'))
        return self.SUCCESS
    end
    if cmd == "take" or cmd == "t" or cmd == "-" or cmd == "sub" or cmd == "subtract" then
        TargetUser:update('karmabadin',TargetUser:getKey('karmabadin')+1)
        Client.User:update('karmabadout',Client.User:getKey('karmabadout')+1)
        Client.User.karmalast[tlen(Client.User.karmalast)+1] = TargetUser:getKey('id')
        Client.User:update("karmatime",os.time())
        self:Info(Client:getName() .. Color.Primary .. " took karma from " .. Color.Secondary .. TargetUser:getKey('username'))
        return self.SUCCESS
    end
    self:Error("Invalid sub command")
    return self:getHelp()
end

return KarmaCommand