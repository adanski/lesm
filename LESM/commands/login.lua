local Login = Command("Login")

Login.ALIASES        = { "signin", }
Login.CORE           = true
Login.LUA            = true
Login.AUTH           = true
Login.DESCRIPTION    = { "Login to your user profile.", "Username and password can have A-z 0-9 _ - but it cannot have spaces",}
Login.SYNTAX         = {
    { "<username> <password>", "Attempts to login to <username> with <password>", },
}
Login.EXAMPLES       = {
    {"BobSteve s3cret", ""}
}

function Login:Run(Client,Args)
    if Client:isOnline() then return self:Error("You are already logged in!") end
    if not Args[1] then return self:Error("Missing username argument") end
    if not Args[2] then return self:Error("Missing password argument") end
    
    local User     = UserHandler:getWithUsername(Args[1], true)
    local password = Core:encrypt(Args[2])
    if not User then return self:Error("Could not find a user matching that username and password") end
    
    Console:Debug("Selected user " .. tostring(User.username), "command")
    Console:Debug("Comparing " .. tostring(User.password) .. " with " .. tostring(password), "command")
    if User.password == password then
        Client.User = User
        if not User:getKey("autologin") then
            self:Info(Color.Secondary .. "TIP: " .. Color.Primary .. "You can turn on autologin with the autologin command")
        end
        Client.User:updateFields(Client)
        Client:loadXp()
        Client:Play(Config.Sound.Login)
        self:Info("Logged into " .. Color.Secondary .. User.username .. Color.Primary .. "!")
        Mail:CheckTotal(Client)
        return self.SUCCESS
    else
        return self:Error("Could not find a user matching that username and password")
    end
end

return Login