local SeenCommand = Command("Seen")

SeenCommand.CORE           = true
SeenCommand.ADMIN          = true
SeenCommand.CHAT           = true
SeenCommand.LUA            = true
SeenCommand.CONSOLE        = true
SeenCommand.REQUIRE_LEVEL  = Config.Level.Admin
SeenCommand.DESCRIPTION    = { "" , }
SeenCommand.ALIASES        = { "lastseen" }
SeenCommand.SYNTAX         = {
    { "<name>" , "", },
}
local osdate,next = os.date,next
function SeenCommand:Run(Client,Args)
    if not Args[1] then return self:Error("Need a name to lookup") end
    local name = Misc:triml(Args[1])
    local Data = { }
    for x = 1 , tlen(RegularUserData) do 
        local rudclient = RegularUserData[x]
        local aliases = rudclient.aliases
        for k = 1 , tlen(aliases) do 
            local alias = Misc:triml(aliases[k])
            if string.find(alias,name,1,true) then
                local foundclient = { }
                foundclient[1] = rudclient.id
                foundclient[2] = rudclient.lastname
                foundclient[3] = aliases[k]
                local f = false
                for j=1, tlen(Clients) do
                    if Clients[k].rudclient.id == rudclient.id then
                        f = true
                    end
                end
                if f then
                    foundclient[4] = "^2Online"
                else
                    foundclient[4] = osdate("%c",rudclient.lastseen)
                end
                Data[tlen(Data)+1]  = foundclient
                break
            end
        end
    end
    if not next(Data) then return self:Error("Did not find any matches") end
    self:Info("Found " .. Color.Secondary .. tlen(Data) .. Color.Primary .. " matches")
    local PT = Misc:FormatTable(Data,{"ID","LASTNAME","ALIAS","LASTSEEN"},false,Client:getMaxChars(),Client:getMaxChars())
    for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
    return self.SUCCESS
end

return SeenCommand