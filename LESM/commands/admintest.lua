local AdminTest = Command("AdminTest")
-- This Command doesn't need to integrate as it does not do anything special

AdminTest.ADMIN          = true
AdminTest.CHAT           = true
AdminTest.CORE           = true
AdminTest.LUA            = true
AdminTest.NOSHRUBBOT     = true
AdminTest.REQUIRE_LOGIN  = true
AdminTest.DESCRIPTION    = { "Shows your level" , }

function AdminTest:Run(Client,Args)
    local Target = ClientHandler:getClient(Args[1])
    if not Target then Target = Client end
    self:InfoAll(Target:getName() .. Color.Primary .. " is a level " .. Color.Secondary .. Target:getLevel() .. Color.Primary .. " user")
    return self.SUCCESS
end

return AdminTest