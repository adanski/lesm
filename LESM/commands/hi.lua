local HiCommand = Command("Hi")

HiCommand.CORE           = true
HiCommand.ADMIN          = true
HiCommand.CHAT           = true
HiCommand.LUA            = true
HiCommand.AUTH           = true
HiCommand.CONSOLE        = true
HiCommand.GLOBALOUTPUT   = true
HiCommand.DESCRIPTION    = { "Says hello!!","Also plays a hello sound" }
HiCommand.ALIASES        = {"hello","hey","hola","hallo" }
HiCommand.SYNTAX         = {
    { "","Say hello!" },
    { "<client>", "Say hello to specified person" },
    { "@last", "Say hello to last person that connected" },
}
HiCommand.EXAMPLES       = {
    { "Bob", "Says hello to bob!"},
    { "@last", "Says hello to steve! ( Since he was the last person that connected",},
}

function HiCommand:Run(Client,Args)
    local message = Client:getName() .. Color.Primary .. " says hello"
    Sound:playAll(Config.Sound.Hello,Client)
    if Args[1] then
        local hi_message = Misc:trim(table.concat(Args, " "))
        if hi_message ~= "" then
            hi_message = MessageHandler:filter(hi_message,Client)
            self:InfoAll(message .. " to " .. Color.Primary .. hi_message .. Color.Tertiary .. "!")
            return self.SUCCESS
        end
    end
    self:InfoAll(message .. Color.Tertiary .. "!")
    return self.SUCCESS
end

return HiCommand