local EntityCommand = Command("Entity")

EntityCommand.CORE           = true
EntityCommand.ADMIN          = true
EntityCommand.CHAT           = true
EntityCommand.LUA            = true
EntityCommand.AUTH           = true
EntityCommand.REQUIRE_LOGIN  = true
EntityCommand.REQUIRE_AUTH   = true
EntityCommand.DESCRIPTION    = { "Manipulates entities ^1UNSAFE",
                        "Gets entity info for MapEnts" ,
                      }
EntityCommand.ALIASES        = { "ent", }
EntityCommand.SYNTAX         = {
    { "" , "Gets entity count"},
    { "get <ent>", "Gets all fieldnames from ent" },
    { "get <ent> <fname>", "Gets specific fieldname from ent" },
    { "set <ent> <fname> <value>", "Attempts to set fieldname to value" },
    { "find <fname> [text]", "Finds all ents with fname matching [text] with their value" },
    { "atself [distance]", "Finds all ents within distance of yourself" },
    { "atent <ent> [distance]", "Finds all ents near <ent>"},
    { "link <ent>", "Links entity to map"},
    { "unlink <ent>", "Unlinks entity from map"},
    { "free <ent>", "Fress entity from the map"},
    { "tp <ent> <ent>" , "Teleports two entitys"},
    { "compare <ent> <ent>", "Gets differences in fieldnames" },
    { "spawn", "test command" },
    { "copy", "test command" },
    { "health", "test command" },
    { "create", "test command" },
}

function EntityCommand:Run(Client,Args)
    if not Args[1] then
        return self:Info("There are " .. Color.Secondary .. EntityHandler:FN_GetEntCount() .. Color.Primary .. " active entities")
    elseif Args[1] == "get" or tonumber(Args[1]) then
        local ent   = tonumber(Args[1])
        local fname = Args[2]
        if not ent then
            ent   = tonumber(Args[2])
            fname = Args[3]
        end
        if not ent then return self:Error("Arg2 Must be an entity number") end
        if not fname or type(fname) == 'string' then
            local Data = EntityHandler:FN_GetData(ent) --readonly is not string is boolean dont think it will throw an error but note just in case
            if not next(Data) then return self:Error("No data found in ent") end
            self:Info("Entity info for " .. Color.Secondary .. ent)
            local PT = Misc:FormatTable(Data,{"NAME","VALUE","READONLY"},false,Client:getMaxChars(),Client:getMaxRows(), tonumber(fname) )
            for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
            return self.SUCCESS
        end
        if not EntityHandler:FN_Exists(Args[3]) then return self:Error("Fieldname does not exit") end
        local status,err = pcall(et.gentity_get,ent,Args[3])
        if status then
            return self:Info(Color.Tertiary .. "(" .. Color.Secondary .. tostring(ent) .. Color.Tertiary .. ")" .. Color.Primary .. " " .. Args[3] .. " equals " .. Color.Secondary .. tostring(err) )
        else
            return self:Error(err)
        end
    elseif Args[1] == "set" then
        local ent = tonumber(Args[2])
        if not ent then return self:Error("Arg2 must be an entity number") end
        local fieldname = Args[3]
        if not fieldname or not EntityHandler:FN_Exists(fieldname) then return self:Error("Unknown fieldname") end
        if not Args[4] then return self:Error("Arg4 must be the value you want to set") end
        
        if EntityHandler:FN_GetType(fieldname) == "FIELD_ARRAY" then
            if not Args[5] then return self:Error("Arg5 must be the value for this fieldname") end
            local index = Args[4]
            local value = Args[5]
            
            local status,err = pcall(et.gentity_set,ent,fieldname,index,value)
            if status then
                return self:Info(Color.Tertiary .. "(" .. Color.Secondary .. tostring(ent) .. Color.Tertiary .. ")" .. Color.Primary .. " " .. fieldname .. " " .. tostring(index) .. " equals " .. Color.Secondary .. tostring(value) )
            else
                return self:Error(err)
            end
        end
        local value = Args[4]
        local status,err = pcall(et.gentity_set,ent,fieldname,value)
        if status then
            return self:Info(Color.Tertiary .. "(" .. Color.Secondary .. tostring(ent) .. Color.Tertiary .. ")" .. Color.Primary .. " " .. fieldname .. " equals " .. Color.Secondary .. tostring(value) )
        else
            return self:Error(err)
        end
    elseif Args[1] == "find" then
        local fieldname = Args[2]
        if not Args[2] or not EntityHandler:FN_Exists(fieldname) then return self:Error("Fieldname does not exist") end
        
        local Data      = { }
        local FoundEnts = { }
        
        if not Args[3] then
            FoundEnts = EntityHandler:FN_FindAll(fieldname)
        else
            FoundEnts = EntityHandler:FN_Find(fieldname,Args[3])
        end
        
        for k = 1 , tlen(FoundEnts) do 
            local status,value = pcall(et.gentity_get,FoundEnts[k],fieldname)
            if status and value ~= nil then
                Data[tlen(Data)+1] = { FoundEnts[k],value }
            end
        end
        local PT   = Misc:FormatTable(Data,{"ENTNUM","ENTVALUE"},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[4]) or tonumber(Args[3]))
        self:Info("Found " .. Color.Secondary ..  tlen(Data) .. Color.Primary .. " matches")
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    elseif Args[1] == "atself" then
        local ent = tonumber(Client.id)
        if not ent then return self:Error("Could not convert your id to ent") end
        local distance  = tonumber(Args[2])
        local FoundEnts = EntityHandler:getEntitiesNear(ent,distance)
        local Data      = { }
        for k = 1 , tlen(FoundEnts) do 
            local status,value = pcall(et.gentity_get,FoundEnts[k],"classname")
            if ( status and value ~= nil ) then
                Data[tlen(Data)+1] = { FoundEnts[k] , value , tostring( EntityHandler:getDistance(ent,FoundEnts[k]) ) }
            end 
        end
        
        self:Info("Found " .. Color.Secondary ..  tlen(Data) .. Color.Primary .. " matches")
        local PT = Misc:FormatTable(Data,{"ENTNUM", "ENTCLASSNAME", "ENTDISTANCE"},false,Client:getMaxColumns(),Client:getMaxRows(),tonumber(Args[3]))
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        
        return self.SUCCESS
    elseif Args[1] == "atent" then
        local ent = tonumber(Args[2])
        if not ent then return self:Error("Arg2 must be an integer") end
        local distance  = tonumber(Args[3])
        local FoundEnts = EntityHandler:getEntitiesNear(ent,distance)
        local Data      = { }
        
        for k=1,tlen(FoundEnts) do 
            local status,value = pcall(et.gentity_get,FoundEnts[k],"classname")
            if ( status and value ~= nil ) then
                Data[tlen(Data)+1] = { FoundEnts[k] , value , tostring( EntityHandler:getDistance(ent,FoundEnts[k]) ) }
            end 
        end
        self:Info("Found " .. Color.Secondary ..  tlen(Data) .. Color.Primary .. " matches")
        local PT = Misc:FormatTable(Data,{"ENTNUM","ENTCLASSNAME","ENTDISTANCE"},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[4]))
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    elseif Args[1] == "link" then
        local ent = tonumber(Args[2])
        if not ent then return self:Error("Arg2 must be an integer") end
        et.trap_LinkEntity(ent)
        self:Info("Linked ent " .. Color.Secondary .. tostring(ent))
        return self.SUCCESS
    elseif Args[1] == "unlink" then
        local ent = tonumber(Args[2])
        if not ent then return self:Error("Arg2 must be an integer") end
        et.trap_UnlinkEntity(ent)
        self:Info("Unlinked ent " .. Color.Secondary .. tostring(ent))
        return self.SUCCESS
    elseif Args[1] == "free" then
        local ent = tonumber(Args[2])
        if not ent then return self:Error("Arg2 must be an integer") end
        et.G_FreeEntity(ent)
        self:Info("Freed ent " .. Color.Secondary .. tostring(ent))
        return self.SUCCESS
    elseif Args[1] == "tp" then
        local ent1 = tonumber(Args[2])
        local ent2 = tonumber(Args[3])
        if not ent1 or not ent2 then return self:Error("Need two numbers") end
        EntityHandler:Teleport(ent1,ent2)
        return self.SUCCESS
    elseif Args[1] == "spawn" then
        EntityHandler:Spawn(Client,Args)
        return self.SUCCESS
    elseif Args[1] == "copy" then
        EntityHandler:Copy(Client,tonumber(Args[2]),Args[3])
        return self.SUCCESS
    elseif Args[1] == "create" then
        local ent = et.G_Spawn()
        self:Info(tostring(ent))
        return self.SUCCESS
    elseif Args[1] == "health" then
        EntityHandler:Item_Health(Client)
        return self.SUCCESS
    elseif Args[1] == "compare" then
        local ent1 = tonumber(Args[2])
        local ent2 = tonumber(Args[3])
        if not ent1 or not ent2 then return self:Error("Need two entities") end
        local Data = EntityHandler:FN_GetDifferences(ent1,ent2)
        if not Data or not next(Data) then return self:Error("No differences") end
        
        self:Info("Found " .. Color.Secondary .. tlen(Data) .. Color.Primary .. " differences")
        local PT = Misc:FormatTable(Data,{"NAME","VALUE","READONLY"},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[4]))
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    elseif Args[1] == "origin" then
        local ent    = tonumber(Args[2])
        local origin = { }
        if not ent then
            origin = Client:getOrigin()
            ent = Client.id
        else
            origin = EntityHandler:safeEntGet(ent,"s.origin")
        end
        if origin and next(origin) then
            return self:Info(Color.Secondary .. ent .. Color.Primary .. " origin " .. Color.Secondary .. table.concat(origin, " , "))
        else
            return self:Error("could not get origin")
        end
    else
        return self:getHelp()
    end
end

return EntityCommand