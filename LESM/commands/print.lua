local PrintCommand = Command("Print")

PrintCommand.ADMIN          = true
PrintCommand.CHAT           = true
PrintCommand.LUA            = true
PrintCommand.AUTH           = true
PrintCommand.CONSOLE        = true
PrintCommand.REQUIRE_LOGIN  = true
PrintCommand.REQUIRE_AUTH   = true
PrintCommand.DESCRIPTION    = { "Print messages via the console", }
PrintCommand.SYNTAX         = {
    { "print (message)", "Prints to console (With Message Format)", },
    { "chat (message)", "Prints to chat (With Message Format)", },
    { "echo (message)", "Prints to obituary (With Message Format)", },
    { "banner (message)", "Prints to banner (With Message Format)", },
    { "centerprint (message)", "Prints to center (With Message Format)", },
    { "printclean (message)", "Prints to console (Without Message Format)", },
    { "chatclean (message)", "Prints to chat (Without Message Format)", },
    { "echoclean (message)", "Prints to obituary (Without Message Format)", },
    { "bannerclean (message)", "Prints to banner (Without Message Format)", },
    { "centerprintclean (message)", "Prints to center (Without Message Format)", },
}

function PrintCommand:Run(Client,Args)
    if not Args[1] then return self:Error("Need a plact to print to") end
    local message = Misc:trim(table.concat(Args," ",2))
    if message == "" then return self:Error("Missing print message") end
    
    local where = string.lower(Args[1])
    if where == "print" then
        Console:Print(message)
    elseif where == "printclean" then
        Console:PrintClean(message)
    elseif where == "chat" then
        Console:Chat(message)
    elseif where == "chatclean" then
        Console:ChatClean(message)
    elseif where == "echo" then
        Console:Echo(message)
    elseif where == "echoclean" then
        Console:EchoClean(message)
    elseif where == "banner" then
        Console:Banner(message)
    elseif where == "bannerclean" then
        Console:BannerClean(message)
    elseif where == "centerprint" then
        Console:CenterPrint(message)
    elseif where == "centerprintclean" then
        Console:CenterPrintClean(message)
    else
        Console:Print( Misc:trim( table.concat(Args, " ") ) )
    end
    return self.SUCCESS
end

return PrintCommand