local IgnoreCommand = Command("Ignore")

IgnoreCommand.CORE           = true
IgnoreCommand.ADMIN          = true
IgnoreCommand.CHAT           = true
IgnoreCommand.LUA            = true
IgnoreCommand.AUTH           = true
IgnoreCommand.DESCRIPTION    = { "Silently ignores target" , "You can decide to ignore only sounds from a client as well" }
IgnoreCommand.ALIASES        = { "silentignore", "signore","unignore", }
IgnoreCommand.SYNTAX         = {
    { "","List ignored clients",},
    { "<client>","Silently ignores target",},
    { "sound <client>","Silently ignores target's sounds",},
    { "clear","Clears all ignored players",},
}

function IgnoreCommand:Run(Client,Args)
    if not Args[1] then
        self:Info("Listing ignored clients")
        if not Client.rudclient.sounds then self:Info("You have sounds disabled do /ignore sounds to renable them") end
        for k = 1 , tlen(Clients) do 
            if Client:isIgnored(Clients[k]) then
                self:InfoClean("Ignored:       " .. Clients[k]:getName())
            elseif Client:isSoundIgnored(Clients[k]) then
                self:InfoClean("Sound Ignored: " .. Clients[k]:getName())
            end
        end
        return self.SUCCESS
    end

    if Misc:triml(Args[1]) == "clear" then
        Client.rudclient.soundignored = { }
        Client.rudclient.ignored = { }
        Client:entSet("sess.ignoreClients",0)
        self:Info("Cleared all ignored clients")
        return self.SUCCESS
    end
    if Misc:triml(Args[1]) == "sounds" or Misc:triml(Args[1]) == "sound" then
        if not Args[2] then
            if Client.rudclient.sounds then
                Client.rudclient.sounds = false
                self:Info("Ignoring all lua sounds")
            else
                Client.rudclient.sounds = true
                self:Info("No longer ignoring all lua sounds")
            end
            return self.SUCCESS
        end
        local Target = ClientHandler:getClient(Args[2])
        if not Target then return self:Error("Could not find client") end
        --if Target.id == Client.id then return self:Error("Can't ignore your self") end -- Maybe allow this bit?
        if Client:isSoundIgnored(Target) then
            Client:soundUnignore(Target)
            self:Info("No longer sound ignoring " .. Target:getName())
        else
            Client:soundIgnore(Target)
            self:Info("Now sound ignoring " .. Target:getName())
        end
        return self.SUCCESS
    end

    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Could not find client") end

    if Misc:triml(Args[2]) == "sound" or Misc:triml(Args[2]) == "sounds" then
        if Client:isSoundIgnored(Target) then
            Client:soundUnignore(Target)
            self:Info("No longer sound ignoring " .. Target:getName())
        else
            Client:soundIgnore(Target)
            self:Info("Now sound ignoring " .. Target:getName())
        end
        return self.SUCCESS
    end

    if Target.id == Client.id then return self:Error("Can't ignore your self") end -- After sound bit

    if Client:isIgnored(Target) then
        Client:unignore(Target)
        self:Info("No longer ignoring " .. Target:getName())
    else
        Client:ignore(Target)
        self:Info("Now ignoring " .. Target:getName())
    end

    return self.SUCCESS
end

return IgnoreCommand