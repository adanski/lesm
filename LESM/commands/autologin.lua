local AutoLogin = Command("AutoLogin")

AutoLogin.ALIASES        = { "al", "alogin", }
AutoLogin.CORE           = true
AutoLogin.LUA            = true
AutoLogin.REQUIRE_LOGIN  = true
AutoLogin.DESCRIPTION    = { 
                            "Controls auto logging into your profile.",
                            "Autologin requires a silent,guid, or ip to be linked to your profile to work.",
                            "Timeoutlength only activates when you have autologin disabled,",
                            "this is the time you would like to be remembered before it no longer auto logs you in."
                           }
AutoLogin.SYNTAX         = { 
    { ""               , "Toggles autologin on and off" },
    {"silent"          , "Links ( or resets if already present ) your autologin silent id" },
    {"etpro"           , "Links ( or resets if already present ) your autologin etpro guid" },
    {"guid"            , "Links ( or resets if already present ) your autologin guid" },
    {"ip"              , "Links ( or resets if already present ) your autologin ip" },
    {"<timeoutlength>" , "Timed autologin" },
                           }
AutoLogin.EXAMPLES       = {
    {"3d", "Timed autologin (3 Days) use m for minutes and h for hours" },
}


function AutoLogin:Run(Client,Args)
    local ip                    = Client:getIp()
    local eGuid,sGuid,etproGuid = Client:getGuids()
    if not Args[1] then
        if Client.User:getKey("autologin") then
            Client.User:update("autologin",false)
            self:Info("Disabled autologin!")
        else
            Client.User:update("autologin",true)
            self:Info("Enabled autologin")
        end
        return self.SUCCESS
    elseif Args[1] == "ip" then
        if ip == '' then return self:Error("Could not retrieve your ip, sorry!") end
        
        if Client.User:getKey("autologinip") == ip then
            Client.User:update("autologinip","")
            self:Info("User autologin for ip is now disabled")
        else
            Client.User:update("autologinip",ip)
            self:Info("User autologin for ip was set to " .. Color.Secondary .. ip)
        end
        return self.SUCCESS
    elseif ( Args[1] == "guid" or Args[1] == "etkey" or Args[1] == "eguid" ) then
        if eGuid == "" then return self:Error("Could not retrieve your etkey guid, sorry!") end
        
        if Client.User:getKey("autologinguid") == eGuid then
            Client.User:update("autologinguid","")
            self:Info("User autologin for guid is now disabled")
        else
            Client.User:update("autologinguid",eGuid)
            self:Info("User autologin for guid now set to " .. Color.Secondary .. eGuid)
        end
        return self.SUCCESS
    elseif ( Args[1] == "silent" or Args[1] == "silentid" or Args[1] == "silentdat" or Args[1] == "silentguid" ) then
        if sGuid == "" then return self:Error("Could not retrieve your silent guid, sorry!") end
        
        if Client.User:getKey("autologinsid") == sGuid then
            Client.User:update("autologinsid","")
            self:Info("User autologin for silent guid is now disabled")
        else
            Client.User:update("autologinsid",sGuid)
            self:Info("User autologin for silent guid now set to " .. Color.Secondary .. sGuid)
        end
        return self.SUCCESS
    elseif ( Args[1] == "etpro" or Args[1] == "etproid" or Args[1] == "etproguid" ) then
        if etproGuid == "" then return self:Error("Could not retrieve your etpro guid, sorry!") end
        
        if Client.User:getKey("autologinetpro") == etproGuid then
            Client.User:update("autologinetpro","")
            self:Info("User autologin for silent guid is now disabled")
        else
            Client.User:update("autologinetpro",etproGuid)
            self:Info("User autologin for etpro guid now set to " .. Color.Secondary .. etproGuid)
        end
        return self.SUCCESS
    else
        local time = Misc:getTimeFormat(Args[2])
        Client.User:update("timeout",os.time() + time)
        self:Info("Timeout length set to " .. Color.Secondary .. Misc:SecondsToClock(time))
        return self.SUCCESS
    end
end


return AutoLogin