local EditCommand = Command("Edit")

EditCommand.CORE           = true
EditCommand.LUA            = true
EditCommand.AUTH           = true
EditCommand.REQUIRE_LOGIN  = true
EditCommand.DESCRIPTION    = { 
                        "Edits keys in your user profile. ",
                        "Number keys have min and max they can be",
                        "String keys have min length and max length they can be",
                        "Some string keys do not allow spaces and other characters",
                      }
EditCommand.SYNTAX         = {
    { "list <page>", "Gets info of all commands" },
    { "<key> -info", "Gets info of command" },
    { "<key>", "Toggles key on and off (Boolean only)" },
    { "<key> <value>", "Edits key with value" },
}
EditCommand.EXAMPLES       = { 
    { "singlepistol", "Toggles spawning with single pistol" },
    { "username EpicMan411", "Edits your username to EpicMan411" },
    { "forumsname The Epic Man", "Edits your forums name to The Epic Man" },
}

function EditCommand:Run(Client,Args)
    if not Args[1] or Misc:triml(Args[1]) == 'list' or Misc:triml(Args[1]) == 'page' or tonumber(Args[1]) then
        local keystable = { }
        for k = 1 , tlen(Keys) do 
            local skip = false
            if not Client:isAuth() and ( Keys[k].auth or not Keys[k].editable ) then skip = true end
            if not skip then
                if Client:isAuth() then
                    keystable[tlen(keystable)+1] = {
                    Keys[k].name,
                    Keys[k].type,
                    Client.User[Keys[k].name],
                    tostring(Keys[k].editable),
                    tostring(Keys[k].auth),
                    Misc:trim(Keys[k].desc),
                    }
                else
                    keystable[tlen(keystable)+1] = {
                    Keys[k].name,
                    Keys[k].type,
                    Client.User[Keys[k].name],
                    tostring(Keys[k].editable),
                    Misc:trim(Keys[k].desc),
                    }
                end
            end
        end
        self:Info("Total of " .. Color.Tertiary .. tlen(keystable) .. Color.Secondary .. " keys".. Color.Primary .. " use " .. Color.Secondary .. "/edit <keyname> -info " .. Color.Primary .. "For full info")
        local PT;
        if Client:isAuth() then
            PT = Misc:FormatTable( keystable , {"NAME","TYPE","VALUE","EDITABLE","AUTH","DESCRIPTION"} , false , Client:getMaxChars() , Client:getMaxRows() , tonumber(Args[1]) or tonumber(Args[2]))
        else
            PT = Misc:FormatTable( keystable , {"NAME","TYPE","VALUE","EDITABLE","DESCRIPTION"} , false , Client:getMaxChars() , Client:getMaxRows() , tonumber(Args[1]) or tonumber(Args[2]))
        end
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        return self.SUCCESS
    end
    Args[1] = string.lower(Args[1])
    local Key;
    for k = 1 , tlen(Keys) do 
        if ( Keys[k].name == Args[1]) then
            Key = Keys[k]
            break
        end
    end
    if Key then
        if Args[2] and Misc:triml(Args[2]) == "-info" then
            self:Info("Info for key " .. Key.name)
            self:InfoClean("Type     " .. Color.Tertiary .. ": " .. Color.Secondary .. Key.type)
            self:InfoClean("Editable " .. Color.Tertiary .. ": " .. Color.Secondary .. tostring(Key.editable))
            self:InfoClean("AuthOnly " .. Color.Tertiary .. ": " .. Color.Secondary .. tostring(Key.auth))
            self:InfoClean("Value    " .. Color.Tertiary .. ": " .. Color.Secondary .. tostring(Client.User:getKey(Key.name)))
            if Key.type == "number" or Key.type == "string" then
                self:InfoClean("Min/Max  " .. Color.Tertiary .. ": " .. Color.Secondary .. Key.min .. Color.Tertiary .. "-".. Color.Secondary ..Key.max)
            end
            Client:PrintClean(Misc:trim(Key.desc))
            return self.SUCCESS
        end
        if not Key.editable then return self:Error("This key is not editable") end
        if not Client:isAuth() and Key.auth then return self:Error("This key is for auth only") end
        if Key.type == "boolean" then
            local value = Client.User:getKey(Key.name)
            if value then
                value = false
            else
                value = true
            end
            Client.User:update(Key.name,value)
            self:Info("Updated your " .. Color.Secondary .. Key.title .. Color.Primary .. " to " .. Color.Secondary .. tostring(value))
            return self.SUCCESS
        elseif Key.type == "number" then
            local value = tonumber(Args[2])
            if not value then return self:Error("Value need to be a number") end
            if value < Key.min then
                return self:Error("Value is lower than minimum allowed value ( " .. Key.min .. " )")
            elseif ( Key.max > 0 and value > Key.max ) then
                return self:Error("Value is higher than maxmium allowed value ( " .. Key.max .. " )")
            end
            Client.User:update(Key.name,value)
            self:Info("Updated your " .. Color.Secondary .. Key.title .. Color.Primary .. " to " .. Color.Secondary .. tostring(value))
            return self.SUCCESS
        elseif Key.type == "string" then
            local value = Misc:trim(table.concat(Args, " " , 2 ))
            if string.len(value) < Key.min then
                return self:Error(Key.title .. "'s length is lower than minimum allowed value ( " .. Key.min .. " )")
            elseif ( Key.max > 0 and string.len(value) > Key.max ) then
                return self:Error(Key.title .. "'s is higher than maxmium allowed value ( " .. Key.max .. " )")
            end
            if Key.pattern ~= "" then
                local f = string.find(value,Key.pattern)
                if f then return self:Error("Disallowed character: "  .. string.sub(value,f,f)) end
            end
            if Key.name == 'password' then
                value = Core:encrypt(value)
                Client.User:update(Key.name,value)
                self:Info("Updated your " .. Color.Secondary .. Key.title)
            else
                Client.User:update(Key.name,value)
                self:Info("Updated your " .. Color.Secondary .. Key.title .. Color.Primary .. " to " .. Color.Secondary .. tostring(value))
            end
        end
    else
        return self:Error("Unknown key")
    end
end

return EditCommand