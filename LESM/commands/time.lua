local TimeDateCommand = Command("TimeDate")

TimeDateCommand.CORE           = true
TimeDateCommand.ADMIN          = true
TimeDateCommand.CHAT           = true
TimeDateCommand.LUA            = true
TimeDateCommand.CONSOLE        = true
TimeDateCommand.GLOBALOUTPUT   = true
TimeDateCommand.DESCRIPTION    = { "Displays time and date" , }
TimeDateCommand.ALIASES        = { "date","time","datetime",}
TimeDateCommand.SYNTAX         = {
    { "" , "Displays time and date", },
}

function TimeDateCommand:Run(Client,Args)
    local day_num = tonumber(os.date("%d"))
    local daysuffix = "th"
    if ( day_num == 1 or day_num == 21 or day_num == 31 ) then
        daysuffix = "st"
    elseif ( day_num == 2 or day_num == 22 ) then
        daysuffix = "nd"
    elseif ( day_num == 3 or day_num == 23 ) then
        daysuffix = "rd"
    end
    local date = os.date(Color.Secondary .. "%H" .. Color.Tertiary .. ":" .. Color.Secondary .. "%M" .. 
    Color.Tertiary .. "%p" .. Color.Primary .." on " .. Color.Secondary .. "%A" .. Color.Primary ..
    " the " .. Color.Secondary .. "%d" .. Color.Tertiary .. daysuffix .. Color.Primary .. " of " .. 
    Color.Secondary .. "%B")
    self:InfoAll(date)
    return self.SUCCESS
end

return TimeDateCommand