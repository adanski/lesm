local Afk = Command("Afk")

Afk.ALIASES        = { "away", }
Afk.CORE           = true
Afk.ADMIN          = true
Afk.CHAT           = true
Afk.LUA            = true
Afk.GLOBALOUTPUT   = true
Afk.DESCRIPTION    = { 
                        "Notify server that you are away." ,
                        "If you are logged in it will tell people how long you are away",
                      }
Afk.SYNTAX         = {
    { "" , "Goes afk without reason" },
    { "[reason...]", "Goes afk with reason [reason]" },
}
Afk.EXAMPLES       = {
    { "", "YourName has gone afk!", },
    { "eating breakfast", "YourName is away eating breakfast", },
}


function Afk:Run(Client,Args)
    if Client.isAfk then return self:Error("You are already away") end
    Sound:playAll(Config.Sound.Afk,Client)
    Client:setTeam("spectator")
    Client.isAfk = true
    local reason = Misc:trim(table.concat(Args," "))
    if ( reason and reason ~= "" ) then
        reason = MessageHandler:filter(reason,Client)
        Client.afkReason = reason
        Console:IteratePrint(Client:getName() .. Color.Primary .. " is away " .. Color.Secondary .. Client.afkReason,"chat","afklocation" , Client)
    else
        Client.afkReason = ""
        Console:IteratePrint(Client:getName() .. Color.Primary .. " has gone away" , "chat" , "afklocation" , Client)
    end
    if Client:isOnline() then
        Client.User:update("afk"      , true )
        Client.User:update("afkreason", Client.afkReason )
        Client.User:update("afktime"  , os.time() )
    end
    return self.SUCCESS
end


return Afk