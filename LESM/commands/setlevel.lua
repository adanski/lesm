local SetLevelCommand = Command("SetLevel")

local _DisableAutoLevel = function(Target)
    if Target and Target.rudclient then Target.rudclient.autolevel = false end
end

SetLevelCommand.ADMIN          = true
SetLevelCommand.CHAT           = true
SetLevelCommand.CONSOLE        = true
if not Game:Shrubbot() then
    SetLevelCommand.CORE           = true
    SetLevelCommand.LUA            = true
    SetLevelCommand.AUTH           = true
    SetLevelCommand.REQUIRE_LEVEL  = Config.Level.Admin
    SetLevelCommand.REQUIRE_LOGIN  = true
    SetLevelCommand.OVERWRITE      = false
else
    SetLevelCommand.OVERWRITE      = true
    SetLevelCommand.CORE           = false
    SetLevelCommand.LUA            = false
    SetLevelCommand.AUTH           = false
    SetLevelCommand.REQUIRE_LEVEL  = 0
    SetLevelCommand.REQUIRE_LOGIN  = false
    SetLevelCommand.REQUIRE_FLAG   = 's'
end
SetLevelCommand.DESCRIPTION    = { "Setlevel a client to specified level" , }
SetLevelCommand.SYNTAX         = {
    { "<client> <level>", "Sets clients level to <level>"}
}

--TODO errors for non shrubbot
function SetLevelCommand:Run(Client,Args)
    local TargetClient = ClientHandler:getClient(Args[1])
    local TargetLevel  = tonumber(Args[2])
    if not TargetClient then return self.ERROR end
    if not TargetLevel  then return self.ERROR end
    if TargetLevel == TargetClient:getLevel() then return self.ERROR end
    if Game:Shrubbot() then
        if et.G_shrubbot_permission( Client.id , "s") == 0 then return self.ERROR end -- Does not have setlevel command
    else
        if Client:isConsole() then
            if TargetClient:setLevel(TargetLevel) then
                self:InfoAll(TargetClient:getName() .. Color.Primary .. " set to level " .. Color.Secondary.. TargetLevel)
            else
                return self:Error("Setlevel failed")
            end
            return self.SUCCESS
        end
    end
    local ClientLevel = Client:getLevel()
    if Client:isAuth() or ClientLevel >= Config.Level.Leader then
        if TargetClient:getLevel() ~= TargetLevel then
            _DisableAutoLevel(TargetClient)
        end
        if Game:Shrubbot() then
            if TargetClient:isOnline() then TargetClient.User:update('level',TargetLevel) end
            return self.ERROR -- Allow command to pass
        else
            if TargetClient:setLevel(TargetLevel) then
                self:InfoAll(TargetClient:getName() .. Color.Primary .. " set to level " .. Color.Secondary.. TargetLevel)
            else
                return self:Error("Setlevel failed")
            end
            return self.SUCCESS
        end
    end
    
    if TargetClient:getLevel() >= ClientLevel then
        self:Error("Cannot setlevel clients at or above your level")
        return self.SUCCESS
    end
    if TargetLevel > ( ClientLevel - Config.SetLevelMax ) then
        self:Error("You are allowed to setlevel up to level" .. (ClientLevel - Config.SetLevelMax ))
        return self.SUCCESS
    end
    -- Promotion
    if TargetLevel > TargetClient:getLevel() then
        if ( Config.AutoLevel.Enabled and Config.AutoLevel.NoSetLevel and ( TargetClient:getLevel() < Config.AutoLevel.StartLevel and TargetLevel <= Config.AutoLevel.EndLevel ) ) then
            self:Error("Please allow the xp autolevel to level the player up")
            return self.SUCCESS
        end
        _DisableAutoLevel(TargetClient)
        
        if Game:Shrubbot() then
            if TargetClient:isOnline() then TargetClient.User:update('level',TargetLevel) end
            return self.ERROR -- Allow command to pass
        else
            if TargetClient:setLevel(TargetLevel) then
                self:InfoAll(TargetClient:getName() .. Color.Primary .. " set to level " .. Color.Secondary.. TargetLevel)
            else
                return self:Error("Setlevel failed")
            end
            return self.SUCCESS
        end
    elseif TargetLevel < TargetClient:getLevel() then -- Demotion
        _DisableAutoLevel(TargetClient)
        
        if Game:Shrubbot() then
            if TargetClient:isOnline() then TargetClient.User:update('level',TargetLevel) end
            return self.ERROR -- Allow command to pass
        else
            if TargetClient:setLevel(TargetLevel) then
                self:InfoAll(TargetClient:getName() .. Color.Primary .. " set to level " .. Color.Secondary.. TargetLevel)
            else
                return self:Error("Setlevel failed")
            end
            return self.SUCCESS
        end
    end
end

return SetLevelCommand