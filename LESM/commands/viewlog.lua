local ViewLogCommand = Command("ViewLog")

ViewLogCommand.AUTH           = true
ViewLogCommand.CONSOLE        = true
ViewLogCommand.REQUIRE_LOGIN  = true
ViewLogCommand.REQUIRE_AUTH   = true
ViewLogCommand.DESCRIPTION    = { "Views log for this match" , }
ViewLogCommand.ALIASES        = {"log", "viewlogs","vl",}
ViewLogCommand.SYNTAX         = {
    { "","Shows entire log", },
    { "<page>", "Shows page of log", },
    { "bots", "Shows only events from bots", },
    { "nobots", "Shows only events from players", },
    { "find <text>", "Shows only events that contain <text>", },
}
local _RemoveWith = function(logTable , pat)
    local newLogTable = { }
    for k = 1 , tlen(logTable) do 
        local msg = string.lower(Misc:trim(et.Q_CleanStr(logTable[k])))
        if not string.find(msg,pat) then
            newLogTable[tlen(newLogTable)+1] = logTable[k]
        end
    end
    return newLogTable
end
local _KeepWith = function(logTable , pat)
    local newLogTable = { }
    for k = 1 , tlen(logTable) do 
        local msg = string.lower(Misc:trim(et.Q_CleanStr(logTable[k])))
        if string.find(msg,pat) then
            newLogTable[tlen(newLogTable)+1] = logTable[k]
        end
    end
    return newLogTable
end

function ViewLogCommand:Run(Client,Args)
    local fulllog = Logger:FullMessage()
    
    local ItemsPerPage = 70
    local page = tonumber(Args[1]) or 1
    if Args[1] then
        if ( string.lower(Args[1]) == "nobots" ) then
            page = tonumber(Args[2]) or 1
            fulllog = _RemoveWith(fulllog,"%[bot%]")
        elseif ( string.lower(Args[1]) == "bots" ) then
            page = tonumber(Args[2]) or 1
            fulllog = _KeepWith(fulllog,"%[bot%]")
        elseif ( string.lower(Args[1]) == "find" ) and Args[2] then
            page = tonumber(Args[3]) or 1
            fulllog = _KeepWith(fulllog,string.lower(Args[2]))
        end
    end
    
    local s_point = 1
    local e_point = ItemsPerPage
    if page > 1 then
        s_point = ( page - 1 ) * ItemsPerPage
        e_point = page * ItemsPerPage
    end
    if e_point > tlen(fulllog) then e_point = tlen(fulllog) end
    for k=s_point,e_point do
        self:InfoClean(fulllog[k])
    end
    self:Info("Displaying page " .. Color.Secondary .. page)
    return self.SUCCESS
end


return ViewLogCommand