local WarnCommand = Command("Warn")

WarnCommand.ADMIN          = true
WarnCommand.CHAT           = true
WarnCommand.CONSOLE        = true
if not Game:Shrubbot() then
    WarnCommand.CORE           = true
    WarnCommand.LUA            = true
    WarnCommand.AUTH           = true
    WarnCommand.REQUIRE_LEVEL  = Config.Level.Admin
    WarnCommand.REQUIRE_LOGIN  = true
    WarnCommand.OVERWRITE      = false
else
    WarnCommand.OVERWRITE      = true
    WarnCommand.CORE           = false
    WarnCommand.LUA            = false
    WarnCommand.AUTH           = false
    WarnCommand.REQUIRE_LEVEL  = 0
    WarnCommand.REQUIRE_LOGIN  = false
    WarnCommand.REQUIRE_FLAG   = 'R'
end
if Config.Shrubbot.Warn then
    WarnCommand.NOSHRUBBOT     = true
end
WarnCommand.DESCRIPTION    = { "Manages handing out warnings and removing warnings to clients",
                               "Can also handle offline warnings as well by using the offline sub command" }
WarnCommand.SYNTAX         = {
    { "" , "Views all warnings currently issued"},
    { "<client> <message>" , "Warns a client with reason"},
    { "<client> remove <warnid>" , "Views all warnings currently issued"},
    { "<client> clear" , "Clears all issued warnings for client"},
    { "offline <rudclientid> <message>" , "Warns a client with reason"},
    { "offline <rudclientid> remove <warnid>" , "Views all warnings currently issued"},
    { "offline <rudclientid> clear" , "Clears all issued warnings for client"},
}

WarnCommand.ALIASES = { "warnings" }

if Config.Warn.AllowNoReason then
    WarnCommand.SYNTAX[tlen(WarnCommand.SYNTAX)+1] = { "<client>" , "Warns a client without"}
    WarnCommand.SYNTAX[tlen(WarnCommand.SYNTAX)+1] = { "offline <rudclientid>" , "Warns a client without"}
end
--   1    2        3      4         5        6
-- {"ID","WARNID","NAME","EXPIRES","REASON","WARNER"}
local _getPrintTableOfRUDWarnings = function()
    local warnExpireTime = Misc:getTimeFormat(Config.Warn.Expires)
    if warnExpireTime < 0 then warnExpireTime = 0 end
    local Warnings = { }
    for k = 1 , tlen(RegularUserData) do 
        local rudclient = RegularUserData[k]
        for i=1,tlen(rudclient.warnings) do
            local w = rudclient.warnings[i]
            local WARNING = { }
            WARNING[1] = rudclient.id
            WARNING[2] = w.id
            WARNING[3] = rudclient.lastname
            local expires = os.time() - w.time
            if warnExpireTime == 0 then
                WARNING[4] = "Permanent"
            elseif expires > warnExpireTime then
                WARNING[4] = "Expired"
            else
                WARNING[4] = Misc:SecondsToClock( warnExpireTime - expires)
            end
            WARNING[5] = w.reason
            WARNING[6] = w.warner
            Warnings[tlen(Warnings)+1] = WARNING
        end
    end
    return Warnings
end

function WarnCommand:Run(Client,Args)
    if Config.Shrubbot.Warn then return self.PASS end -- Let shrubbot handle command
    
    -- List all warnings
    if not Args[1] or ( Misc:triml(Args[1]) == "list" or Misc:triml(Args[1]) == "page" ) then
        local Warnings = _getPrintTableOfRUDWarnings()
        if not next(Warnings) then return self:Error("No warnings present") end
        local PT = Misc:FormatTable(Warnings,{"ID","WARNID","NAME","EXPIRES","REASON","WARNER"},false,Client:getMaxChars(),Client:getMaxRows(),tonumber(Args[2]))
        self:Info(Color.Secondary .. tlen(Warnings) .. Color.Primary .. " Warnings")
        for k = 1 , tlen(PT) do self:InfoClean(PT[k]) end
        self:InfoClean("Use finger command to see more detailed warn info on client( /lua help finger )")
        return self.SUCCESS
    end
    
    local warnExpireTime = Misc:getTimeFormat(Config.Warn.Expires)
    if warnExpireTime < 0 then warnExpireTime = 0 end
    
    if Misc:triml(Args[1]) == "offline" then
        local rudclient = RegularUser:getById(Args[2])
        if not rudclient then return self:Error("Cannot find rudclient id") end
        if not Client:isConsole() then
            if not Client:isAuth() and rudclient.level > Client:getLevel() then return self:Error("Client higher level than you") end
        end
        local warn = { warner = Client:getName(), reason = "", }
        if not Args[3] then
            if Config.Warn.AllowNoReason then
                RegularUser:warn(rudclient,warn)
                self:Info("Warning issued to offline user: " .. rudclient.lastname)
                return self.SUCCESS
            else
                return self:Error("Must use a reason when warning")
            end
        elseif Misc:triml(Args[3]) == "remove" then
            if RegularUser:removeWarning(rudclient.warnings,Args[4]) then
                self:Info("Removed warning from " .. rudclient.lastname)
                return self.SUCCESS
            else
                return self:Error("Could not find warning")
            end
        elseif Misc:triml(Args[3]) == "clear" then
            rudclient.warnings = { }
            self:Info(rudclient.lastname .. Color.Primary .. "'s warnings cleared")
            return self.SUCCESS
        else
            warn.reason = Misc:trim(table.concat(Args, " ",3) )
            RegularUser:warn(rudclient,warn)
            self:Info("Warning issued to offline user: " .. rudclient.lastname)
            return self.SUCCESS
        end
    end
    local Target = ClientHandler:getClient(Args[1])
    if not Target then return self:Error("Cannot find client") end
    if not Target.rudclient then return self:Error(va("Cannot find %s rudclient",Target:getName() )) end
    if not Client:isConsole() then
        if not Client:isAuth() and Target:getLevel() > Client:getLevel() then return self:Error(va("%s higher level than you" , Target:getName() )) end
    end
    local warn = { warner = Client:getName(), reason = "", }
    if not Args[2] then
        if Config.Warn.AllowNoReason then
            RegularUser:warn(Target.rudclient,warn)
            self:Info("Warning issued to " .. Color.Secondary  .. Target:getName())
            --Target:Chat("You have been warned by " .. Color.Secondary .. Client:getName() )
            Game:Play(Config.Sound.Warn)
            return self.SUCCESS
        else
            return self:Error("Must use a reason when warning")
        end
    elseif Misc:triml(Args[2]) == "remove" then
        if RegularUser:removeWarning(Target.rudclient.warnings,Args[3]) then
            self:Info("Removed warning from " .. Target:getName())
            Target:Chat("You have been dewarned by " .. Color.Secondary .. Client:getName() )
            return self.SUCCESS
        else
            return self:Error("Could not find warning")
        end
    elseif Misc:triml(Args[2]) == "clear" then
        Target.rudclient.warnings = { }
        self:Info(Color.Secondary .. Target:getName() .. Color.Primary .. "'s warnings cleared")
        Target:Chat("Your warnings were cleared by " .. Color.Secondary .. Client:getName() )
        return self.SUCCESS
    else
        warn.reason = Misc:trim(table.concat(Args, " ",2) )
        RegularUser:warn(Target.rudclient,warn)
        self:Info("Warning issued to " .. Color.Secondary .. Target:getName() )
        --Target:Chat("You have been warned by " .. Color.Secondary .. Client:getName() .. Color.Primary .. " with the reason: " .. Color.Secondary .. warn.reason )
        Game:Play(Config.Sound.Warn)
        return self.SUCCESS
    end
end

return WarnCommand