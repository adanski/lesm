local WhoAmICommand = Command("WhoAmI")

WhoAmICommand.ADMIN          = true
WhoAmICommand.CHAT           = true
WhoAmICommand.LUA            = true
WhoAmICommand.AUTH           = true
WhoAmICommand.GLOBALOUTPUT   = true
WhoAmICommand.DESCRIPTION    = { "Tells what you are currently spawned as" , }

function WhoAmICommand:Run(Client,Args)
    if not Client:isConnected() then return self:Error("Not connected") end
    if Client:getTeam() == et.TEAM_SPEC then
        self:InfoAll(Client:getName() .. Color.Primary .. " is a spectator!" )
        return self.SUCCESS
    end
    
    local weaponId    = Client:getWeapon()
    local weapon      = WeaponHandler:getWithId()
    local class       = Client:getClassTable()
    --Console:Info(tostring(weaponId))
    --Console:Info(tostring(weapon))
    --Console:Info(tostring(class))
    if not class or not weapon then
        return self:Error("Could not get your data, server does not have values set up.")
    end
    self:InfoAll(Client:getName() .. Color.Primary .. " is a " .. Color.Secondary .. class.name .. Color.Primary .. " with a " .. Color.Secondary .. weapon.nickname)
    return self.SUCCESS
end

return WhoAmICommand