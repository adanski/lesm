ConfigHandler = { }

--- Creates new config based on default options
local _CreateNewConfig = function()
    local NewConfig = { }
    for k=1,tlen(DefaultOptions) do
        local opt = DefaultOptions[k]
        if ( opt.type == "table" and next(opt.default) and not opt.list ) then
            Config[opt.name]      = { }
            NewConfig[opt.name]   = { }
            for j=1,tlen(opt.default) do
                local defaultOption = Misc:ConvertType(opt.default[j].type,opt.default[j].default)
                if defaultOption == nil then
                    Console:Error("cfg." .. tostring(opt.name) .. "." .. tostring(opt.default[j].name) .. " = " .. tostring(defaultOption) .. " (" .. tostring(opt.default[j].type) .. "," ..type(defaultOption) ..")")
                end
                Config[opt.name][opt.default[j].name]    = defaultOption
                NewConfig[opt.name][opt.default[j].name] = defaultOption
            end
        else
            local defaultValue = Misc:ConvertType(opt.type,opt.default)
            if defaultValue == nil then
                Console:Error("cfg." .. tostring(opt.name) .. " = " .. tostring(defaultValue) .. " (" .. tostring(opt.type) .. "," ..type(defaultValue) ..")")
            end
            Config[opt.name]    = defaultValue
            NewConfig[opt.name] = defaultValue
        end
    end
    return NewConfig
end

local _lower = string.lower
--- Registers A Config option and its default value
local _RegisterOption = function(keyName,type,default,list)
    if keyName == nil then return end
    if type    == nil then return end
    if default == nil then return end
    
    keyName = Misc:trim(keyName)
    type    = Misc:triml(type)
    default = Misc:ConvertType(type,default)
    
    local Option   = { }
    Option.name    = keyName
    Option.type    = type
    Option.default = default
    Option.list    = list or false
    
    DefaultOptions[tlen(DefaultOptions)+1] = Option
end

local _RegisterTableOption = function(tableName,keyName,type,default,list)
    if tableName == nil then return end
    if keyName   == nil then return end
    if type      == nil then return end
    if default   == nil then return end
    
    tableName = Misc:trim(tableName)
    keyName   = Misc:trim(keyName)
    type      = Misc:triml(type)
    default   = Misc:ConvertType(type,default)
    --Console:InfoClean( string.format( "Registering Key Table: \ntname: %s\nkeyname: %s\ntype: %s\ndefault: %s" , tostring(tableName) , tostring(keyName) ,tostring(type),tostring(default) ) )
    local DefaultOption;
    for k=1,tlen(DefaultOptions) do
        if _lower(DefaultOptions[k].name) == _lower(tableName) then
            DefaultOption = DefaultOptions[k]
            break
        end
    end
    
    if not DefaultOption then
        DefaultOption = { name=tableName , type="table" , default={} }
        DefaultOptions[tlen(DefaultOptions)+1] = DefaultOption
    end
    
    local Option = { }
    Option.name    = keyName
    Option.type    = type
    Option.default = default
    Option.list    = false
    
    DefaultOption.default[tlen(DefaultOption.default)+1] = Option
end

---  Registers all ConfigOptions
local _RegisterOptions = function()
    DefaultOptions = { }
    
    -- Command prefix's
    _RegisterTableOption("Prefix" , "Lua"   , "string" , "lua")
    _RegisterTableOption("Prefix" , "Auth"  , "string" , "sudo")
    _RegisterTableOption("Prefix" , "Mail"  , "string" , "mail")
    _RegisterTableOption("Prefix" , "Chat"  , "string" , "!")
    _RegisterTableOption("Prefix" , "Admin" , "string" , "!")
    
    -- Banner options
    _RegisterTableOption("Banner","Enabled"  , "boolean" , false)
    _RegisterTableOption("Banner","Location" , "string"  , "chat")
    _RegisterTableOption("Banner","AddRules" , "boolean" , false)
    _RegisterTableOption("Banner","Random"   , "boolean" , false)
    _RegisterTableOption("Banner","Delay"    , "number"  , 60)
    
    -- JSON FilesFiles
    _RegisterTableOption("JSON","Banners"    ,"string","Banners.json")
    _RegisterTableOption("JSON","Users"      ,"string","Users.json")
    _RegisterTableOption("JSON","Colors"     ,"string","Colors.json")
    _RegisterTableOption("JSON","Sounds"     ,"string","Sounds.json")
    _RegisterTableOption("JSON","Rules"      ,"string","Rules.json")
    _RegisterTableOption("JSON","Blacklist"  ,"string","Blacklist.json")
    _RegisterTableOption("JSON","SwearFilter","string","SwearFilter.json")
    _RegisterTableOption("JSON","Grammar"    ,"string","Grammar.json")
    _RegisterTableOption("JSON","Entities"   ,"string","Entities.json")
    _RegisterTableOption("JSON","Cvars"      ,"string","Cvars.json")
    
    -- XpSave
    -- TODO timeform,at these
    _RegisterTableOption("XpSave","Enabled"      ,"boolean" , false )
    _RegisterTableOption("XpSave","Bots"         ,"boolean" , true  )
    _RegisterTableOption("XpSave","RequireLogin" ,"boolean" , false )
    _RegisterTableOption("XpSave","Expire"       ,"number"  , 0     ) -- Seconds that must pass of client not connected before their xp expires
    _RegisterTableOption("XpSave","Decay"        ,"number"  , 0     ) -- Seconds that must pass of client not connected before decay starts
    _RegisterTableOption("XpSave","DecayRate"    ,"number"  , 0     ) -- Rate at which each skill will decrease
    _RegisterTableOption("XpSave","DecayMin"     ,"number"  , 0     ) -- Minimum a skill can go down to
    
    -- Debugging
    _RegisterTableOption("Debug","Enabled"   , "boolean" , false )
    _RegisterTableOption("Debug","Stream"    , "boolean" , false )
    _RegisterTableOption("Debug","ToChat"    , "boolean" , false )
    _RegisterTableOption("Debug","ToPrint"   , "boolean" , false )
    _RegisterTableOption("Debug","switchteam", "boolean" , false )
    _RegisterTableOption("Debug","callback"  , "boolean" , false )
    _RegisterTableOption("Debug","file"      , "boolean" , false )
    _RegisterTableOption("Debug","balance"   , "boolean" , false )
    _RegisterTableOption("Debug","setteam"   , "boolean" , false )
    _RegisterTableOption("Debug","mail"      , "boolean" , false )
    _RegisterTableOption("Debug","entspawn"  , "boolean" , false )
    _RegisterTableOption("Debug","command"   , "boolean" , false )
    _RegisterTableOption("Debug","core"      , "boolean" , false )
    _RegisterTableOption("Debug","other"     , "boolean" , false )
    _RegisterTableOption("Debug","getteams"  , "boolean" , false )
    _RegisterTableOption("Debug","xpsave"    , "boolean" , false )
    
    -- AutoLevel
    -- If autolevel start level isn't 1 then what do we want to do
    -- Only level if user's level is StartLevel-1 ?
    _RegisterTableOption("AutoLevel","Enabled"      , "boolean",false  )
    _RegisterTableOption("AutoLevel","RequireLogin" , "boolean",false  )
    _RegisterTableOption("AutoLevel","NoSetLevel"   , "boolean",false  )
    _RegisterTableOption("AutoLevel","StartLevel"   , "number" , 1     )
    _RegisterTableOption("AutoLevel","EndLevel"     , "number" , 10    )
    _RegisterTableOption("AutoLevel","XPBase"       , "number" , 500.0 )
    _RegisterTableOption("AutoLevel","XPModifier"   , "number" , 1.75  )
    _RegisterTableOption("AutoLevel","Location"     , "string" , "chat"  )
    _RegisterTableOption("AutoLevel","Message"      , "string" , "<name><color1> has reached <color3><color2>xp <color1>and has been promoted to level <color3><newlevel>")

    -- MessageFilter
    _RegisterTableOption("Message" , "GrammarCheck" , "boolean" , false )
    _RegisterTableOption("Message" , "NoCaps"       , "boolean" , false )
    _RegisterTableOption("Message" , "NoColors"     , "boolean" , false )
    _RegisterTableOption("Message" , "SwearFilter"  , "boolean" , false )
    _RegisterTableOption("Message" , "ColorReplace" , "boolean" , false )
    _RegisterTableOption("Message" , "NameMentions" , "boolean" , false )
    _RegisterTableOption("Message" , "EventMentions", "boolean" , false )
    _RegisterTableOption("Message" , "TableSymbol"  , "string"  , ":"   )
    _RegisterTableOption("Message" , "TableColor"   , "string"  , "<color3>"   )
    _RegisterTableOption("Message" , "Format"       , "string"  , "<color3>|<color2>#<color3>|<color1> <message> <color3>|<color2>#<color3>|" )
    -- add global vsay bypass
    
    -- PingMonitor
    _RegisterTableOption("PingMonitor" , "BlacklistTime", "string"  , "10m" )
    _RegisterTableOption("PingMonitor" , "Warn"         , "boolean" , false )
    _RegisterTableOption("PingMonitor" , "Spec"         , "boolean" , false )
    _RegisterTableOption("PingMonitor" , "Message"      , "string"  , "Your average ping is too high")
    _RegisterTableOption("PingMonitor" , "Ping"         , "number"  , 900 )
    _RegisterTableOption("PingMonitor" , "MinChecks"    , "number"  , 10 )
    
    -- Blacklist
    --_RegisterTableOption("Blacklist","Message"       , "string" , "You have been blacklisted from this server")
    _RegisterTableOption("Blacklist","AllowIfLeader" , "boolean" , false)
    _RegisterTableOption("Blacklist","AllowIfAuth"   , "boolean" , false)
    
    -- ClanTag Protection
    _RegisterTableOption("ClanTag","Enabled"       , "boolean" , false)
    _RegisterTableOption("ClanTag","Warn"          , "boolean" , false)
    _RegisterTableOption("ClanTag","Exact"         , "boolean" , true )
    _RegisterTableOption("ClanTag","BlacklistTime" , "string"  , "1h" )
    _RegisterTableOption("ClanTag","MaxRenames"    , "number"  , 3    )
    _RegisterTableOption("ClanTag","Level"         , "number"  , 0    )
    _RegisterTableOption("ClanTag","Message"       , "string"  ,  "Your name contains our clantag, please change it.")
    _RegisterTableOption("ClanTag","Tags"          , "table"   , { }  )
    -- IDEA: Action table: { "warn" , "warn" , "rename" , "rename" , "kick" , "blacklist" }
    
    -- SOUNDS
    _RegisterTableOption("Sound","Enabled"        , "boolean" , true)
    _RegisterTableOption("Sound","Error"          , "string" , "sound/lua/error.wav")           -- DONE
    _RegisterTableOption("Sound","Command"        , "string" , "sound/lua/command.wav")         -- DONE
    _RegisterTableOption("Sound","Adminwatch"     , "string" , "sound/lua/adminwatch.wav")      -- DONE
    _RegisterTableOption("Sound","Autolevel"      , "string" , "sound/lua/autolevel.wav")       -- DONE
    _RegisterTableOption("Sound","Welcome"        , "string" , "sound/lua/welcome.wav")         -- DONE
    _RegisterTableOption("Sound","Connect"        , "string" , "sound/lua/connect.wav")         -- DONE
    _RegisterTableOption("Sound","Disconnect"     , "string" , "sound/lua/disconnect.wav")      -- DONE
    _RegisterTableOption("Sound","Blacklist"      , "string" , "sound/lua/blacklist.wav")       -- DONE
    _RegisterTableOption("Sound","Login"          , "string" , "sound/lua/login.wav")           -- DONE
    _RegisterTableOption("Sound","Logout"         , "string" , "sound/lua/logout.wav")          -- DONE
    _RegisterTableOption("Sound","Register"       , "string" , "sound/lua/register.wav")        -- DONE
    _RegisterTableOption("Sound","Salute"         , "string" , "sound/lua/salute.wav")          -- DONE
    _RegisterTableOption("Sound","KillConfirm"    , "string" , "sound/lua/killconfirm.wav")     -- DONE
    _RegisterTableOption("Sound","TeamKillConfirm", "string" , "sound/lua/teamkillconfirm.wav") -- DONE
    _RegisterTableOption("Sound","NewMail"        , "string" , "sound/lua/newmail.wav")         -- DONE
    _RegisterTableOption("Sound","Hello"          , "string" , "sound/lua/hi.wav")              -- DONE
    _RegisterTableOption("Sound","Bye"            , "string" , "sound/lua/bye.wav")             -- DONE
    _RegisterTableOption("Sound","Afk"            , "string" , "sound/lua/afk.wav")             -- DONE
    _RegisterTableOption("Sound","Return"         , "string" , "sound/lua/return.wav")          -- DONE
    _RegisterTableOption("Sound","PrivateMessage" , "string" , "sound/lua/privatemessage.wav")  -- DONE
    _RegisterTableOption("Sound","Warn"           , "string" , "sound/lua/warn.wav")            -- TODO
    _RegisterTableOption("Sound","Mention"        , "string" , "sound/lua/mention.wav")            -- TODO when messagefilter redone
    
    -- TODO Will make this into a table eventually
    -- Multi { 2, "sound/lua/kills/2.wav", "<clientname> got a doublekill on <victim1name> and <victim2name>" }
    -- Spree { 5, "sound/lua/spree/5.wav", "<clientname> is on a 5 kill spree!" }
    _RegisterTableOption("Sound","kill2"          , "string" , "sound/lua/kills/2.wav")
    _RegisterTableOption("Sound","kill3"          , "string" , "sound/lua/kills/3.wav")
    _RegisterTableOption("Sound","kill4"          , "string" , "sound/lua/kills/4.wav")
    _RegisterTableOption("Sound","kill5"          , "string" , "sound/lua/kills/5.wav")
    _RegisterTableOption("Sound","kill6"          , "string" , "sound/lua/kills/6.wav")
    _RegisterTableOption("Sound","kill7"          , "string" , "sound/lua/kills/7.wav")
    _RegisterTableOption("Sound","kill8"          , "string" , "sound/lua/kills/8.wav")
    _RegisterTableOption("Sound","kill9"          , "string" , "sound/lua/kills/9.wav")
    _RegisterTableOption("Sound","kill10"         , "string" , "sound/lua/kills/10.wav")
    _RegisterTableOption("Sound","killfirst"          , "string" , "sound/lua/kills/first.wav")
    _RegisterTableOption("Sound","spree5"         , "string" , "sound/lua/spree/5.wav")
    _RegisterTableOption("Sound","spree10"        , "string" , "sound/lua/spree/10.wav")
    _RegisterTableOption("Sound","spree15"        , "string" , "sound/lua/spree/15.wav")
    _RegisterTableOption("Sound","spree20"        , "string" , "sound/lua/spree/20.wav")
    
    --- LEVELS
    -- May need more in the future, But for now we can go off this.
    _RegisterTableOption("Level","Admin"  , "number" , 10)
    _RegisterTableOption("Level","Leader" , "number" , 12)
    
    --- Profile Options
    _RegisterTableOption("Profile","ForceRegister"  , "boolean" , false  )
    _RegisterTableOption("Profile","RequireBoth"    , "boolean" , false  )
    _RegisterTableOption("Profile","AutoCreate"     , "boolean" , false  )
    _RegisterTableOption("Profile","Greetings"      , "boolean" , false  )
    _RegisterTableOption("Profile","SoundGreetings" , "boolean" , false  )
    _RegisterTableOption("Profile","Farewells"      , "boolean" , false  )
    _RegisterTableOption("Profile","SoundFarewells" , "boolean" , false  )
    _RegisterTableOption("Profile","GreetingFormat" , "string"  , "<color2><<color1><clientname><color2>> <color1><message>" )
    _RegisterTableOption("Profile","FarewellFormat" , "string"  , "<color2><<color1><clientname><color2>> <color1><message>" )
    _RegisterTableOption("Profile","Location"       , "string"  , "chat" )
    _RegisterTableOption("Profile","MaxIp"          , "number"  , 3      )
    _RegisterTableOption("Profile","MaxGuid"        , "number"  , 1      )
    _RegisterTableOption("Profile","AutoWidth"      , "number"  , 0)
    
    --- Voting
    _RegisterTableOption("Vote","Mute"         , "boolean" , true)
    _RegisterTableOption("Vote","RequireLogin" , "boolean" , false)
    _RegisterTableOption("Vote","RequireLevel" , "number" , 0)
    
    --- Warning handles
    _RegisterTableOption("Warn","Expires"       , "string"  , "48h" ) -- in Hours, below 0 means they dont expire can use float value too
    _RegisterTableOption("Warn","Max"           , "number"  , 3     ) -- Max amount of warns before action taken (Currently only blacklist) 0 and lower means it will take no action
    _RegisterTableOption("Warn","RemoveLast"    , "boolean" , true  )
    _RegisterTableOption("Warn","BlacklistTime" , "string"  , "48h" )
    _RegisterTableOption("Warn","Mail"          , "boolean" , true  ) -- goes to mail when warn
    _RegisterTableOption("Warn","AllowNoReason" , "boolean" , true  )
    
    --- Karma
    _RegisterTableOption("Karma","Consecutive" , "number" , 1)
    _RegisterTableOption("Karma","Time"        , "string" , "1h")
    
    --- Intermission
    -- Voting and messages
    _RegisterTableOption("Intermission","Mute"         , "boolean" , false)
    _RegisterTableOption("Intermission","RequireLogin" , "boolean" , false)
    _RegisterTableOption("Intermission","BlockSpec"    , "boolean" , false)
    _RegisterTableOption("Intermission","RequireLevel" , "number"  , 0    )
    _RegisterTableOption("Intermission","ReadyMessage" , "boolean" , false)
    _RegisterTableOption("Intermission","VoteMessage"  , "boolean" , false)
    _RegisterTableOption("Intermission","PrintBoth  "  , "boolean" , false) -- true if you want both to go to chat seperately
                                                                            -- false if you want the first one to go to print, and then second to go to chat together

    --- DeathPrint
    _RegisterTableOption("DeathPrint","Global"   , "boolean" , false) -- Would get extremely spamming. Expiermental for future obituary replacement
    _RegisterTableOption("DeathPrint","Message"  , "string" , "<killername><c1> <teamkilled> you from <distancemessage><c1> with <c3><killerentity(health)><c2>hp")
    _RegisterTableOption("DeathPrint","Location" , "string" , "chatclean")
    
    _RegisterTableOption("Distance","Measurement"    , "number" , 0) -- 0 for quake units 1 for feet / inches 2 for metric
    _RegisterTableOption("Distance","ImperialFeet"   , "string" , "<c3><feet><c2>feet" )
    _RegisterTableOption("Distance","ImperialInches" , "string" , "<c3><inches><c2>inches" )
    _RegisterTableOption("Distance","Metric"         , "string" , "<c3><metres> <c2>metres" )
    _RegisterTableOption("Distance","Unit"           , "string" , "<c3><units> <c2>units" )
    
    --- Connect Print
    _RegisterTableOption("ConnectPrint","Message"       , "string" , "<clientname><c1> connected")
    _RegisterTableOption("ConnectPrint","MessageBots"   , "string" , "<clientname><c1> connected")
    _RegisterTableOption("ConnectPrint","Location"      , "string" , "chatclean")
    
    --- BalanceTeam options
    _RegisterTableOption("Balance","Enabled"       , "boolean" , false)
    _RegisterTableOption("Balance","CountBots"     , "boolean" , false)
    _RegisterTableOption("Balance","MaxSwitches"   , "number"  , 4    )
    _RegisterTableOption("Balance","BlockTeam"     , "boolean" , false)
    _RegisterTableOption("Balance","PutSpec"       , "boolean" , false)
    _RegisterTableOption("Balance","BlacklistTime" , "string"  , "0" ) -- 0 = disabled
    _RegisterTableOption("Balance","KickReason"    , "string"  , "Unbalancing teams too many times")
    _RegisterTableOption("Balance","ExcludeOnline" , "boolean" , false)
    _RegisterTableOption("Balance","ExcludeAuth"   , "boolean" , false)
    _RegisterTableOption("Balance","ExcludeLevel"  , "number"  , -1)
    
    --- DynamiteCounter options
    _RegisterTableOption("DynamiteCounter","Enabled"   , "boolean" , false)
    _RegisterTableOption("DynamiteCounter","Plant"     , "boolean" , false)
    _RegisterTableOption("DynamiteCounter","Explode"   , "boolean" , false)
    _RegisterTableOption("DynamiteCounter","Defuse"    , "boolean" , false)
    _RegisterTableOption("DynamiteCounter","PrintAll"  , "boolean" , false)
    _RegisterTableOption("DynamiteCounter","Location"  , "string"  , "chat")
    _RegisterTableOption("DynamiteCounter","CountDown" , "table" , { 20 , 10 , 5 , 3 , 2 , 1, })

    _RegisterTableOption("Log","Full"    , "boolean" , false)
    _RegisterTableOption("Log","Colors"  , "boolean" , false)
    _RegisterTableOption("Log","OneFile" , "boolean" , true)
    
    _RegisterTableOption("Shrubbot", "BlockNoSilentFlag",   "boolean", true) -- OLD Config.BlockNoSilentFlag
    _RegisterTableOption("Shrubbot", "ConvertNoSilentFlag", "boolean", true) -- OLD Config.ConvertNoSilentFlag
    _RegisterTableOption("Shrubbot", "Warn",                "boolean", true) -- OLD Config.Warn.Shrubbot
    _RegisterTableOption("Shrubbot", "Mute",                "boolean", true)
    _RegisterTableOption("Shrubbot", "CommandFix", "table", { }, true) -- OLD Config.ShrubCommandFix
    
    _RegisterOption("PauseStart"       , "boolean" , false )
    _RegisterOption("DamagePrint"      , "boolean" , false )
    _RegisterOption("SpawnWalkthrough" , "boolean" , false )
    _RegisterOption("ClassHPRegen"     , "boolean" , false )
    _RegisterOption("ClassHPMax"       , "boolean" , false )
    _RegisterOption("MapEnts"          , "boolean" , false )
    _RegisterOption("ReportToMail"     , "boolean" , true  )
    _RegisterOption("AdminWatchConsole", "boolean" , false )
    _RegisterOption("Encrypt"          , "boolean" , false  ) -- lua 5.0 does not allow 0x characters in code, so basically of sha1 fails
    -- If true will use a basic string encrpyt in lua 5.0 sha1 in 5.1 and sha2 in 5.3
    
    _RegisterOption("TeamBlock"           , "number" , 0.0)
    _RegisterOption("TeamBlockMultiplier" , "number" , 3.0)
    _RegisterOption("MaxReports"          , "number" , 3)
    _RegisterOption("SpamProtect"         , "number" , 1.00)
    _RegisterOption("SetLevelMax"         , "number" , 2)
    _RegisterOption("Spec999"             , "number" , 0)
    _RegisterOption("Rename"              , "table"  , { } , true)
    
    --_RegisterOption("FreezeStart"     , "boolean" , false )
    _RegisterOption("FreezeStart"     , "number"  , 0    ) -- if 0 or lower then do not activate
    _RegisterOption("MapListStart"    , "number"  , 1     )
    _RegisterOption("MapListVarName"  , "string"  , "d"   )
    _RegisterOption("DisplayRegister" , "boolean" , true  )
    _RegisterOption("MinNameLength"   , "number"  , 1     )
    _RegisterOption("ConMaxChars"     , "number"  , 38    )
    
    -- TODO Fix empty json files error
    -- TODO ClanTag warns verify
    -- TODO Combine regularuserdata and userprofile
    -- TODO Move connectprint/deathprint/damageprint into a client
    -- TODO admin mail shows to non admin
    
    -- messageformat message 
    --      <name><damage> <weapon> etc. 
end

local _CreateLoweredConfig = function(ConfigJSON)
    local ConfigJSON_Lower = { }
    for k, v in pairs(ConfigJSON) do
        if type(v) == "table" and not v[1] then
            ConfigJSON_Lower[string.lower(k)] = { }
            for k2,v2 in pairs(v) do
                ConfigJSON_Lower[string.lower(k)][string.lower(tostring(k2))] = v2
            end
        else
            ConfigJSON_Lower[string.lower(k)] = v
        end
    end
    return ConfigJSON_Lower
end
--- Loads the config
-- if it does not exist create one
-- if errors create Configdefaults as a reference config
function ConfigHandler:Init()
    Console:Info("Loading Config.json")
    _RegisterOptions()
    local NewConfig = _CreateNewConfig()
    local errors    = { }
    if not FileHandler:exists("LESM/save/Config.json") then
        Console:Info("Your config does not exist, we will create a new one for you")
        FileHandler:Save{ filePath = "LESM/save/Config.json" , fileData = NewConfig  }
        Console:Info("Configuration loaded.")
        return
    end
    
    if not next(errors) then
        local ConfigJSON = FileHandler:Load{ filePath = "LESM/save/Config.json" , makeIfNotExists = false, } -- Use a more complex algorithm to make it if it does not exist.
        if not next(ConfigJSON) then -- Config failed to load, most likely due to syntax errors
            errors[tlen(errors)+1] = "Your Config was found, but was empty - Defaulting to default config"
            errors[tlen(errors)+1] = "It is possible your config syntax is wrong"
            errors[tlen(errors)+1] = "ConfigDefaults.json has been created"
            errors[tlen(errors)+1] = "Fix your config or copy ConfigDefaults to Config"
        else -- Config successfully read
            --Console:Info("ConfigJSON_Lower START")
            local ConfigJSON_Lower = _CreateLoweredConfig(ConfigJSON)
            --Console:Info("ConfigJSON_Lower END")

            --Console:Info("DefaultOptions START")
            for k=1,tlen(DefaultOptions) do
                local opt = DefaultOptions[k]
                if ConfigJSON_Lower[string.lower(opt.name)] == nil or type(ConfigJSON_Lower[string.lower(opt.name)]) ~= opt.type then -- Config value not present
                    errors[tlen(errors)+1] = opt.name
                    if ( opt.type == "table" and next(opt.default) and not opt.list ) then
                        Config[opt.name]           = { }
                        NewConfig[opt.name]        = { }
                        ConfigJSON_Lower[opt.name] = { }
                        for j=1,tlen(opt.default) do
                            Config[opt.name][opt.default[j].name]      = Misc:ConvertType(opt.default[j].type,opt.default[j].default)
                            NewConfig[opt.name][opt.default[j].name] = Misc:ConvertType(opt.default[j].type,opt.default[j].default)
                        end
                    else
                        Config[opt.name]      = Misc:ConvertType(opt.type,opt.default)
                        NewConfig[opt.name]   = Misc:ConvertType(opt.type,opt.default)
                    end
                else -- Config value is present - Needs to be converted to correct type.
                    if ( opt.type == "table" and next(opt.default) and not opt.list ) then
                        for j=1,tlen(opt.default) do
                            if ConfigJSON_Lower[string.lower(opt.name)][string.lower(opt.default[j].name)] == nil then -- Config value not present
                                errors[tlen(errors)+1] = opt.name .."."..opt.default[j].name
                                Config[opt.name][opt.default[j].name]      = Misc:ConvertType(opt.default[j].type,opt.default[j].default)
                                NewConfig[opt.name][opt.default[j].name] = Misc:ConvertType(opt.default[j].type,opt.default[j].default)
                            else
                                Config[opt.name][opt.default[j].name]      = Misc:ConvertType(opt.default[j].type,ConfigJSON_Lower[string.lower(opt.name)][string.lower(opt.default[j].name)])
                                NewConfig[opt.name][opt.default[j].name] = Misc:ConvertType(opt.default[j].type,ConfigJSON_Lower[string.lower(opt.name)][string.lower(opt.default[j].name)])
                            end
                        end
                    else
                        Config[opt.name]    = Misc:ConvertType(opt.type,ConfigJSON_Lower[string.lower(opt.name)])
                        NewConfig[opt.name] = Misc:ConvertType(opt.type,ConfigJSON_Lower[string.lower(opt.name)])
                    end
                end
            end
--            Console:Info("DefaultOptions END")
        end
    end
    
    if not next(errors) then -- Config loaded with 0 missing values
        Console:Info("Configuration loaded.")
    else -- Config loaded with errors
        Console:Info("Config is missing values")
        for k=1,tlen(errors) do
            Console:InfoClean("[Config] " .. errors[k])
        end
        Console:Info("Config is missing values")
        FileHandler:Save{ filePath = "LESM/save/ConfigDefaults.json",fileData=NewConfig}
    end
end


--- Saves Config
-- Relys on config being updated
function ConfigHandler:Save()
    local ConfigJSON = { }
    for k=1,tlen(DefaultOptions) do
        local opt = DefaultOptions[k]
        if opt.type == "table" and not opt.list then
            ConfigJSON[opt.name] = { }
            if type(Config[opt.name]) ~= "table" then Config[opt.name] = { } end
            if next(opt.default) then
                for j=1,tlen(opt.default) do
                    ConfigJSON[opt.name][opt.default[j].name] = Misc:ConvertType(opt.default[j].type,Config[opt.name][opt.default[j].name])
                end
            end
        else
            ConfigJSON[opt.name] = Misc:ConvertType(opt.type,Config[opt.name])
        end
    end
    FileHandler:Save{ filePath = "LESM/save/Config.json",fileData = ConfigJSON }
end

--- Gets Config[key]
function ConfigHandler:getOption(key)
    key = Misc:triml(key)
    local key2;
    if string.find(key,"(%w+)%.(%w+)") then key,key2 = Misc:match(key,"(%w+)%.(%w+)") end
    --Console:Info("ConfigHandler:getOption( "..tostring(key) .. " , " .. tostring(key2) .." )")
    for k=1,tlen(DefaultOptions) do
        if string.lower(DefaultOptions[k].name) == key then
            if DefaultOptions[k].type == "table" then
                if not key2 then
                    --Console:Info("getOption key found")
                    return Config[DefaultOptions[k].name]
                end
                for j=1,tlen(DefaultOptions[k].default) do 
                    if string.lower(DefaultOptions[k].default[j].name) == key2 then
                        --Console:Info("getOption key2 found")
                        return Config[DefaultOptions[k].name][DefaultOptions[k].default[j].name]
                    end
                end
                return -- Return here, to prevent overwriting tables
            end
            --Console:Info("getOption key found (" .. tostring(DefaultOptions[k].name)  .. " , " .. tostring(DefaultOptions[k].type) .. " , " .. tostring(DefaultOptions[k].default) .. " )")
            return Config[DefaultOptions[k].name]
        end
    end
    return nil
end

--- Sets Config[key] to [value]
function ConfigHandler:setOption(key,value)
    key = Misc:triml(key)
    local key2;
    if string.find(key,"(%w+)%.(%w+)") then key,key2 = Misc:match(key,"(%w+)%.(%w+)") end
    for k=1,tlen(DefaultOptions) do
        if string.lower(DefaultOptions[k].name) == key then
            if DefaultOptions[k].type == "table" then
                if not key2 then return end
                for j=1,tlen(DefaultOptions[k].default) do 
                    if string.lower(DefaultOptions[k].default[j].name) == key2 then
                        Config[DefaultOptions[k].name][DefaultOptions[k].default[j].name] = Misc:ConvertType(DefaultOptions[k].default[j].type,value)
                        return
                    end
                end
                return -- Return here, to prevent overwriting tables
            end
            -- This will shows value as expected only if true, if false then it wont get this far because it returns nil
            local value_test = Misc:ConvertType(DefaultOptions[k].type,value)
            
            -- This shows up as false as expected
            
            Config[DefaultOptions[k].name] = value_test
            
            -- Just incase it set it to nil then set it to false
            if Config[DefaultOptions[k].name] == nil then Config[DefaultOptions[k].name] = false end
            
            -- Shows nil
            return
        end
    end
end