Misc = { }

--- A lot of the functions here are borrowed from various forums and modified slightly
-- Misc functions should not be dependant on any other class
-- Exception #1 is loaded libaries like sha1 or json
-- Exception #2 is debug messages
-- There are some areas where it looks for config options I really shouldn't but it would require a proxy function otherwise

local _char = string.char
local _byte = string.byte
local _sub  = string.sub
local _len  = string.len
local _rand = math.random
local _floor = math.floor
local _ceil = math.ceil
local _mod  = math.mod
local tonumber = tonumber

local _vowels = { 97, 101, 105, 111, 117, }
local _consonants = { 98,99,100,102,103,104,106,107,108,109,110,112,113,114,115,116,118,119,120,121,122, }

local _getVowel = function()
    local r = _rand(1,tlen(_vowels))
    return _char(_vowels[r])
end

local _getConsonant = function()
    local r = _rand(1,tlen(_consonants))
    return _char(_consonants[r])
end

local _getValidCharacter = function()
    local n;
    while true do
        n = _rand(33, 125)
        if ( n ~= 34 and n ~= 37 and n ~= 39 and n ~= 47 and n ~= 59 and n ~= 94 and n ~= 96 ) then
            return _char(n)
        end
    end
end

------------------
-- TABLE FUNCTIONS
------------------

--- Gets table average
-- Requires list
function Misc:table_average(t)
    local avg = 0
    if not t then return avg end
    for k = 1 , tlen(t) do 
        avg = avg + t[k]
    end
    return ( avg / tlen(t) )
end

--- Returns table t[i_start] to t[i_end]
-- i_start defaults 1
-- i_end defaults tlen(t)
function Misc:table_range(t,i_start,i_end)
    if not t then return { } end
    if not next(t) then return t end
    i_start = tonumber(i_start) or 1
    i_end   = tonumber(i_end) or tlen(t)
    local new_t = { }
    for k=i_start,i_end do
        new_t[tlen(new_t)+1] = t[k]
    end
    return new_t
end

--- Shuffle a table
-- returns new shuffled table
function Misc:table_shuffle(t)
    local new_t = { }
    while tlen(t) > 0 do
        table.insert(new_t, table.remove(t, _rand(tlen(t)) ) )
    end
    return new_t
end

function Misc:table_total(t)
    if not t then return 0.0 end
    local total = 0.0
    for k = 1 , tlen(t) do 
        total = t[k] + total
    end
    return total or 0.0
end

--- Merges two tables
-- returns a merged table with no duplicate values
function Misc:table_merge(t1,t2)
    if ( t1 == nil and t2 == nil ) then return { } end
    if t1 == nil then return t2 end
    if t2 == nil then return t1 end
    local final_table = { }
    for k = 1 , tlen(t1) do 
        local found = false
        for j = 1 , tlen(t2) do 
            if t1[k] == t2[j] then
                found = true
                break
            end
        end
        if not found then final_table[tlen(final_table)+1] = t1[k] end
    end
    for k = 1 , tlen(t2) do 
        final_table[tlen(final_table)+1] = t2[k]
    end
end

--- Checks if a value exists in a table
-- returns true or false
function Misc:table_find(t,v)
    local f = false
    for k = 1 , tlen(t) do 
        if t[k] == v then
            f = true
            break
        end
    end
    return f
end

--- Checks if a value exists in a table
-- returns true or false
function Misc:table_has_match(t1,t2)
    for k=1, tlen(t1) do
        for j=1,tlen(t2) do
            if t2[j] == t1[k] then
                return true
            end
        end
    end
    return false
end

--- Removes value from table [limit] number of times
-- if limit is 0 or lower it will remove all
function Misc:table_remove(t,v,limit)
    limit = tonumber(limit) or 1
    local removed = 0
    for k = 1 , tlen(t) do 
        if t[k] == v then
            table.remove(t,k)
            removed = removed + 1
            if limit >= 1 and removed >= limit then break end
        end
    end
end

--- Adds a value to a table if it does not already exist
function Misc:table_addNotExist(t,v)
    if not self:table_find(t,v) then
        t[tlen(t)+1] = v
    end
end

--- Convert a table into correct types
-- I might not need this function any more.
function Misc:table_convertvalues(t)
    if type(t) ~= "table" then return { } end -- Return blank table
    local new_t = { }
    for key,value in pairs(t) do
        if tonumber(key) ~= nil then key = tonumber(key) end
        if type(value) == "table" then
            value = self:table_convertvalues(value)
        elseif tonumber(value) ~= nil then
            value = tonumber(value)
        elseif value == "" then
            value = ""
        elseif value == "true" then
            value = true
        elseif value == "false" then
            value = false
        end
        new_t[key] = value
    end
    return new_t
end


--- Gets longest string in a table
function Misc:table_longestString(t,index)
    local longest = 0
    for k = 1 , tlen(t) do 
        local length = 0
        if index then
            length = _len(et.Q_CleanStr(tostring(t[k][index])))
        else
            length = _len(et.Q_CleanStr(tostring(t[k])))
        end
        if length > longest then
            longest = length
        end
    end
    return longest
end

-------------------------
-- TABLE FORMAT FUNCTIONS
-------------------------

local _addcolumns = function(t)
    local equals = 0
    for k = 1 , tlen(t) do 
        equals = equals + t[k]
    end
    return equals
end

local _equalize = function(t,bannerpadlength,max)
    while ( _addcolumns(t) + bannerpadlength ) > max do
        local index = 0
        local len   = 0
        for k = 1 , tlen(t) do 
            if t[k] > len then
                len   = t[k]
                index = k
            end
        end
        t[index] = t[index] - 1
    end
    return t
end

local _getColumns = function( columnLength , extra , maxLength )
    local numRows = 1
    while ( columnLength * numRows ) + extra < maxLength do
        numRows = numRows + 1
    end
    return numRows - 1
end

local _getTableColor = function()
    local color = Config.Message.TableColor
    color = string.gsub(color,"<color1>",Color.Primary)
    color = string.gsub(color,"<color2>",Color.Secondary)
    color = string.gsub(color,"<color3>",Color.Tertiary)
    return color
end
--[[
[data]          Table of table of data values
[columnHeaders] Table of column header names
[center]        If data should be centered
[maxChars]      Maximum line length (Will automatically shorten all columns as needed)
[maxLines]      Maximum amount of lines per page
]]-- 
function Misc:FormatTable( data , columnHeaders , center , maxChars , maxLines , currentPage )
    if not data or not next(data) then return end
    if not columnHeaders or not next(columnHeaders) then return end
    if not center == true then center = false end
    maxChars = tonumber(maxChars) or Console:getMaxChars()
    
    local color   = _getTableColor()
    local padding = color .. Config.Message.TableSymbol
    
    local FormattedTable    = { }
    local columnHeader      = padding
    local columnTitleLength = { }
    local columnLength      = ( ( 3 * tlen(columnHeaders) ) + 1 ) + ( tlen(columnHeaders) * _len( et.Q_CleanStr(padding) ) )
    
    -- Check to make sure there are the same amount of data as columns
    -- Get longest string length of each column, checking both data and column header
    for k=1,tlen(columnHeaders) do
        columnTitleLength[k] = _len( et.Q_CleanStr(columnHeaders[k]) )
        local columnDataLength = self:table_longestString( data , k )
        if columnTitleLength[k] < columnDataLength then columnTitleLength[k] = columnDataLength end
    end
    
    _equalize(columnTitleLength,columnLength,maxChars) -- Shorten each column to fit max characters
    
    -- Make the column header
    for k = 1 , tlen(columnHeaders) do
        columnHeader = columnHeader .. " " .. Color.Secondary .. self:GetStaticString(self:trim(columnHeaders[k]),columnTitleLength[k]," ",center) .. padding --  .. " "
    end
    
    local border = self:GetStaticString( padding ,_len( et.Q_CleanStr( columnHeader ) ) - 1 , et.Q_CleanStr(padding) , false )
    
    -- Make the header with column header titles
    FormattedTable[tlen(FormattedTable)+1] = border
    FormattedTable[tlen(FormattedTable)+1] = columnHeader
    FormattedTable[tlen(FormattedTable)+1] = border
    
    -- Get page data
    local currentData = 1
    local maxData     = tlen(data)
    maxLines          = tonumber(maxLines)
    currentPage       = tonumber(currentPage) or 1
    
    if maxLines then
        --Console:Info( "Max Pages: %d" , _ceil( maxData / maxLines ) )
        if currentPage <= 0 then
            currentPage = 1
        elseif currentPage > _ceil( maxData / maxLines ) then
            currentPage = _ceil( maxData / maxLines )
        end
        if tlen(data) > maxLines then
            maxData     = maxLines * currentPage
            currentData = maxLines * ( currentPage - 1 )
            if currentData <= 0 then currentData = 1 end
        end
        if maxData > tlen( data ) then maxData = tlen( data ) end
        --Console:Info( "currentData: %d maxData: %d maxLines: %d currentPage: %d" , currentData,maxData,maxLines,currentPage )
    end
    
    local row = 4
    -- Make the actual table data
    for k=currentData,maxData do
        if tlen( data[k] ) ~= tlen( columnHeaders ) then return end -- Not same amount of table data.
        FormattedTable[row] = padding
        for column=1,tlen(data[k]) do
            FormattedTable[row] = FormattedTable[row] .. " " .. Color.Primary .. self:GetStaticString(data[k][column],columnTitleLength[column]," ",center) .. padding -- .. " " after center)
        end
        row = row + 1
    end
    
    -- Make the footer
    local footer = border
    if maxLines then
        local display = maxLines
        if maxLines + currentData > tlen(data) then
            display = tlen(data) -- - currentData
        end
        footer = padding .. padding .. padding .. padding .. Color.Primary .. " page " .. Color.Secondary .. currentPage .. Color.Tertiary .. '/' .. Color.Secondary .. _ceil( tlen(data) / maxLines ) .. ' ' .. padding .. padding .. padding .. padding
        footer = footer  .. Color.Primary .. ' Displaying ' .. Color.Secondary .. display .. Color.Primary .. ' of ' .. Color.Secondary .. tlen(data) .. padding
        footer = self:GetStaticString( footer ,_len( et.Q_CleanStr( columnHeader ) ) - 1 , et.Q_CleanStr(padding) , false )
    end
    FormattedTable[tlen(FormattedTable)+1] = footer
    
    return FormattedTable
end

function Misc:FormatList( data , center , maxChars , maxLines , currentPage )
    if not data or not next(data) then return end
    if not center == true then center = false end
    maxChars = tonumber(maxChars) or Console:getMaxChars()
    
    local color   = _getTableColor()
    local padding = color .. Config.Message.TableSymbol
    
    -- TESTTABLE:
    -- :: Zelly :: Bob :: Test2 :: Test3 ::
    local FormattedTable = { }
    local paddingLen     = _len(et.Q_CleanStr(padding))
    local columnLength   = self:table_longestString(data)
    local columnExtra_1  = 1 -- Spaces per column
    local columnExtra_2  = ( paddingLen * 2 ) + 1
    local columns        = _getColumns( columnLength + columnExtra_1 , columnExtra_2 , maxChars )
    local rowLength      = ( ( columnLength + columnExtra_1 ) * columns ) + columnExtra_2
    local columnHeader   = self:GetStaticString( padding , rowLength , et.Q_CleanStr(padding) , false )
    
    -- Get page data
    local currentData = 1
    local maxData     = tlen(data)
    maxLines          = tonumber(maxLines)
    currentPage       = tonumber(currentPage) or 1
    
    if maxLines then
        if currentPage <= 0 then
            currentPage = 1
        elseif currentPage > _ceil( _ceil( maxData / columns ) / maxLines ) then
            currentPage = _ceil( _ceil(maxData/columns) / maxLines )
        end
        --Console:Info( "Max Pages: %d" , _ceil( _ceil(maxData/columns) / maxLines ) )
        if _ceil( maxData / columns) > maxLines then
            maxData     = (columns*maxLines) * currentPage
            currentData = (columns*maxLines) * ( currentPage - 1 ) + 1
            if currentData <= 0 then currentData = 1 end
        end
        if maxData > tlen( data ) then maxData = tlen( data ) end
        --Console:Info( "currentData: %d maxData: %d maxLines: %d currentPage: %d" , currentData,maxData,maxLines,currentPage )
    end
    
    -- Make header
    FormattedTable[tlen(FormattedTable)+1] = columnHeader
    
    -- Make table
    local row = 2
    for k=currentData,maxData do
        if not FormattedTable[row] then FormattedTable[row] = Color.Primary end
        FormattedTable[row] = FormattedTable[row] .. self:GetStaticString(data[k],columnLength," ",center)
        if k == tlen( data ) then
            FormattedTable[row] = padding .. ' ' .. self:GetStaticString(FormattedTable[row],rowLength-3,' ' , false) .. padding
        elseif math.mod(k,columns) == 0 then
            FormattedTable[row] = padding .. ' ' .. FormattedTable[row] .. ' ' .. padding
            row = row + 1
        end
    end
    
    
    -- Make the footer
    local footer = border
    if maxLines then
        local display = columns*maxLines
        if currentData + display > tlen(data) then
            display = tlen(data) -- - currentData
        end
        --Console:Info( "columns[%d] maxLines[%d] display[%d] currentData[%d] tlen(data)[%d]" , columns,maxLines,display,currentData,tlen(data) )
        footer = padding .. padding .. padding .. padding .. Color.Primary .. " page " .. Color.Secondary .. currentPage .. Color.Tertiary .. '/' .. Color.Secondary .. _ceil( _floor(tlen(data)/columns) / maxLines ) .. ' ' .. padding .. padding .. padding .. padding
        footer = footer  .. Color.Primary .. ' Displaying ' .. Color.Secondary .. display .. Color.Primary .. ' of ' .. Color.Secondary .. tlen(data) .. ' ' .. padding
        footer = self:GetStaticString( footer ,_len( et.Q_CleanStr( columnHeader ) ) - 1 , et.Q_CleanStr(padding) , false )
    end
    FormattedTable[tlen(FormattedTable)+1] = footer
    
    return FormattedTable
end

--------------------------------
-- TABLE/STRING/FORMAT FUNCTIONS
--------------------------------
-- These functions kind of fit all categories

if string.find(_VERSION,"5.0") then
    --- Wrap a string
    -- 5.1 version
    function Misc:wrap( str , length )
        local split_str = self:split( str , " " )
        local new_str   = { }
        local currentLine = 1
        new_str[currentLine] = ""
        for k=1 , tlen(split_str) do 
            if split_str[k] ~= "" then
                if ( _len(split_str[k]) + _len(new_str[currentLine]) + 1 ) <= length then
                    new_str[currentLine] = new_str[currentLine] .. ' ' .. split_str[k]
                else
                    currentLine = currentLine + 1
                    new_str[currentLine] = split_str[k]
                end
            end
        end
        return table.concat( new_str , "\n" )
    end
else
    --- Wrap a string
    -- 5.2+ version
    function Misc:wrap(str, limit)
        limit      = limit or 72
        local here = 1
        return string.gsub(str,"(%s+)()(%S+)()" , function(sp, st, word, fi)
                if fi-here > limit then
                    here = st
                    return "\n" .. word
                end
            end,20)
    end
end

--- Splits words that are bigger than length
-- Purpose of this is to returned a spaced string so that the formatter can add line returns when needed.
-- Without this large words would go beyond max length
function Misc:splitLargeWords(str , length)
    local t    = self:split(str," ") -- split string into table of words
    local newt = { }                 -- placeholder for return table
    
    for k = 1 , tlen(t) do -- iterate each word
        while _len(t[k]) > length do                 -- if word length is longer thant max length
            newt[tlen(newt)+1] = _sub(t[k],1,length) -- cut off the length amount of string add to new string
            t[k]               = _sub(t[k],length+1) -- set current table to the rest of string
        end
        if t[k] ~= "" then newt[tlen(newt)+1] = t[k] end   -- add the rest of the string
    end
    return table.concat(newt," ")
end

--- Formats a long string into a table with specified length
-- Each table index should be length long
-- This function also removes trailing color codes
function Misc:FormatLongString(str,length)
    if not str then return { "" } end
    length = tonumber(length)
    if not length then return { str, } end
    
    --Console:InfoClean( "FormatLongString str: %s | length:%d" , str,length )
    str = self:splitLargeWords(str,length)
    --Console:InfoClean( "FormatLongString str: %s" , str)
    
    local wrappedstr = self:wrap(str,length)

    --Console:InfoClean( "FormatLongString wrappedstr: %s" , string.gsub(wrappedstr,"\n","//") )
    local t             = self:split(wrappedstr,"\n")
    local longeststring = self:table_longestString(t)
    for k = 1 , tlen(t) do
        t[k] = string.gsub( t[k] , "\n" , "" )
        --Console:InfoClean( "FormatLongString (before)table[%d]: %s" , k,t[k])
        t[k] = self:GetStaticString(self:cleanLineEndingColorCodes(t[k]),longeststring)
        --Console:InfoClean( "FormatLongString (after)table[%d]: %s" , k,t[k])
        -- used to have -1 Make sure to fill rest with spaces
    end
    return t
end

-------------------
-- STRING FUNCTIONS
-------------------

-- Google lua encrypt string took several versions of what I found and made my own little version of it.
local _convert = function(chars,dist,inv)
    return _char( _mod( _byte( chars ) - 32 + ( inv and -dist or dist ) , 95) + 32 )
end
local _rand_char = function()
   local num = _rand(32,126)
   
   while true do
       if num ~= 37 and num ~= 92 then
           break
        else
           num = _rand(32,126)
        end
   end
   return _char(num)
end
--- Encrypt a string
-- Google lua encrypt string took several versions of what I found and made my own little version of it.
function Misc:encrypt(s)
    local k = {29, 58, 93, 28, 27}
    local inv = nil
    local enc = ""
    for i = 1 , _len(s) do 
        if _len(s)-k[5] >= i or not inv then
            for inc=0,3 do
                if _mod(i,4) == inc then
                    enc = enc .. _convert(_sub(s,i,i),k[inc+1],inv)
                    break
                end
            end
        end
    end
    if not inv then
        for _=1,k[5] do
            enc = enc .. _rand_char();
        end
    end
    return enc
end

--- This is supposed to escape a string
-- I don't think it has ever worked for me though, am using it wrong I beleive.
function Misc:escape( str )
    if not str then return "" end
    str = string.gsub(str,'%%', '%%%%')
    str = string.gsub(str,'%^', '%%%^')
    str = string.gsub(str,'%$', '%%%$')
    str = string.gsub(str,'%(', '%%%(')
    str = string.gsub(str,'%)', '%%%)')
    str = string.gsub(str,'%.', '%%%.')
    str = string.gsub(str,'%[', '%%%[')
    str = string.gsub(str,'%]', '%%%]')
    str = string.gsub(str,'%*', '%%%*')
    str = string.gsub(str,'%+', '%%%+')
    str = string.gsub(str,'%-', '%%%-')
    str = string.gsub(str,'%?', '%%%?')
    return str
end

--- Cleans line ending color codes
-- Example:
--      Before:^1this ^2is a string^3
--      After :^1this ^2is a string
--      Before:^1this ^2is a string^3^4^7
--      After :^1this ^2is a string
--      Before:^1this ^2is a string^
--      After :^1this ^2is a string
function Misc:cleanLineEndingColorCodes( str )
    local f = true
    while f do
        if _sub( str ,-2 , -2 ) == "^" then
            str = _sub( str , 1 , -3 )
        elseif _sub( str , -1 , -1 ) == "^" then
            str = _sub(str , 1 , -2 )
        else
            f = false
        end
    end
    return str
end

--- Gets a static string
-- [str] is the string you want to convert
-- [length] is the max length it can be
-- [sep] is the seperator you want to fill in the blanks with (Defaults to space)
-- [center] is true if you want the string centered false is you want to justify to left ( Defaults to false )
function Misc:GetStaticString(str,length,sep,center)
    if not str then str = " " end                 -- Make sure we have at least something to work with
    str = tostring(str)                           -- Make sure if its a number then convert to string
    if not length then return str end             -- We don't have a length to go off, just return the str
    if not sep then sep = " " end                 -- Default sep to spaces
    if center ~= true then center = false end -- Default to left align

    if tonumber(length) == nil then           -- If length is a string then use the length of that string
        length = _len(et.Q_CleanStr(length))
    end
    length = tonumber(length)                    -- If it was a string number then make sure its in number form
    local strlength = _len( et.Q_CleanStr(str) ) -- String length without color codes

    if strlength > length then                    -- String length is greater then the length we want
        str = _sub(et.Q_CleanStr(str),1,length) .. sep -- Have to lose colors here, or else its going to be wrong
        return str
    end

    if center then -- If we are centering the text
        local pad_max = _floor(( length - strlength ) / 2)
        local padding = ""
        for x=0, pad_max-1 do
            padding = padding
        end
        str = padding .. str .. padding
    end

    for x = _len(et.Q_CleanStr(str)),length,1 do -- Fill the rest in
        str =  str .. sep
    end

    return str
end

--- Trims white space from a string
-- Added ability to lowercase and clean string by adding more arguments
function Misc:trim(s,lower,clean)
    if s == nil then return "" end
    s = tostring(s)
    s = self:match(s,"^%s*(.-)%s*$")
    if lower then s = string.lower(s) end
    if clean then s = et.Q_CleanStr(s) end
    return s
end

--- Trims white space and lowercases
-- Not needed any more, but is still used throughout code
-- need to phase out
function Misc:triml(s)
    if s == nil then return "" end
    s = tostring(s)
    s = string.lower(s)
    return self:trim(s)
end

--- Trims whitespace , lowercases and cleans string of color codes
-- Not needed any more, but is still used throughout code
-- need to phase out
function Misc:trimlname(name)
    if name == nil then return "" end
    return self:triml(et.Q_CleanStr(name))
end

--- Matches a string
function Misc:match( s , p )
    if string.find(_VERSION,"5.0") then
        local junk1,junk2,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10 = string.find( s , p )
        return a1,a2,a3,a4,a5,a6,a7,a8,a9,a10
    else
        return string.match( s , p )
    end
end

--- Splits string into a table using pattern
function Misc:split(str,pat)
    local t = {}
    local fpat = "(.-)" .. pat
    local last_end = 1
    local s, e, cap = string.find(str,fpat, 1)
    while s do
        if s ~= 1 or cap ~= "" then
            t[tlen(t)+1] = cap
        end
        last_end = e+1
        s, e, cap = string.find(str,fpat, last_end)
    end

    if last_end <= _len(str) then
        cap = _sub(str,last_end)
        t[tlen(t)+1] = cap
    end
    return t
end

--- Makes first character in string uppercase
function Misc:firstUpper(str)
    return (string.gsub(str,"^%l", string.upper))
end

--- Generates a random word
function Misc:randomWord()
    local vowel = _getVowel()
    local name  = _getConsonant() .. _getConsonant() .. vowel .. vowel .. _getConsonant() .. _getConsonant() .. _getVowel()
    return self:firstUpper(name)
end

--- Makes a random string
function Misc:makeString(l)
    if l < 1 then l = 6 end -- Check for l < 1
    local s = "" -- Start string
    for i = 1, l do
        s = s .. _getValidCharacter() -- turn it into character and add to string
    end
    return s -- Return string
end

---------------
-- IP FUNCTIONS
---------------

-- ip1 is the full ip to match
-- ip2 is the possibily partial ip
function Misc:matchIp( ip1 , ip2 )
    if not ip1 or not ip2 then return false end
    if not string.find( ip1 , "%d+%.%d+%.%d+%.%d+" ) then return false end
    if ip1 == '' or ip2 == '' then return false end -- Don't want to match anything
    if string.find( ip2 , "%d+%.%d+%.%d+%.%d+" ) then -- FULL IP
        if ip1 == ip2 then
            return true
        else
            return false
        end
    else
        if string.find( ip1 , ip2 , 1 , true ) then -- PARTIAL IP
            return true
        else
            return false
        end
    end
end
--- Checks if a ip is IPv4
function Misc:isIPv4(ip)
    if ( ip == nil ) then return false end
    if ( self:GetIPType(ip) == 1) then
        return true
    else
        return false
    end
end

--- Checks type of ip
-- 0 = not string
-- 1 = ipv4
-- 2 = ipv6
-- 3 = random string
-- http://stackoverflow.com/questions/10975935/lua-function-check-if-ipv4-or-ipv6-or-string
function Misc:GetIPType(ip)
    -- must pass in a string value
    if ip == nil or type(ip) ~= "string" then
        return 0
    end

    -- check for format 1.11.111.111 for ipv4
    local chunks = { self:match(ip,"(%d+)%.(%d+)%.(%d+)%.(%d+)")}
    if (tlen(chunks) == 4) then
        for _,v in pairs(chunks) do
            if (tonumber(v) < 0 or tonumber(v) > 255) then
                return 0
            end
        end
        return 1
    else
        return 0
    end

    -- check for ipv6 format, should be 8 'chunks' of numbers/letters
    local _, chunks = string.gsub(ip,"[%a%d]+%:?", "")
    if chunks == 8 then
        return 2
    end

    -- if we get here, assume we've been given a random string
    return 3
end

-----------------
-- TIME FUNCTIONS
-----------------

--- Convert a time format to seconds
-- Example 2m = 120 seconds
function Misc:getTimeFormat(timeinfo)
    if not timeinfo then return 0 end
    if tonumber(timeinfo) then return tonumber(timeinfo) end
    timeinfo = Misc:triml(timeinfo)
    local time,timeType = self:match(timeinfo,"^(%d+)(%a)$")
    time = tonumber(time)
    if not time or not timeType then return 0 end
    if timeType == "s" then
        return time
    elseif timeType == "m" then
        return time * 60
    elseif timeType == "h" then
        return time * 3600
    elseif timeType == "d" then
        return time * 86400
    elseif timeType == "w" then
        return time * ( 86400 * 7 )
    elseif timeType == "o" then
        return time * ( 86400 * 30 )
    elseif timeType == "y" then
        return time * ( 86400 * 365 )
    else
        return time
    end
end

--- Converts seconds to a more readable format
-- TODO Misc:secondsToClock ? don't like clock word
function Misc:SecondsToClock(seconds)
    seconds = tonumber(seconds) or 0
    if seconds <= 0 then return "0 seconds" end
    
    local days = _floor( seconds / 86400 );seconds = math.mod(seconds, 86400)
    local hours = _floor( seconds / 3600 );seconds = math.mod(seconds , 3600)
    local mins = _floor( seconds / 60 );seconds = _floor( math.mod(seconds, 60) )
    
    if days == 1 then
        days = "1 Day "
    elseif days > 0 then
        days = days .. " Days "
    elseif days <= 0 then
        days = ""
    end
    
    if hours == 1 then
        hours = "1 Hour "
    elseif hours > 0 then
        hours = string.format("%02.f", hours) .. " Hours "
    elseif hours <= 0 then
        hours = ""
    end
    
    if mins == 1 then
        mins = "1 Minute "
    elseif mins > 0 then
        mins = string.format("%02.f", mins) .. " Minutes "
    elseif mins <= 0 then
        mins = ""
    end
    
    if seconds == 1 then
        seconds = "1 Second"
    elseif seconds > 0 then
        seconds = string.format("%02.f", seconds) .. " Seconds"
    elseif seconds <= 0 then
        seconds = ""
    end
    
    return days .. hours .. mins .. seconds
end

-------------------------
-- NUMBER/FLOAT FUNCTIONS
-------------------------
--- Guarntee integer value
-- Rounds up or down based on decimal value
function Misc:int( floatvalue , floor ) -- Not sure if works in lua 5.0/1
    if not floatvalue then return nil end
    if floor then floatvalue = _floor(floatvalue) end
    local intvalue,decimal = math.modf( floatvalue )
    if decimal >= 0.5 then
        intvalue = intvalue + 1
    end
    if math.tointeger then
        return math.tointeger(intvalue)
    else
        return tonumber(intvalue)
    end
end

--- Round to decimal value
function Misc:round(value,decimal)
    if decimal then
        return _floor( ((value * 10^decimal) + 0.5) / (10^decimal) )
    else
        return _floor(value+0.5)
    end
end

--- Rounds percentage to decimal value
function Misc:roundPercent(value,decimal)
    local mult = 10^(decimal or 0)
    return _floor(value * mult + 0.5) / mult
end

------------------
-- OTHER FUNCTIONS
------------------

--- Converts value to correct type for key
-- key can also be the type you want
function Misc:ConvertType(key,value)
    if not key then
        Console:Error(va("Misc:ConvertType tried to convert %s to nil type" , tostring(value)))
        return
    end

    key = self:triml(key)

    -- Check basic types first
    if key == "number" then
        return tonumber(value) or 0
    elseif key == "string" then
        return self:trim(value)
    elseif key == "boolean" then
        if not value then return false end
        if ( value == true or self:triml(value) == "true" or tonumber(value) == 1 ) then return true end
        return false
    elseif key == "table" then
        if type(value) ~= "table" then value = { } end
        return value
    elseif key == "" then
        if value == nil then
            return ""
        elseif ( value == true or value == false ) then
            return value
        elseif tonumber(value) ~= nil then
            return tonumber(value)
        elseif self:triml(value) == "true" then
            return true
        elseif self:triml(value) == "false" then
            return false
        else-- Check for table?
            return tostring(value)
        end
    end
    -- No defined key type

    -- Check Keys table
    for k = 1 , tlen(Keys) do 
        if Keys[k].name == key then
            if value == nil then
                return Keys[k].default
            else
                return self:ConvertType(Keys[k].type,value)
            end
        end
    end

    Console:Error(va("Misc:ConvertType Couldn't convert %s to %s type",tostring(value),tostring(key)))
    return nil
end

--[[ not used
-- Wrap a string into a paragraph
--   s: string to wrap
--   w: width to wrap to [78]
--   i1: indent of first line [0]
--   i2: indent of subsequent lines [0]
-- returns
--   s: wrapped paragraph
function wrap(s, w, i1, i2)
  w = w or 78
  i1 = i1 or 0
  i2 = i2 or 0
  affirm(i1 < w and i2 < w,
         "wrap: the indents must be less than the line width")
  s = strrep(" ", i1) .. s
  local lstart, len = 1, strlen(s)
  while len - lstart > w do
    local i = lstart + w
    while i > lstart and strsub(s, i, i) ~= " " do i = i - 1 end
    local j = i
    while j > lstart and strsub(s, j, j) == " " do j = j - 1 end
    s = strsub(s, 1, j) .. "\n" .. strrep(" ", i2) ..
      strsub(s, i + 1, -1)
    local change = i2 + 1 - (i - j)
    lstart = j + change
    len = len + change
  end
  return s
end



function Misc:getBitClient(bitFlag)
    if bitflag == nil then return nil end
    for k = 0 , tlen(Bits) do 
        if ( bitFlag == Bits[k] ) then return k end
    end
    return nil
end
--]]



--- Checks if a package is loaded
--[[ not used also should not require on Console etc. should move out of this class
function Misc:isModuleAvailable(name)
    if package.loaded[name] then
        return true
    else
        for _, searcher in ipairs(package.searchers or package.loaders) do
            local loader = searcher(name)
            if type(loader) == 'function' then
                package.preload[name] = loader
                return true
            end
        end
        Console:Error("Module \"%s\" is not available", tostring(name) )
        return false
    end
end

function Misc:unrequire(m) -- Not sure if I will need this or not.
    package.loaded[m] = nil
    _G[m] = nil
end]]--