# Lua Enhanced Server Module
This project is for Enhancing Clan servers with Lua.  
[wiki](/zelly/lua-enhanced-server-mod/wiki/Home "The wiki contains just about everything you need to know")  
    
Please contact me in xfire *anewxfireaccount* if you have any questions  
  
  
# Where is my update? (2.7.6)
Working on making 2.7.6 compatible with silent 0.9.0 at least.  
Testing all the commands before I release it.  


# Features
See a list of features [here](/zelly/lua-enhanced-server-mod/wiki/Features)  
# Tutorial #
This tutorial will help you in starting out creating a new profile and using all the features you need.  
See the tutorial [here](/zelly/lua-enhanced-server-mod/wiki/Tutorial)  
# Keys #
View all of the keys you have access to as well on the [Keys](/zelly/lua-enhanced-server-mod/wiki/Keys) page  
# Bugs & Proposals #
Please feel free to report any problems or proposals to the [issue tracker](https://bitbucket.org/zelly/lua-enhanced-server-mod/issues/all)   
https://bitbucket.org/zelly/lua-enhanced-server-mod/issues/new  
I will get to it as soon as possible  
